# KUtil
(pronounced "cute-il")

This is a huge library with all kinds of features that I need for my projects. Feel free to use it :)

## Usage

Just dig through the packages that exist. Usage is pretty much straight forward. You can also take a look at the unit tests. Don't even think about documentation :)

## About pull requests

I probably won't accept new features, again: this is a library for me, that I use in my projects. If you need something for your projects, just create your own.

But I'll probably accept unit tests, changes and fixes.

## Stability and bugs

Yes, this project is still changing sometimes some core behaviours. But for versions, I kind of follow the [semantic version schema](https://semver.org/).
Patch versions indicate non-functional or breaking change, i.e. bug fixes or added functionality.
Minor versions indicate changed behaviour or api breaking change.
No clue about major versions, not happened yet ;)

### Maven

```xml
<dependency>
    <groupId>de.bixilon</groupId>
    <artifactId>kutil</artifactId>
    <version>1.27</version>
</dependency>
```

### Gradle

```groovy
implementation 'de.bixilon:kutil:1.27'
```

```kotlin
implementation("de.bixilon:kutil:1.27")
```
