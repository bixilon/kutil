/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.url

import de.bixilon.kutil.collections.CollectionUtil.synchronizedMapOf
import java.net.URL
import java.net.URLConnection
import java.net.URLStreamHandler
import java.net.URLStreamHandlerFactory

object URLProtocolStreamHandlers : URLStreamHandlerFactory {
    private val PROTOCOLS: MutableMap<String, URLStreamHandler> = synchronizedMapOf()

    init {
        URL.setURLStreamHandlerFactory(this)
    }

    override fun createURLStreamHandler(protocol: String?): URLStreamHandler? {
        return PROTOCOLS[protocol]
    }

    val NULL_URL_CONNECTION: URLConnection = object : URLConnection(null) {
        override fun connect() = Unit
    }

    fun register(protocol: String, handler: URLStreamHandler) {
        PROTOCOLS[protocol] = handler
    }

    inline operator fun set(protocol: String, handler: URLStreamHandler) = register(protocol, handler)

    fun unregister(protocol: String) {
        PROTOCOLS.remove(protocol)
    }
}

