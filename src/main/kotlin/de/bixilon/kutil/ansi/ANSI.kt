/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.ansi

object ANSI {
    const val ESCAPE = "\u001b["
    val RESET = formatting(0)
    val BOLD = formatting(1)
    val FAINT = formatting(2)
    val ITALIC = formatting(3)
    val UNDERLINE = formatting(4)
    val SLOW_BLINK = formatting(5)
    val FAST_BLINK = formatting(6)
    val STRIKE = formatting(9)

    val BLACK = formatting(30)
    val BLACK_BACKGROUND = formatting(40)
    val RED = formatting(31)
    val RED_BACKGROUND = formatting(41)
    val GREEN = formatting(32)
    val GREEN_BACKGROUND = formatting(42)
    val YELLOW = formatting(33)
    val YELLOW_BACKGROUND = formatting(43)
    val BLUE = formatting(34)
    val BLUE_BACKGROUND = formatting(34)
    val MAGENTA = formatting(35)
    val MAGENTA_BACKGROUND = formatting(45)
    val CYAN = formatting(36)
    val CYAN_BACKGROUND = formatting(46)
    val WHITE = formatting(37)
    val WHITE_BACKGROUND = formatting(47)

    fun escape(text: String): String {
        return ESCAPE + text + "m"
    }

    fun formatting(code: Int): String = escape(code.toString())

    fun rgb(red: Int, green: Int, blue: Int): String = escape("38;2;$red;$green;${blue}")
}
