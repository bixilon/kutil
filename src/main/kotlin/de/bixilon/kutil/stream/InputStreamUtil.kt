/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.stream

import de.bixilon.kutil.buffer.ByteBufferUtil.createBuffer
import de.bixilon.kutil.exception.ExceptionUtil.ignoreAll
import de.bixilon.kutil.string.StringUtil
import java.io.InputStream
import java.io.OutputStream
import java.nio.charset.Charset
import java.security.MessageDigest
import java.security.Signature

object InputStreamUtil {

    fun InputStream.readAll(close: Boolean = true): ByteArray {
        if (close) {
            return use { readAllBytes() }
        }
        return readAllBytes()
    }

    fun InputStream.readAsString(charset: Charset = StringUtil.DEFAULT_CHARSET, close: Boolean = true): String {
        val string = String(readAll(), charset)
        if (close) ignoreAll { close() }
        return string
    }

    fun InputStream.copy(output: OutputStream): Long {
        val buffer = createBuffer()
        var length: Int
        var size = 0L
        while (true) {
            length = this.read(buffer, 0, buffer.size)
            if (length < 0) {
                break
            }
            size += length
            output.write(buffer, 0, length)
        }
        return size
    }


    fun InputStream.copy(vararg output: OutputStream, digest: MessageDigest? = null, signature: Signature? = null, closeIn: Boolean = true, closeOut: Boolean = true) {
        val buffer = createBuffer()

        while (true) {
            val length = read(buffer, 0, buffer.size)
            if (length < 0) {
                break
            }
            for (stream in output) {
                stream.write(buffer, 0, length)
            }
            digest?.update(buffer, 0, length)
            signature?.update(buffer, 0, length)
        }

        if (closeIn) ignoreAll { close() }
        if (closeOut) {
            for (stream in output) {
                ignoreAll { stream.close() }
            }
        }
    }
}
