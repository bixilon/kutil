package de.bixilon.kutil.cast

import de.bixilon.kutil.cast.CastUtil.nullCast
import de.bixilon.kutil.cast.CastUtil.unsafeCast

object CollectionCast {

    inline fun Any?.asAnyCollection(): Collection<Any> {
        return this.unsafeCast()
    }

    inline fun Any?.toAnyCollection(): Collection<Any>? {
        return this?.nullCast()
    }

    inline fun Any?.asAnyList(): List<Any> {
        return this.unsafeCast()
    }

    inline fun Any?.toAnyList(): List<Any>? {
        return this?.nullCast()
    }

    inline fun Any?.asAnyMap(): Map<Any, Any> {
        return this.unsafeCast()
    }

    inline fun Any?.toAnyMap(): Map<Any, Any>? {
        return this?.nullCast()
    }
}
