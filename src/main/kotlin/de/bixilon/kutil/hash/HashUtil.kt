/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.hash

import de.bixilon.kutil.array.ByteArrayUtil.toHex
import de.bixilon.kutil.buffer.ByteBufferUtil.createBuffer
import java.io.ByteArrayInputStream
import java.io.InputStream
import java.security.MessageDigest

object HashUtil {
    const val SHA_1 = "SHA-1"
    const val SHA_256 = "SHA-256"
    const val SHA_512 = "SHA-512"

    private fun MessageDigest.hash(input: InputStream): String {
        val buffer = createBuffer()
        var length: Int
        while (true) {
            length = input.read(buffer, 0, buffer.size)
            if (length < 0) {
                break
            }
            this.update(buffer, 0, length)
        }
        return this.digest().toHex()
    }

    fun InputStream.sha1(): String {
        return MessageDigest.getInstance(SHA_1).hash(this)
    }

    fun ByteArray.sha1(): String {
        return ByteArrayInputStream(this).sha1()
    }

    fun InputStream.sha256(): String {
        return MessageDigest.getInstance(SHA_256).hash(this)
    }

    fun ByteArray.sha256(): String {
        return ByteArrayInputStream(this).sha256()
    }

    fun InputStream.sha512(): String {
        return MessageDigest.getInstance(SHA_512).hash(this)
    }

    fun ByteArray.sha512(): String {
        return ByteArrayInputStream(this).sha512()
    }

    fun Long.murmur64(): Long {
        var value = this
        value = value xor (value ushr 33)
        value *= -0xae502812aa7333L
        value = value xor (value ushr 33)
        value *= -0x3b314601e57a13adL
        value = value xor (value ushr 33)
        return value
    }
}
