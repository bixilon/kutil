/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.uuid

import de.bixilon.kutil.string.WhitespaceUtil.removeWhitespaces
import java.util.*

object UUIDUtil {
    private const val NORMAL_LENGTH = 36
    private const val TRIMMED_LENGTH = 32
    private const val DASH = '-'
    val NULL = UUID(0L, 0L)

    fun String.toUUID(): UUID {
        var uuid = this
        var length = uuid.length
        if (length > NORMAL_LENGTH) {
            uuid = uuid.removeWhitespaces()
            length = uuid.length
        }
        if (length == NORMAL_LENGTH) return UUID.fromString(uuid)
        if (length == TRIMMED_LENGTH) {
            val builder = StringBuilder(NORMAL_LENGTH)
            builder.append(uuid)
            builder.insert(20, DASH)
            builder.insert(16, DASH)
            builder.insert(12, DASH)
            builder.insert(8, DASH)
            uuid = builder.toString()
            return UUID.fromString(uuid)
        }

        throw IllegalArgumentException("Invalid uuid string $uuid")
    }

    fun UUID.trim(): String {
        return this.toString().replace("-", "")
    }
}
