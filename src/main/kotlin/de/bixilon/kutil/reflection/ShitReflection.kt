package de.bixilon.kutil.reflection

import de.bixilon.kutil.reflection.ReflectionUtil.getFieldOrNull

object ShitReflection {

    fun Any.getByField(vararg fields: String): Any? {
        val clazz = this::class.java
        for (name in fields) {
            val field = clazz.getFieldOrNull(name, true) ?: continue
            return field.get(this)
        }
        throw IllegalAccessError("Can not get field of $this by name: ${fields.contentToString()}")
    }
}
