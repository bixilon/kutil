/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.reflection

import de.bixilon.kutil.cast.CastUtil.unsafeCast
import de.bixilon.kutil.exception.Broken
import de.bixilon.kutil.exception.ExceptionUtil.ignoreAll
import de.bixilon.kutil.observer.ObserveUtil.receiver
import de.bixilon.kutil.reflection.java.JvmReflection
import de.bixilon.kutil.reflection.wrapper.ObjectField
import de.bixilon.kutil.reflection.wrapper.StaticField
import de.bixilon.kutil.reflection.wrapper.unsafe.UnsafeObjectField
import de.bixilon.kutil.reflection.wrapper.unsafe.UnsafeStaticField
import de.bixilon.kutil.reflection.wrapper.wrapped.WrappedObjectField
import de.bixilon.kutil.reflection.wrapper.wrapped.WrappedStaticField
import de.bixilon.kutil.unsafe.UnsafeUtil
import de.bixilon.kutil.unsafe.UnsafeUtil.setUnsafeAccessible
import java.lang.reflect.Field
import java.lang.reflect.Modifier
import kotlin.jvm.internal.CallableReference
import kotlin.reflect.KClass
import kotlin.reflect.KProperty
import kotlin.reflect.KProperty0
import kotlin.reflect.jvm.javaField

object ReflectionUtil {
    private val ANY = Any::class.java
    private val PRIVATE_GET_DECLARED_FIELDS = ignoreAll { Class::class.java.getDeclaredMethod("privateGetDeclaredFields", Boolean::class.java).apply { setUnsafeAccessible() } }

    fun Field.forceSet(instance: Any, value: Any?) {
        if (this.type.isPrimitive) {
            if (value == null) throw NullPointerException("Can not set null to a primitive field!")
            if (Modifier.isStatic(modifiers)) static.setPrimitive(value) else field.setPrimitive(instance, value)
        } else {
            if (Modifier.isStatic(modifiers)) static.set(value) else field.set(instance, value)
        }
    }

    @Deprecated("that is actually broken in java 18+")
    fun Field.removeStaticFinal() {
        val instance = JvmReflection.INSTANCE ?: throw UnsupportedOperationException("Java 18+ does not work with that!")
        instance.removeStaticFinal(this)
    }


    val Class<*>.realName: String
        get() = this.name.removePrefix(this.packageName).removePrefix(".")


    inline fun Class<*>.forceInit() {
        Class.forName(this.name, true, this.classLoader)
    }

    fun <T> KProperty0<T>.forceSet(value: T) {
        if (this !is CallableReference) throw IllegalStateException("Not a CallableReference")
        val field = this.jvmField
        field.forceSet(boundReceiver, value)
    }

    val KProperty<*>.jvmField: Field
        get() {
            if (this !is CallableReference) throw IllegalStateException("Not a CallableReference")
            val field = receiver::class.java.getFieldOrNull(this.name) ?: javaField ?: throw NoSuchFieldException("This property has no backing field!")

            field.setUnsafeAccessible()

            return field
        }

    fun Class<*>.getUnsafeDeclaredFields(): Array<Field> {
        ignoreAll { PRIVATE_GET_DECLARED_FIELDS?.invoke(this, false)?.unsafeCast<Array<Field>>()?.let { return it } }

        return declaredFields
    }


    fun Class<*>.getFieldOrNull(name: String, supertypes: Boolean = true): Field? {
        val fields = getUnsafeDeclaredFields()
        val field = fields.find { it.name == name }
        if (field != null) {
            field.setUnsafeAccessible()
            return field
        }

        if (!supertypes) return null

        val superclass = superclass
        if (superclass == ANY) return null
        return superclass.getFieldOrNull(name, true)
    }

    fun Class<*>.getUnsafeField(name: String, supertypes: Boolean = true): Field {
        return getFieldOrNull(name, supertypes) ?: throw NoSuchFieldException("Can not find field $name of $this")
    }

    val KProperty<*>.static: StaticField
        get() = jvmField.static
    val Field.static: StaticField
        get() {
            if (!Modifier.isStatic(modifiers)) Broken("The field you try do access is not static!")
            return when {
                UnsafeUtil.allow -> UnsafeStaticField(this)
                else -> WrappedStaticField(this)
            }
        }

    val KProperty<*>.field: ObjectField
        get() = jvmField.field
    val Field.field: ObjectField
        get() {
            if (Modifier.isStatic(modifiers)) Broken("The field you try do access is static!")
            return when {
                UnsafeUtil.allow -> UnsafeObjectField(this)
                else -> WrappedObjectField(this)
            }
        }


    operator fun <T : Any> KClass<T>.set(obj: Any, name: String, value: Any) {
        val field = this.java.getFieldOrNull(name) ?: throw NullPointerException("A field with the name $name was not found!")
        field.field[obj] = value
    }
}
