/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.reflection.java

import de.bixilon.kutil.reflection.ReflectionUtil.getUnsafeField
import de.bixilon.kutil.unsafe.UnsafeUtil.setUnsafeAccessible
import java.lang.reflect.Field

class Java8Reflection : JvmReflection {
    private val getFieldAccessor = Field::class.java.getDeclaredMethod("getFieldAccessor", Any::class.java).apply { setUnsafeAccessible() }
    private val UnsafeQualifiedStaticObjectFieldAccessorImpl = Class.forName("sun.reflect.UnsafeQualifiedStaticObjectFieldAccessorImpl")
    private val isReadOnly = UnsafeQualifiedStaticObjectFieldAccessorImpl.getUnsafeField("isReadOnly")


    override fun removeStaticFinal(field: Field) {
        val accessor = getFieldAccessor.invoke(field, null)
        isReadOnly.setBoolean(accessor, false)
    }
}
