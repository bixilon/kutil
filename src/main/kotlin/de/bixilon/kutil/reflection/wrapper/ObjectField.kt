/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.reflection.wrapper

import de.bixilon.kutil.cast.CastUtil.unsafeCast

interface ObjectField : ReflectionField {

    operator fun <T> get(instance: Any): T = getAny(instance).unsafeCast()
    fun getAny(instance: Any): Any?
    fun getBoolean(instance: Any): Boolean
    fun getByte(instance: Any): Byte
    fun getShort(instance: Any): Short
    fun getChar(instance: Any): Char
    fun getInt(instance: Any): Int
    fun getLong(instance: Any): Long
    fun getFloat(instance: Any): Float
    fun getDouble(instance: Any): Double

    operator fun set(instance: Any, value: Any?)
    operator fun set(instance: Any, value: Boolean)
    operator fun set(instance: Any, value: Byte)
    operator fun set(instance: Any, value: Short)
    operator fun set(instance: Any, value: Char)
    operator fun set(instance: Any, value: Int)
    operator fun set(instance: Any, value: Long)
    operator fun set(instance: Any, value: Float)
    operator fun set(instance: Any, value: Double)
    fun setPrimitive(instance: Any, value: Any) = when (value) {
        is Boolean -> set(instance, value)
        is Byte -> set(instance, value)
        is Short -> set(instance, value)
        is Char -> set(instance, value)
        is Int -> set(instance, value)
        is Long -> set(instance, value)
        is Float -> set(instance, value)
        is Double -> set(instance, value)
        else -> set(instance, value)
    }
}
