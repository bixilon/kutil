/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.reflection.wrapper.unsafe

import de.bixilon.kutil.reflection.wrapper.StaticField
import de.bixilon.kutil.unsafe.UnsafeUtil
import java.lang.reflect.Field

class UnsafeStaticField(val field: Field) : StaticField {
    private val base = UnsafeUtil.UNSAFE.staticFieldBase(field)
    private val offset = UnsafeUtil.UNSAFE.staticFieldOffset(field)
    override val type: Class<*> get() = this.field.type

    private fun requireType(name: String) {
        if (name != type.name) throw IllegalArgumentException("Can not set ${type.name} field to an $name")
    }

    override fun getAny(): Any? {
        return UnsafeUtil.UNSAFE.getObject(base, offset)
    }

    override fun getBoolean(): Boolean {
        requireType("boolean")
        return UnsafeUtil.UNSAFE.getBoolean(base, offset)
    }

    override fun getByte(): Byte {
        requireType("byte")
        return UnsafeUtil.UNSAFE.getByte(base, offset)
    }

    override fun getShort(): Short {
        requireType("short")
        return UnsafeUtil.UNSAFE.getShort(base, offset)
    }

    override fun getChar(): Char {
        requireType("char")
        return UnsafeUtil.UNSAFE.getChar(base, offset)
    }

    override fun getInt(): Int {
        requireType("int")
        return UnsafeUtil.UNSAFE.getInt(base, offset)
    }

    override fun getLong(): Long {
        requireType("long")
        return UnsafeUtil.UNSAFE.getLong(base, offset)
    }

    override fun getFloat(): Float {
        requireType("float")
        return UnsafeUtil.UNSAFE.getFloat(base, offset)
    }

    override fun getDouble(): Double {
        requireType("double")
        return UnsafeUtil.UNSAFE.getDouble(base, offset)
    }

    override fun set(value: Any?) {
        if (value != null && !field.type.isAssignableFrom(value::class.java)) {
            throw IllegalArgumentException("${value::class.java} is not assignable to ${field.type}")
        }
        UnsafeUtil.UNSAFE.putObject(base, offset, value)
    }

    override fun set(value: Boolean) {
        requireType("boolean")
        UnsafeUtil.UNSAFE.putBoolean(base, offset, value)
    }

    override fun set(value: Byte) {
        requireType("byte")
        UnsafeUtil.UNSAFE.putByte(base, offset, value)
    }

    override fun set(value: Short) {
        requireType("short")
        UnsafeUtil.UNSAFE.putShort(base, offset, value)
    }

    override fun set(value: Char) {
        requireType("char")
        UnsafeUtil.UNSAFE.putChar(base, offset, value)
    }

    override fun set(value: Int) {
        requireType("int")
        UnsafeUtil.UNSAFE.putInt(base, offset, value)
    }

    override fun set(value: Long) {
        requireType("long")
        UnsafeUtil.UNSAFE.putLong(base, offset, value)
    }

    override fun set(value: Float) {
        requireType("float")
        UnsafeUtil.UNSAFE.putFloat(base, offset, value)
    }

    override fun set(value: Double) {
        requireType("double")
        UnsafeUtil.UNSAFE.putDouble(base, offset, value)
    }

    override val name: String
        get() = this.field.name


    override fun toString(): String {
        return field.toString()
    }

    override fun hashCode(): Int {
        return base.hashCode() + offset.toInt()
    }

    override fun equals(other: Any?): Boolean {
        if (other !is UnsafeStaticField) return false
        return other.offset == offset && other.base == base
    }
}
