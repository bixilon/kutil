/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundationeither version 3 of the Licenseor (at your option) any later version.
 *
 * This program is distributed in the hope that it will be usefulbut WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If notsee <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.reflection.wrapper

import de.bixilon.kutil.cast.CastUtil.unsafeCast

interface StaticField : ReflectionField {

    fun <T> get(): T = getAny().unsafeCast()
    fun getAny(): Any?
    fun getBoolean(): Boolean
    fun getByte(): Byte
    fun getShort(): Short
    fun getChar(): Char
    fun getInt(): Int
    fun getLong(): Long
    fun getFloat(): Float
    fun getDouble(): Double

    fun set(value: Any?)
    fun set(value: Boolean)
    fun set(value: Byte)
    fun set(value: Short)
    fun set(value: Char)
    fun set(value: Int)
    fun set(value: Long)
    fun set(value: Float)
    fun set(value: Double)
    fun setPrimitive(value: Any) = when (value) {
        is Boolean -> set(value)
        is Byte -> set(value)
        is Short -> set(value)
        is Char -> set(value)
        is Int -> set(value)
        is Long -> set(value)
        is Float -> set(value)
        is Double -> set(value)
        else -> set(value)
    }
}
