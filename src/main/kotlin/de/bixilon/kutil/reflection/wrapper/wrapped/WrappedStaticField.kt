/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.reflection.wrapper.wrapped

import de.bixilon.kutil.exception.ExceptionUtil.catchAll
import de.bixilon.kutil.reflection.java.JvmReflection
import de.bixilon.kutil.reflection.wrapper.StaticField
import de.bixilon.kutil.unsafe.UnsafeUtil.setUnsafeAccessible
import java.lang.reflect.Field
import java.lang.reflect.Modifier

class WrappedStaticField(val field: Field) : StaticField {
    override val type: Class<*> get() = this.field.type

    init {
        field.setUnsafeAccessible()
        if (Modifier.isStatic(field.modifiers) && Modifier.isFinal(field.modifiers)) {
            catchAll { JvmReflection.INSTANCE?.removeStaticFinal(field) }
        }
    }

    override fun getAny(): Any? {
        return field.get(null)
    }

    override fun getBoolean(): Boolean {
        return field.getBoolean(null)
    }

    override fun getByte(): Byte {
        return field.getByte(null)
    }

    override fun getShort(): Short {
        return field.getShort(null)
    }

    override fun getChar(): Char {
        return field.getChar(null)
    }

    override fun getInt(): Int {
        return field.getInt(null)
    }

    override fun getLong(): Long {
        return field.getLong(null)
    }

    override fun getFloat(): Float {
        return field.getFloat(null)
    }

    override fun getDouble(): Double {
        return field.getDouble(null)
    }

    override fun set(value: Any?) {
        field.set(null, value)
    }

    override fun set(value: Boolean) {
        field.setBoolean(null, value)
    }

    override fun set(value: Byte) {
        field.setByte(null, value)
    }

    override fun set(value: Short) {
        field.setShort(null, value)
    }

    override fun set(value: Char) {
        field.setChar(null, value)
    }

    override fun set(value: Int) {
        field.setInt(null, value)
    }

    override fun set(value: Long) {
        field.setLong(null, value)
    }

    override fun set(value: Float) {
        field.setFloat(null, value)
    }

    override fun set(value: Double) {
        field.setDouble(null, value)
    }

    override val name: String
        get() = this.field.name


    override fun toString(): String {
        return field.toString()
    }

    override fun hashCode(): Int {
        return field.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        if (other !is WrappedStaticField) return false
        return other.field == field
    }
}
