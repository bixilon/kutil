/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.reflection.wrapper.unsafe

import de.bixilon.kutil.reflection.wrapper.ObjectField
import de.bixilon.kutil.unsafe.UnsafeUtil
import java.lang.reflect.Field

class UnsafeObjectField(val field: Field) : ObjectField {
    private val offset = UnsafeUtil.UNSAFE.objectFieldOffset(field)
    override val type: Class<*> get() = this.field.type

    private fun requireType(name: String) {
        if (name != type.name) throw IllegalArgumentException("Can not set ${type.name} field to an $name")
    }

    override fun getAny(instance: Any): Any? {
        return UnsafeUtil.UNSAFE.getObject(instance, offset)
    }

    override fun getBoolean(instance: Any): Boolean {
        requireType("boolean")
        return UnsafeUtil.UNSAFE.getBoolean(instance, offset)
    }

    override fun getByte(instance: Any): Byte {
        requireType("byte")
        return UnsafeUtil.UNSAFE.getByte(instance, offset)
    }

    override fun getShort(instance: Any): Short {
        requireType("short")
        return UnsafeUtil.UNSAFE.getShort(instance, offset)
    }

    override fun getChar(instance: Any): Char {
        requireType("char")
        return UnsafeUtil.UNSAFE.getChar(instance, offset)
    }

    override fun getInt(instance: Any): Int {
        requireType("int")
        return UnsafeUtil.UNSAFE.getInt(instance, offset)
    }

    override fun getLong(instance: Any): Long {
        requireType("long")
        return UnsafeUtil.UNSAFE.getLong(instance, offset)
    }

    override fun getFloat(instance: Any): Float {
        requireType("float")
        return UnsafeUtil.UNSAFE.getFloat(instance, offset)
    }

    override fun getDouble(instance: Any): Double {
        requireType("double")
        return UnsafeUtil.UNSAFE.getDouble(instance, offset)
    }

    override operator fun set(instance: Any, value: Any?) {
        if (value != null && !field.type.isAssignableFrom(value::class.java)) {
            throw IllegalArgumentException("${value::class.java} is not assignable to ${field.type}")
        }
        UnsafeUtil.UNSAFE.putObject(instance, offset, value)
    }

    override operator fun set(instance: Any, value: Boolean) {
        requireType("boolean")
        UnsafeUtil.UNSAFE.putBoolean(instance, offset, value)
    }

    override operator fun set(instance: Any, value: Byte) {
        requireType("byte")
        UnsafeUtil.UNSAFE.putByte(instance, offset, value)
    }

    override operator fun set(instance: Any, value: Short) {
        requireType("short")
        UnsafeUtil.UNSAFE.putShort(instance, offset, value)
    }

    override operator fun set(instance: Any, value: Char) {
        requireType("char")
        UnsafeUtil.UNSAFE.putChar(instance, offset, value)
    }

    override operator fun set(instance: Any, value: Int) {
        requireType("int")
        UnsafeUtil.UNSAFE.putInt(instance, offset, value)
    }

    override operator fun set(instance: Any, value: Long) {
        requireType("long")
        UnsafeUtil.UNSAFE.putLong(instance, offset, value)
    }

    override operator fun set(instance: Any, value: Float) {
        requireType("float")
        UnsafeUtil.UNSAFE.putFloat(instance, offset, value)
    }

    override operator fun set(instance: Any, value: Double) {
        requireType("double")
        UnsafeUtil.UNSAFE.putDouble(instance, offset, value)
    }

    override val name: String
        get() = this.field.name


    override fun toString(): String {
        return field.toString()
    }

    override fun hashCode(): Int {
        return field.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        if (other !is UnsafeObjectField) return false
        return other.offset == offset && other.field == field
    }
}
