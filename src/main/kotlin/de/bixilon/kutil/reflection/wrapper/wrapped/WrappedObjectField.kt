/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.reflection.wrapper.wrapped

import de.bixilon.kutil.reflection.wrapper.ObjectField
import de.bixilon.kutil.unsafe.UnsafeUtil.setUnsafeAccessible
import java.lang.reflect.Field

class WrappedObjectField(val field: Field) : ObjectField {
    override val type: Class<*> get() = this.field.type

    init {
        field.setUnsafeAccessible()
    }

    override fun getAny(instance: Any): Any? {
        return field.get(instance)
    }

    override fun getBoolean(instance: Any): Boolean {
        return field.getBoolean(instance)
    }

    override fun getByte(instance: Any): Byte {
        return field.getByte(instance)
    }

    override fun getShort(instance: Any): Short {
        return field.getShort(instance)
    }

    override fun getChar(instance: Any): Char {
        return field.getChar(instance)
    }

    override fun getInt(instance: Any): Int {
        return field.getInt(instance)
    }

    override fun getLong(instance: Any): Long {
        return field.getLong(instance)
    }

    override fun getFloat(instance: Any): Float {
        return field.getFloat(instance)
    }

    override fun getDouble(instance: Any): Double {
        return field.getDouble(instance)
    }

    override operator fun set(instance: Any, value: Any?) {
        field.set(instance, value)
    }

    override operator fun set(instance: Any, value: Boolean) {
        field.setBoolean(instance, value)
    }

    override operator fun set(instance: Any, value: Byte) {
        field.setByte(instance, value)
    }

    override operator fun set(instance: Any, value: Short) {
        field.setShort(instance, value)
    }

    override operator fun set(instance: Any, value: Char) {
        field.setChar(instance, value)
    }

    override operator fun set(instance: Any, value: Int) {
        field.setInt(instance, value)
    }

    override operator fun set(instance: Any, value: Long) {
        field.setLong(instance, value)
    }

    override operator fun set(instance: Any, value: Float) {
        field.setFloat(instance, value)
    }

    override operator fun set(instance: Any, value: Double) {
        field.setDouble(instance, value)
    }

    override val name: String
        get() = this.field.name


    override fun toString(): String {
        return field.toString()
    }

    override fun hashCode(): Int {
        return field.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        if (other !is WrappedObjectField) return false
        return other.field == field
    }
}
