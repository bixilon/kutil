package de.bixilon.kutil.math

import de.bixilon.kutil.math.MathConstants.PIf
import kotlin.math.PI

object Trigonometry {
    private const val VALUES = 65536
    private const val MASK = 0xFFFF
    private val SINE_TABLE = FloatArray(VALUES) { kotlin.math.sin(it * PI * 2.0 / VALUES).toFloat() }

    fun sin(value: Float): Float {
        return SINE_TABLE[(value * (VALUES / PIf / 2.0f)).toInt() and MASK]
    }

    fun cos(value: Float): Float {
        return SINE_TABLE[(value * (VALUES / PIf / 2.0f) + (VALUES / 4)).toInt() and MASK]
    }
}
