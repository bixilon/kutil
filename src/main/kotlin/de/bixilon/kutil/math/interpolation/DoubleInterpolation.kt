package de.bixilon.kutil.math.interpolation

import kotlin.math.PI
import kotlin.math.sin

object DoubleInterpolation {

    fun interpolateLinear(delta: Double, start: Double, end: Double): Double {
        if (delta <= 0.0) {
            return start
        }
        if (delta >= 1.0) {
            return end
        }
        return start + delta * (end - start)
    }

    fun interpolateExponential(delta: Double, start: Double, end: Double): Double {
        if (delta <= 0.0) {
            return start
        }
        if (delta >= 1.0) {
            return end
        }
        return start + (delta * delta) * (end - start)
    }

    fun interpolateSine(delta: Double, start: Double, end: Double): Double {
        if (delta <= 0.0f) {
            return start
        }
        if (delta >= 1.0f) {
            return end
        }
        return start + (sin(delta * PI / 2.0)) * (end - start) // not using sine table. I mean you are already using a double...
    }
}
