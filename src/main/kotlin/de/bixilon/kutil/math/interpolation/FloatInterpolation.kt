package de.bixilon.kutil.math.interpolation

import de.bixilon.kutil.math.Trigonometry.sin
import kotlin.math.PI

object FloatInterpolation {

    fun interpolateLinear(delta: Float, start: Float, end: Float): Float {
        if (delta <= 0.0f) {
            return start
        }
        if (delta >= 1.0f) {
            return end
        }
        return start + delta * (end - start)
    }

    fun interpolateExponential(delta: Float, start: Float, end: Float): Float {
        if (delta <= 0.0f) {
            return start
        }
        if (delta >= 1.0f) {
            return end
        }
        return start + (delta * delta) * (end - start)
    }

    fun interpolateSine(delta: Float, start: Float, end: Float): Float {
        if (delta <= 0.0f) {
            return start
        }
        if (delta >= 1.0f) {
            return end
        }
        return start + (sin(delta * PI.toFloat() / 2.0f)) * (end - start)
    }
}
