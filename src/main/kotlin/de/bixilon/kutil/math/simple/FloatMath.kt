package de.bixilon.kutil.math.simple

object FloatMath {

    fun Float.clamp(min: Float, max: Float): Float {
        if (this < min) {
            return min
        }
        if (this > max) {
            return max
        }
        return this
    }

    val Float.rounded10: Float
        get() = (this * 10.0f + 5.0f).toInt().toFloat() / 10.0f


    val Float.floor: Int
        get() {
            val int = this.toInt()
            return if (this < int) int - 1 else int
        }

    val Float.ceil: Int
        get() {
            val int = this.toInt()
            return if (this > int) int + 1 else int
        }


    inline fun Float.square(): Float = this * this

    inline val Float.fractional: Float
        get() = this - this.toInt()
}
