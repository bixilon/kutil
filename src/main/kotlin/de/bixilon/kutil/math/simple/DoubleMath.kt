package de.bixilon.kutil.math.simple

object DoubleMath {

    fun Double.clamp(min: Double, max: Double): Double {
        if (this < min) {
            return min
        }
        if (this > max) {
            return max
        }
        return this
    }

    val Double.rounded10: Double
        get() = (this * 10.0 + 0.5).toInt().toDouble() / 10.0


    val Double.floor: Int
        get() {
            val int = this.toInt()
            return if (this < int) int - 1 else int
        }

    val Double.ceil: Int
        get() {
            val int = this.toInt()
            return if (this > int) int + 1 else int
        }


    inline fun Double.square(): Double = this * this

    inline val Double.fractional: Double
        get() = this - this.toInt()
}
