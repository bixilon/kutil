package de.bixilon.kutil.math.simple

import de.bixilon.kutil.math.MathConstants
import de.bixilon.kutil.math.simple.DoubleMath.ceil
import kotlin.math.ln

object IntMath {

    fun Int.clamp(min: Int, max: Int): Int {
        if (this < min) {
            return min
        }
        if (this > max) {
            return max
        }
        return this
    }


    val Int.binaryBase: Int
        get() = (ln(this.toDouble()) / MathConstants.LN_2).ceil


    fun Int.divideUp(divider: Int): Int {
        return (this + divider - 1) / divider
    }
}
