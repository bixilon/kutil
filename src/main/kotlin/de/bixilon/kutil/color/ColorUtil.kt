package de.bixilon.kutil.color

import de.bixilon.kutil.primitive.IntUtil.toHex
import java.awt.Color

object ColorUtil {

    fun Color.toHex() = "#${red.toHex(2)}${green.toHex(2)}${blue.toHex(2)}"

}
