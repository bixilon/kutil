/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.random

import java.util.*

object RandomStringUtil {
    private val RANDOM_STRING_CHARS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".toCharArray()

    fun Random.randomString(length: Int): String {
        val builder = StringBuilder(length)
        for (index in 0 until length) {
            builder.append(randomChar())
        }
        return builder.toString()
    }

    fun Random.randomChar(): Char {
        return RANDOM_STRING_CHARS[nextInt(RANDOM_STRING_CHARS.size)]
    }
}
