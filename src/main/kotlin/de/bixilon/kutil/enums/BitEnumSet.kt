package de.bixilon.kutil.enums

import de.bixilon.kutil.bit.set.AbstractBitSet

class BitEnumSet<T : Enum<*>>(
    private val universe: Array<T>,
    private val bits: AbstractBitSet = AbstractBitSet.of(universe.size),
) : MutableSet<T> {
    override var size = 0
        private set

    operator fun set(element: T, value: Boolean) {
        val ordinal = element.ordinal
        if (bits[ordinal] == value) return
        bits[ordinal] = value
        if (value) size++ else size--
    }

    override fun add(element: T): Boolean {
        val ordinal = element.ordinal
        if (bits[ordinal]) return false
        bits[ordinal] = true
        size++
        return true
    }

    override fun addAll(elements: Collection<T>): Boolean {
        var added = 0
        for (element in elements) {
            val ordinal = element.ordinal
            if (bits[ordinal]) continue
            bits[ordinal] = true
            added++
        }
        size += added
        return added == elements.size
    }

    override fun clear() {
        bits.clear()
    }

    override fun isEmpty() = size == 0

    override fun contains(element: T): Boolean {
        return bits[element.ordinal]
    }

    override fun containsAll(elements: Collection<T>): Boolean {
        for (element in elements) {
            if (element !in this) return false
        }
        return true
    }

    override fun iterator(): MutableIterator<T> = EnumIterator()

    override fun retainAll(elements: Collection<T>): Boolean {
        TODO("Not yet implemented")
    }

    override fun removeAll(elements: Collection<T>): Boolean {
        var removed = 0
        for (element in elements) {
            val ordinal = element.ordinal
            if (!bits[ordinal]) continue
            bits[ordinal] = false
            removed++
        }
        this.size -= removed
        return removed == elements.size
    }

    override fun remove(element: T): Boolean {
        val ordinal = element.ordinal
        if (!bits[ordinal]) return false
        bits[ordinal] = false
        size--
        return true
    }

    override fun hashCode(): Int {
        return bits.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        if (other is BitEnumSet<*>) {
            return other.universe === universe && other.bits == bits
        }
        if (other !is Set<*>) return false

        if (size != other.size) return false
        return containsAll(other)
    }

    fun copy(): BitEnumSet<T> {
        val copy = BitEnumSet(universe, bits.clone())
        copy.size = size
        return copy
    }


    override fun toString(): String {
        val iterator = iterator()
        if (!iterator.hasNext()) return "[]"
        val builder = StringBuilder()
        builder.append('[')

        while (true) {
            val element = iterator.next()
            builder.append(element)
            if (!iterator.hasNext()) {
                return builder.append(']').toString()
            }

            builder.append(',').append(' ')
        }
    }

    private inner class EnumIterator : MutableIterator<T> {
        private var current = -1
        private var next = -1

        init {
            setNext() // allows calling next() when knowing the size
        }

        override fun hasNext(): Boolean {
            if (size == 0) return false
            setNext()
            if (next >= universe.size) return false
            if (next == current) return false

            return true
        }

        private fun setNext() {
            if (next != current) return

            for (index in maxOf(next + 1, 0) until universe.size) {
                if (!bits[index]) continue

                next = index
                return
            }
        }

        override fun next(): T {
            if (next < 0 || current == next) throw IllegalStateException("There is no next!")
            current = next
            return universe[current]
        }

        override fun remove() {
            if (current < 0) throw IllegalStateException("Not iterating?")
            if (current > size) return
            if (!bits[current]) return
            bits[current] = false
            size--
        }
    }
}
