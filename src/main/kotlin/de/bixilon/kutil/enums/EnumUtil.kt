/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.enums

import de.bixilon.kutil.collections.map.creator.MapCreator.sMutableMapOf

object EnumUtil {

    fun <T : Enum<*>> getEnumValues(values: Array<T>): Map<String, T> {
        val names: MutableMap<String, T> = sMutableMapOf()

        for (value in values) {
            names[value.name.lowercase()] = value

            if (value is AliasableEnum) {
                for (name in value.names) {
                    names[name] = value
                }
            }
        }

        return names
    }
}
