/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.enums

import de.bixilon.kutil.bit.set.AbstractBitSet

interface ValuesEnum<T : Enum<*>> : Iterable<T> {
    val VALUES: Array<T>
    val NAME_MAP: Map<String, T>

    operator fun get(ordinal: Int): T {
        return VALUES[ordinal]
    }

    fun getOrNull(ordinal: Int): T? {
        return VALUES.getOrNull(ordinal)
    }

    operator fun get(name: String): T {
        return NAME_MAP[name.lowercase()] ?: throw IllegalArgumentException("Can not find enum value: $name")
    }

    fun getOrNull(name: String?): T? {
        if (name == null) return null
        return NAME_MAP[name.lowercase()]
    }


    operator fun get(any: Any): T? {
        return when (any) {
            is String -> get(any)
            is Int -> get(any)
            is Long -> get(any.toInt())
            else -> throw IllegalArgumentException("Can not get enum identifier $any")
        }
    }

    fun getOrNull(any: Any?): T? {
        return when (any) {
            null -> null
            is String -> getOrNull(any)
            is Int -> getOrNull(any)
            is Long -> getOrNull(any.toInt())
            else -> throw IllegalArgumentException("Can not get enum identifier $any")
        }
    }

    fun next(current: T): T {
        val ordinal = current.ordinal
        if (ordinal + 1 > VALUES.size) {
            return VALUES[0]
        }
        return VALUES[ordinal + 1]
    }

    fun set() = BitEnumSet(VALUES)
    fun setOfAll() = BitEnumSet(VALUES, AbstractBitSet.of(VALUES.size).apply { setAll() })
    fun set(vararg values: T): BitEnumSet<T> {
        val set = BitEnumSet(VALUES)
        for (value in values) {
            set += value
        }
        return set
    }


    override fun iterator(): Iterator<T> {
        return VALUES.iterator()
    }

    companion object {
        fun <T : Enum<T>> ValuesEnum<T>.names() = EnumUtil.getEnumValues(VALUES)
    }
}
