/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.observer.map.bi

import de.bixilon.kutil.cast.CastUtil.nullCast
import de.bixilon.kutil.collections.map.bi.AbstractBiMap
import de.bixilon.kutil.collections.map.bi.AbstractMutableBiMap
import de.bixilon.kutil.concurrent.lock.Lock
import de.bixilon.kutil.observer.IllegalFieldError
import de.bixilon.kutil.observer.ObserveUtil.invalid
import de.bixilon.kutil.observer.ObserveUtil.observer
import de.bixilon.kutil.observer.ObserverReference
import de.bixilon.kutil.observer.RemoveObserver
import de.bixilon.kutil.observer.map.MapChange
import java.lang.ref.WeakReference
import java.util.concurrent.atomic.AtomicInteger
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty
import kotlin.reflect.KProperty0

open class BiMapObserver<K, V>(value: AbstractMutableBiMap<K, V>) : ReadWriteProperty<Any, AbstractMutableBiMap<K, V>> {
    protected val observers: MutableList<ObserverReference<(MapChange<K, V>) -> Unit>> = mutableListOf()
    protected val value = ObservedBiMap(value)
    val lock: Lock = Lock.lock()
    
    val id = AtomicInteger(0)


    init {
        this.value.addObserver {
            lock.lock()
            val iterator = observers.iterator()
            for ((reference, _, observer) in iterator) {
                if (reference.invalid) {
                    iterator.remove()
                    continue
                }
                try {
                    observer(it)
                } catch (_: RemoveObserver) {
                    iterator.remove()
                    continue
                } catch (exception: Throwable) {
                    exception.printStackTrace()
                }
            }
            lock.unlock()
        }
    }

    override fun getValue(thisRef: Any, property: KProperty<*>): AbstractMutableBiMap<K, V> {
        return value
    }

    override fun setValue(thisRef: Any, property: KProperty<*>, value: AbstractMutableBiMap<K, V>) {
        if (this.value == value) {
            return
        }
        this.value.unsafe = value
    }

    open operator fun plusAssign(observer: ObserverReference<(MapChange<K, V>) -> Unit>) {
        lock.lock()
        this.observers += observer
        lock.unlock()
    }

    open operator fun minusAssign(id: Int) {
        lock.lock()
        this.observers.removeIf { it.id == id }
        lock.unlock()
    }

    companion object {
        fun <K, V> observedBiMap(value: AbstractMutableBiMap<K, V>): BiMapObserver<K, V> = BiMapObserver(value)

        @JvmName("observedBiMap1")
        fun <K, V> AbstractMutableBiMap<K, V>.observedBiMap(): BiMapObserver<K, V> = observedBiMap(this)

        fun <K, V> KProperty0<AbstractBiMap<K, V>>.observeBiMap(owner: Any, instant: Boolean = false, observer: (MapChange<K, V>) -> Unit): Int {
            val delegate = this.observer.nullCast<BiMapObserver<K, V>>() ?: throw IllegalFieldError(this)
            val reference = WeakReference(owner)
            val id = delegate.id.getAndIncrement()
            delegate += ObserverReference(reference, id, observer)
            if (instant) {
                observer(MapChange(adds = delegate.value.entries, emptySet()))
            }
            return id
        }
    }
}
