/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.observer.map

class MapChange<K, V>(
    val adds: Set<Map.Entry<K, V>>,
    val removes: Set<Map.Entry<K, V>>,
) {

    companion object {

        fun <K, V> Collection<Map.Entry<K, V>>.keys(): MutableSet<K> {
            val keys: MutableSet<K> = mutableSetOf()
            for ((key, _) in this) {
                keys += key
            }
            return keys
        }

        fun <K, V> Collection<Map.Entry<K, V>>.values(): MutableSet<V> {
            val values: MutableSet<V> = mutableSetOf()
            for ((_, value) in this) {
                values += value
            }
            return values
        }
    }
}
