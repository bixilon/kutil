/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.observer.map.bi

import de.bixilon.kutil.collections.map.bi.AbstractMutableBiMap
import de.bixilon.kutil.observer.map.MapChange
import de.bixilon.kutil.observer.map.MapEntry


class ObservedBiMap<K, V>(unsafe: AbstractMutableBiMap<K, V>) : AbstractMutableBiMap<K, V> {
    private val observer: MutableList<(MapChange<K, V>) -> Unit> = mutableListOf()
    override val size: Int get() = unsafe.size

    var unsafe = unsafe
        set(value) {
            if (field == value) {
                return
            }
            val previous = field
            field = value
            notifyObservers(MapChange(adds = value.entries, removes = previous.entries))
        }

    fun addObserver(observer: (MapChange<K, V>) -> Unit) {
        this.observer += observer
    }

    private fun notifyObservers(change: MapChange<K, V>) {
        for (observer in observer) {
            observer.invoke(change)
        }
    }

    override fun containsKey(key: K): Boolean {
        return unsafe.containsKey(key)
    }

    override fun containsValue(value: V): Boolean {
        return unsafe.containsValue(value)
    }

    override fun get(key: K): V? {
        return unsafe[key]
    }

    override fun isEmpty(): Boolean {
        return unsafe.isEmpty()
    }

    override val entries: MutableSet<MutableMap.MutableEntry<K, V>> by unsafe::entries
    override val keys: MutableSet<K> by unsafe::keys
    override val values: MutableCollection<V> by unsafe::values

    override fun clear() {
        notifyObservers(MapChange(emptySet(), entries))
        unsafe.clear()
    }

    override fun put(key: K, value: V): V? {
        val original = unsafe.put(key, value)
        val removed = original?.let { return@let setOf(MapEntry(key, it)) } ?: emptySet()

        notifyObservers(MapChange(setOf(MapEntry(key, value)), removed))
        return original
    }

    override fun putAll(from: Map<out K, V>) {
        val removed: MutableSet<MapEntry<K, V>> = mutableSetOf()
        for ((key, value) in from) {
            removed += MapEntry(key, unsafe.put(key, value) ?: continue)
        }

        notifyObservers(MapChange(from.entries, removed))
    }

    override fun remove(key: K): V? {
        val removed = unsafe.remove(key) ?: return null
        notifyObservers(MapChange(emptySet(), setOf(MapEntry(key, removed))))
        return removed
    }

    override fun removeValue(value: V): K? {
        val removed = unsafe.removeValue(value) ?: return null
        notifyObservers(MapChange(emptySet(), setOf(MapEntry(removed, value))))
        return removed
    }

    override fun getKey(value: V): K? {
        return unsafe.getKey(value)
    }

    override fun getValue(key: K): V? {
        return unsafe.getValue(key)
    }

    override fun toString(): String {
        return unsafe.toString()
    }

    override fun hashCode(): Int {
        return unsafe.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        return unsafe == other
    }
}
