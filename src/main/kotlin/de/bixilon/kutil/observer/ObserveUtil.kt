/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.observer

import de.bixilon.kutil.cast.CastUtil.unsafeCast
import de.bixilon.kutil.exception.ExceptionUtil.catchAll
import de.bixilon.kutil.reflection.ReflectionUtil.field
import de.bixilon.kutil.reflection.ReflectionUtil.getFieldOrNull
import de.bixilon.kutil.reflection.ReflectionUtil.getUnsafeField
import de.bixilon.kutil.unsafe.UnsafeUtil.setUnsafeAccessible
import java.lang.ref.WeakReference
import java.lang.reflect.Field
import kotlin.jvm.internal.CallableReference
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KClass
import kotlin.reflect.KProperty
import kotlin.reflect.KProperty0
import kotlin.reflect.KProperty1
import kotlin.reflect.full.memberProperties
import kotlin.reflect.jvm.isAccessible
import kotlin.reflect.jvm.javaField

object ObserveUtil {
    private val CLASS = Class.forName("kotlin.jvm.internal.CallableReference")
    private val RECEIVER_FIELD = CLASS.getUnsafeField("receiver").field
    private val OWNER_FIELD = CLASS.getUnsafeField("owner").field

    private val MUTABLE_PROPERTY_1_CLASS = Class.forName("kotlin.reflect.jvm.internal.KProperty1Impl")
    private val DELEGATE_SOURCE_FIELD = MUTABLE_PROPERTY_1_CLASS.getUnsafeField("delegateSource").field

    private val KCLASS_CLASS = Class.forName("kotlin.reflect.jvm.internal.KClassImpl")
    private val JCLASS_FIELD = KCLASS_CLASS.getUnsafeField("jClass").field

    val WeakReference<*>.invalid: Boolean
        get() {
            val value = get() ?: return true
            if (value is ObservedReference && value.isValid()) {
                return false
            }
            // if (referenceValue == null || (referenceValue is LifecycleOwner && referenceValue.lifecycle.currentState == Lifecycle.State.DESTROYED)) {
            return false
        }

    val KProperty<*>.receiver: Any
        get() = RECEIVER_FIELD[this]

    val KClass<*>.jClass: Class<*>
        get() = JCLASS_FIELD[this]

    val KProperty<*>.delegateSourceField: Field?
        get() {
            val lazy: Lazy<Any> = DELEGATE_SOURCE_FIELD[this]
            return lazy.value.unsafeCast()
        }
    val CallableReference.jOwner: Class<*>
        get() = OWNER_FIELD[this]

    private fun KProperty0<*>.getJDelegate(): Any? {
        if (this !is CallableReference) return null
        val javaField = javaField ?: return null
        if (!javaField.name.endsWith("delegate")) return null
        javaField.setUnsafeAccessible()

        return javaField.get(boundReceiver)
    }

    private fun KProperty0<*>.getJOwnerDelegate(): Any? {
        if (this !is CallableReference) return null
        val property = jOwner.getFieldOrNull("$name\$delegate", true) ?: return null
        property.setUnsafeAccessible()
        return property.get(boundReceiver)
    }

    private fun KProperty0<*>.getReceiverDelegate(): Any? {
        if (this !is CallableReference) return null
        val property = boundReceiver::class.memberProperties.find { it.name == this.name }.unsafeCast<KProperty1<Any, Any>>()
        property.isAccessible = true
        val delegateSourceField = property.delegateSourceField ?: return null
        delegateSourceField.setUnsafeAccessible()

        return property.getDelegate(boundReceiver)
    }

    private fun KProperty0<*>.getKDelegate(): Any? {
        isAccessible = true
        return getDelegate()
    }

    val KProperty0<*>.delegate: Any?
        get() {
            catchAll { getJOwnerDelegate()?.let { return it } }
            catchAll { getReceiverDelegate()?.let { return it } }
            catchAll { getJDelegate()?.let { return it } }
            catchAll { getKDelegate()?.let { return it } }

            return null
        }

    val KProperty0<*>.observer: ReadWriteProperty<*, *>?
        get() {
            val delegate = delegate
            if (delegate !is ReadWriteProperty<*, *>) {
                System.err.println("Can not get delegate for $this. Is it an observable property?")
                return null
            }
            return delegate
        }
}
