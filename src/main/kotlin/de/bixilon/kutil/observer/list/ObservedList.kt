/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.observer.list

import java.util.*
import java.util.stream.Stream

class ObservedList<V>(unsafe: MutableList<V>) : MutableList<V> {
    private val observer: MutableList<(ListChange<V>) -> Unit> = mutableListOf()
    override val size: Int get() = unsafe.size

    var unsafe = unsafe
        set(value) {
            if (field == value) {
                return
            }
            val previous = field
            field = value
            notifyObservers(ListChange(adds = value, removes = previous))
        }

    fun addObserver(observer: (ListChange<V>) -> Unit) {
        this.observer += observer
    }

    private fun notifyObservers(change: ListChange<V>) {
        for (observer in observer) {
            observer.invoke(change)
        }
    }

    override fun contains(element: V): Boolean {
        return unsafe.contains(element)
    }

    override fun containsAll(elements: Collection<V>): Boolean {
        return unsafe.containsAll(elements)
    }

    override fun get(index: Int): V {
        return unsafe[index]
    }

    override fun indexOf(element: V): Int {
        return unsafe.indexOf(element)
    }

    override fun isEmpty(): Boolean {
        return unsafe.isEmpty()
    }

    override fun iterator(): MutableIterator<V> {
        return ListIterator(unsafe.listIterator())
    }

    override fun lastIndexOf(element: V): Int {
        return unsafe.lastIndexOf(element)
    }

    override fun add(element: V): Boolean {
        val add = unsafe.add(element)
        if (!add) return false
        notifyObservers(ListChange(listOf(element), emptyList()))
        return true
    }

    override fun add(index: Int, element: V) {
        unsafe.add(index, element)
        notifyObservers(ListChange(listOf(element), emptyList()))
    }

    override fun addAll(index: Int, elements: Collection<V>): Boolean {
        val addAll = unsafe.addAll(index, elements)
        if (!addAll) return false
        notifyObservers(ListChange(elements, emptyList())) // maybe not all elements got added?
        return true
    }

    override fun addAll(elements: Collection<V>): Boolean {
        val addAll = unsafe.addAll(elements)
        if (!addAll) return false
        notifyObservers(ListChange(elements, emptyList())) // maybe not all elements got added?
        return true
    }

    override fun clear() {
        val copy = unsafe.toList()
        unsafe.clear()
        notifyObservers(ListChange(emptyList(), copy))
    }

    override fun listIterator(): MutableListIterator<V> {
        return ListIterator(unsafe.listIterator())
    }

    override fun listIterator(index: Int): MutableListIterator<V> {
        return ListIterator(unsafe.listIterator(index))
    }

    override fun remove(element: V): Boolean {
        if (!unsafe.remove(element)) {
            return false
        }
        notifyObservers(ListChange(emptyList(), listOf(element)))
        return true
    }

    override fun removeAll(elements: Collection<V>): Boolean {
        val contained: MutableList<V> = mutableListOf()
        for (element in elements) {
            if (element !in unsafe) {
                continue
            }
            contained += element
        }
        if (!unsafe.removeAll(elements)) {
            return false
        }
        if (contained.isNotEmpty()) {
            notifyObservers(ListChange(emptyList(), contained))
        }
        return true
    }

    override fun removeAt(index: Int): V {
        val removed = unsafe.removeAt(index)
        notifyObservers(ListChange(emptyList(), listOf(removed)))
        return removed
    }

    override fun retainAll(elements: Collection<V>): Boolean {
        val removed = unsafe.retainAll(elements)
        notifyObservers(ListChange(emptyList(), elements)) // ToDo
        return removed
    }

    override fun set(index: Int, element: V): V {
        val previous = unsafe.set(index, element)
        notifyObservers(ListChange(listOf(element), listOf(element)))
        return previous
    }

    override fun subList(fromIndex: Int, toIndex: Int): MutableList<V> {
        return unsafe.subList(fromIndex, toIndex)
    }

    override fun toString(): String {
        return unsafe.toString()
    }

    override fun hashCode(): Int {
        return unsafe.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        return unsafe == other
    }


    override fun spliterator(): Spliterator<V> {
        return unsafe.spliterator()
    }

    override fun parallelStream(): Stream<V> {
        return unsafe.parallelStream()
    }

    override fun stream(): Stream<V> {
        return unsafe.stream()
    }

    private inner class ListIterator(private val iterator: MutableListIterator<V>) : MutableListIterator<V> {
        private var current: V? = null

        override fun remove() {
            val current = this.current ?: throw IllegalStateException("Not iterating!")
            iterator.remove()
            notifyObservers(ListChange(adds = emptySet(), removes = setOf(current)))
        }

        override fun hasNext(): Boolean {
            return iterator.hasNext()
        }

        override fun next(): V {
            val value = iterator.next()
            this.current = value
            return value
        }

        override fun hasPrevious(): Boolean {
            return iterator.hasPrevious()
        }

        override fun nextIndex(): Int {
            return iterator.nextIndex()
        }

        override fun previous(): V {
            val value = iterator.previous()
            this.current = value
            return value
        }

        override fun previousIndex(): Int {
            return iterator.previousIndex()
        }

        override fun add(element: V) {
            iterator.add(element)
            notifyObservers(ListChange(adds = setOf(element), removes = emptySet()))
        }

        override fun set(element: V) {
            val current = this.current ?: throw IllegalStateException("Not iterating!")
            iterator.set(element)
            notifyObservers(ListChange(adds = setOf(element), removes = setOf(current)))
        }
    }
}
