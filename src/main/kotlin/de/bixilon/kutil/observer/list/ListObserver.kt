/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.observer.list

import de.bixilon.kutil.cast.CastUtil.nullCast
import de.bixilon.kutil.concurrent.lock.Lock
import de.bixilon.kutil.observer.IllegalFieldError
import de.bixilon.kutil.observer.ObserveUtil.invalid
import de.bixilon.kutil.observer.ObserveUtil.observer
import de.bixilon.kutil.observer.ObserverReference
import de.bixilon.kutil.observer.RemoveObserver
import java.lang.ref.WeakReference
import java.util.concurrent.atomic.AtomicInteger
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty
import kotlin.reflect.KProperty0

open class ListObserver<V>(
    value: MutableList<V>,
    val lock: Lock = Lock.lock(),
) : ReadWriteProperty<Any, MutableList<V>> {
    protected val observers: MutableList<ObserverReference<(ListChange<V>) -> Unit>> = mutableListOf()
    protected val value = ObservedList(value)

    val id = AtomicInteger(0)

    init {
        this.value.addObserver {
            lock.lock()
            val iterator = observers.iterator()
            for ((reference, _, observer) in iterator) {
                if (reference.invalid) {
                    iterator.remove()
                    continue
                }
                try {
                    observer(it)
                } catch (_: RemoveObserver) {
                    iterator.remove()
                    continue
                } catch (exception: Throwable) {
                    exception.printStackTrace()
                }
            }
            lock.unlock()
        }
    }

    override fun getValue(thisRef: Any, property: KProperty<*>): MutableList<V> {
        return value
    }

    override fun setValue(thisRef: Any, property: KProperty<*>, value: MutableList<V>) {
        if (this.value == value) {
            return
        }
        this.value.unsafe = value
    }

    open operator fun plusAssign(observer: ObserverReference<(ListChange<V>) -> Unit>) {
        lock.lock()
        this.observers += observer
        lock.unlock()
    }


    open operator fun minusAssign(id: Int) {
        lock.lock()
        this.observers.removeIf { it.id == id }
        lock.unlock()
    }

    companion object {
        fun <V> observedList(value: MutableList<V>): ListObserver<V> = ListObserver(value)

        @JvmName("observedList1")
        fun <V> MutableList<V>.observedList(): ListObserver<V> = observedList(this)

        fun <V> KProperty0<List<V>>.observeList(owner: Any, instant: Boolean = false, observer: (ListChange<V>) -> Unit): Int {
            val delegate = this.observer.nullCast<ListObserver<V>>() ?: throw IllegalFieldError(this)
            val reference = WeakReference(owner)
            val id = delegate.id.getAndIncrement()
            delegate += ObserverReference(reference, id, observer)
            if (instant) {
                observer(ListChange(adds = delegate.value, emptyList()))
            }
            return id
        }
    }
}
