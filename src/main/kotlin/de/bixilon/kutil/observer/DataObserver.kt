/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.observer

import de.bixilon.kutil.cast.CastUtil.nullCast
import de.bixilon.kutil.collections.map.bi.AbstractBiMap
import de.bixilon.kutil.concurrent.lock.Lock
import de.bixilon.kutil.observer.ObserveUtil.invalid
import de.bixilon.kutil.observer.ObserveUtil.observer
import java.lang.ref.WeakReference
import java.util.concurrent.atomic.AtomicInteger
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KMutableProperty0
import kotlin.reflect.KProperty
import kotlin.reflect.KProperty0

open class DataObserver<V>(
    protected var value: V,
    protected val lock: Lock = Lock.lock(),
) : ReadWriteProperty<Any, V> {
    protected val observers: MutableList<ObserverReference<(V) -> Unit>> = mutableListOf()

    val id = AtomicInteger(0)

    override fun getValue(thisRef: Any, property: KProperty<*>): V {
        return value
    }

    fun get() = value
    open fun set(value: V): V {
        val previous = this.value
        if (previous == value) {
            return value
        }
        this.value = value
        lock.lock()
        unsafeSet(value)
        lock.unlock()
        return previous
    }

    protected fun unsafeSet(value: V) {
        this.value = value

        val iterator = observers.iterator()
        for ((reference, _, observer) in iterator) {
            if (reference.invalid) {
                iterator.remove()
                continue
            }
            try {
                observer.invoke(value)
            } catch (_: RemoveObserver) {
                iterator.remove()
                continue
            } catch (exception: Throwable) {
                exception.printStackTrace()
            }
        }
    }

    override fun setValue(thisRef: Any, property: KProperty<*>, value: V) {
        set(value)
    }

    open operator fun plusAssign(observer: ObserverReference<(V) -> Unit>) {
        lock.lock()
        this.observers += observer
        lock.unlock()
    }

    open operator fun minusAssign(id: Int) {
        lock.lock()
        this.observers.removeIf { it.id == id }
        lock.unlock()
    }

    companion object {

        fun <V> observed(value: V): DataObserver<V> {
            if (value is AbstractBiMap<*, *>) {
                throw IllegalArgumentException("Use observedBiMap")
            }
            if (value is Map<*, *>) {
                throw IllegalArgumentException("Use observedMap")
            }
            if (value is List<*>) {
                throw IllegalArgumentException("Use observedList")
            }
            if (value is Set<*>) {
                throw IllegalArgumentException("Use observedSet")
            }
            return DataObserver(value)
        }

        @JvmName("observed1")
        fun <V> V.observed(): DataObserver<V> = observed(this)

        fun <V> KProperty0<V>.observe(owner: Any, instant: Boolean = false, observer: (V) -> Unit): Int {
            val delegate = this.observer.nullCast<DataObserver<V>>() ?: throw IllegalFieldError(this)
            val reference = WeakReference(owner)
            val id = delegate.id.getAndIncrement()
            delegate += ObserverReference(reference, id, observer)
            if (instant) {
                observer(delegate.value)
            }
            return id
        }

        fun KMutableProperty0<*>.removeObserver(id: Int) {
            val delegate = getDelegate().nullCast<DataObserver<*>>() ?: throw IllegalFieldError(this)
            delegate -= id
        }
    }
}
