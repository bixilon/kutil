/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.observer.set

import de.bixilon.kutil.collections.set.SetUtil.copy
import java.util.*
import java.util.stream.Stream

class ObservedSet<V>(unsafe: MutableSet<V>) : MutableSet<V> {
    private val observers: MutableList<(SetChange<V>) -> Unit> = mutableListOf()
    override val size: Int get() = unsafe.size

    var unsafe = unsafe
        set(value) {
            if (field == value) {
                return
            }
            val previous = field
            field = value
            notifyObservers(SetChange(adds = value, removes = previous))
        }

    fun addObserver(observer: (SetChange<V>) -> Unit) {
        observers += observer
    }

    private fun notifyObservers(change: SetChange<V>) {
        for (observer in observers) {
            observer.invoke(change)
        }
    }

    override fun add(element: V): Boolean {
        val added = unsafe.add(element)
        if (!added) return false
        notifyObservers(SetChange(setOf(element), emptySet()))
        return true
    }

    override fun addAll(elements: Collection<V>): Boolean {
        val add = unsafe.addAll(elements)
        if (!add) {
            return false
        }
        notifyObservers(SetChange(elements, emptySet()))
        return true
    }

    override fun clear() {
        val copy = unsafe.copy()
        unsafe.clear()
        notifyObservers(SetChange(emptySet(), copy))
    }

    override fun iterator(): MutableIterator<V> {
        return SetIterator(unsafe.iterator())
    }

    override fun remove(element: V): Boolean {
        val removed = unsafe.remove(element)
        if (!removed) return false
        notifyObservers(SetChange(emptySet(), setOf(element)))
        return true
    }

    override fun removeAll(elements: Collection<V>): Boolean {
        val removed = unsafe.removeAll(elements)
        if (!removed) return false
        notifyObservers(SetChange(emptySet(), elements))
        return true
    }

    override fun retainAll(elements: Collection<V>): Boolean {
        val retained = unsafe.retainAll(elements)
        if (!retained) {
            return false
        }
        println("Kutil: Set::retainAll is not observed yet!")
        return true
    }

    override fun contains(element: V): Boolean {
        return unsafe.contains(element)
    }

    override fun containsAll(elements: Collection<V>): Boolean {
        return unsafe.containsAll(elements)
    }

    override fun isEmpty(): Boolean {
        return unsafe.isEmpty()
    }

    override fun toString(): String {
        return unsafe.toString()
    }

    override fun hashCode(): Int {
        return unsafe.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        return unsafe == other
    }

    override fun spliterator(): Spliterator<V> {
        return unsafe.spliterator()
    }

    override fun parallelStream(): Stream<V> {
        return unsafe.parallelStream()
    }

    override fun stream(): Stream<V> {
        return unsafe.stream()
    }

    private inner class SetIterator(private val iterator: MutableIterator<V>) : MutableIterator<V> {
        private var current: V? = null

        override fun remove() {
            val current = this.current ?: throw IllegalStateException("Not iterating!")
            iterator.remove()
            notifyObservers(SetChange(adds = emptySet(), removes = setOf(current)))
        }

        override fun hasNext(): Boolean {
            return iterator.hasNext()
        }

        override fun next(): V {
            val value = iterator.next()
            this.current = value
            return value
        }
    }
}
