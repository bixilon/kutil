package de.bixilon.kutil.exception

open class StacklessException : Exception {

    constructor() : super()
    constructor(message: String?) : super(message)

    override fun fillInStackTrace() = this
}
