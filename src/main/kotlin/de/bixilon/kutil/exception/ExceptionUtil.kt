/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.exception

import java.io.PrintWriter
import java.io.StringWriter
import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.InvocationKind
import kotlin.contracts.contract

object ExceptionUtil {

    @OptIn(ExperimentalContracts::class)
    fun <T> tryCatch(vararg exceptions: Class<out Throwable> = arrayOf(), executor: () -> T): T? {
        contract {
            callsInPlace(executor, InvocationKind.EXACTLY_ONCE)
        }
        try {
            return executor()
        } catch (thrown: Throwable) {
            for (exception in exceptions) {
                if (exception.isAssignableFrom(thrown::class.java)) {
                    return null
                }
            }
            throw thrown
        }
    }

    @OptIn(ExperimentalContracts::class)
    inline fun <T> catchAll(executor: () -> T): T? {
        contract {
            callsInPlace(executor, InvocationKind.EXACTLY_ONCE)
        }
        try {
            return executor()
        } catch (ignored: Throwable) {
        }
        return null
    }

    @OptIn(ExperimentalContracts::class)
    inline fun <T> ignoreAll(executor: () -> T): T? {
        contract {
            callsInPlace(executor, InvocationKind.EXACTLY_ONCE)
        }
        try {
            return executor()
        } catch (error: Throwable) {
            error.printStackTrace()
        }
        return null
    }


    inline fun retry(tries: Int, block: () -> Unit) {
        if (tries < 1) throw IllegalArgumentException("Invalid number of tries: $tries")
        var thrown: Throwable? = null
        for (i in 0 until tries) {
            try {
                block.invoke()
                return
            } catch (error: Throwable) {
                thrown = error
            }
        }
        throw thrown!!
    }

    fun Throwable.toStackTrace(): String {
        val stringWriter = StringWriter()
        this.printStackTrace(PrintWriter(stringWriter))
        return stringWriter.toString()
    }
}
