/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.time

import de.bixilon.kutil.reflection.ReflectionUtil.field
import de.bixilon.kutil.unsafe.UnsafeUtil.setUnsafeAccessible
import kotlin.time.Duration

object DurationUtil {
    private val RAW_CONSTRUCTOR = Duration::class.java.getDeclaredConstructor(Long::class.java).apply { setUnsafeAccessible() }
    private val RAW_FIELD = DurationRaw::duration.field

    private class DurationRaw(val duration: Duration)

    fun Duration.raw(): Long { // Duration is inlined by the kotlin compiler and if you must set a field with reflections, you will have your issues.
        val wrapped = DurationRaw(this)
        return RAW_FIELD.getLong(wrapped)
    }

    @JvmStatic
    fun Long.durationString(): String {
        return RAW_CONSTRUCTOR.newInstance(this).toString()
    }
}
