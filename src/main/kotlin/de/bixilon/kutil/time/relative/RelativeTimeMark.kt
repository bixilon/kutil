package de.bixilon.kutil.time.relative

import kotlin.time.Duration
import kotlin.time.TimeMark

@JvmInline
value class RelativeTimeMark(val elapsed: Duration) : TimeMark, Comparable<RelativeTimeMark> {
    override fun elapsedNow() = elapsed

    /*
    override fun equals(other: Any?): Boolean {
        if (other !is RelativeTimeMark) return false
        return other.elapsed == elapsed
    }

    override fun hashCode() = elapsed.hashCode()
     */

    operator fun minus(mark: RelativeTimeMark): Duration {
        return elapsed - mark.elapsed
    }

    operator fun plus(mark: RelativeTimeMark): Duration {
        return elapsed + mark.elapsed
    }

    override fun plus(duration: Duration): RelativeTimeMark {
        return RelativeTimeMark(elapsed + duration)
    }

    override fun compareTo(other: RelativeTimeMark): Int {
        return elapsed.compareTo(other.elapsed)
    }

    companion object {
        val NULL = RelativeTimeMark(Duration.ZERO)
    }
}
