package de.bixilon.kutil.time.relative

import kotlin.time.TimeMark
import kotlin.time.TimeSource

class RelativeTimeSource(
    private var start: TimeMark? = null
) : TimeSource {

    fun start() {
        if (start != null) throw IllegalStateException("Big bang already happened!")
        start = TimeSource.Monotonic.markNow()
    }

    override fun markNow(): RelativeTimeMark {
        val start = this.start ?: return RelativeTimeMark.NULL

        return RelativeTimeMark(start.elapsedNow())
    }
}
