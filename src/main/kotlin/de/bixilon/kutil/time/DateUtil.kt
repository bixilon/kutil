package de.bixilon.kutil.time

import de.bixilon.kutil.time.TimeUtil.millis
import java.util.*

object DateUtil {
    val currentCalendar: Calendar
        get() {
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = millis()
            return calendar
        }

    val christmas: Boolean
        get() {
            val calendar = currentCalendar
            if (calendar.get(Calendar.MONTH) != Calendar.DECEMBER) {
                return false
            }
            val day = calendar.get(Calendar.DAY_OF_MONTH)
            return day in 24..26
        }

    val newYear: Boolean
        get() {
            val calendar = currentCalendar
            val month = calendar.get(Calendar.MONTH)
            val day = calendar.get(Calendar.DAY_OF_MONTH)

            return (month == Calendar.DECEMBER && day == 31) || (month == Calendar.JANUARY && day == 1)
        }

    val moritzBirthday: Boolean
        get() {
            val calendar = currentCalendar
            return calendar.get(Calendar.MONTH) == Calendar.JANUARY && calendar.get(Calendar.DAY_OF_MONTH) == 15
        }
    val bixilonBirthday: Boolean
        get() {
            val calendar = currentCalendar
            return calendar.get(Calendar.MONTH) == Calendar.FEBRUARY && calendar.get(Calendar.DAY_OF_MONTH) == 26
        }

    val winter: Boolean
        get() {
            val calendar = currentCalendar
            val month = calendar.get(Calendar.MONTH)
            return month == Calendar.DECEMBER || month == Calendar.JANUARY || month == Calendar.FEBRUARY
        }
}
