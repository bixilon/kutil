/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.json

import de.bixilon.kutil.cast.CastUtil.nullCast
import de.bixilon.kutil.cast.CastUtil.unsafeCast

object JsonUtil {

    fun Any?.asJsonObject(): JsonObject {
        return this.unsafeCast()
    }

    fun Any?.toJsonObject(): JsonObject? {
        return this?.nullCast()
    }

    fun Any?.asMutableJsonObject(): MutableJsonObject {
        return this.unsafeCast()
    }

    fun Any?.toMutableJsonObject(): MutableJsonObject? {
        return this?.nullCast()
    }

    fun Any?.asJsonList(): List<Any?> {
        return this.unsafeCast()
    }

    fun Any?.toJsonList(): List<Any?>? {
        return this?.nullCast()
    }
}
