/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.hex

import de.bixilon.kutil.exception.ExceptionUtil.catchAll
import de.bixilon.kutil.string.StringUtil.mutable

object HexUtil {

    val String.isHexStringFast: Boolean
        get() {
            val length = length
            val bytes = this.mutable()
            if (length != bytes.size) return false // not ascii encoded
            for (char in bytes) {
                if (char.toInt().fromHex() < 0) return false
            }
            return true
        }

    val String.isHexStringSafe: Boolean
        get() {
            val stream = codePoints().iterator()
            while (stream.hasNext()) {
                val code = stream.nextInt()
                when (code) {
                    in '0'.code..'9'.code -> Unit
                    in 'a'.code..'f'.code -> Unit
                    in 'A'.code..'F'.code -> Unit
                    else -> return false
                }
            }
            return true
        }

    val String.isHexString: Boolean
        get() {
            catchAll { return isHexStringFast }
            return isHexStringSafe // actually reachable, because isHexStringFast can throw an exception
        }

    fun Byte.fromHex() = when (this) {
        in '0'.code..'9'.code -> this - '0'.code
        in 'a'.code..'f'.code -> this - ('a'.code - 0x0A)
        in 'A'.code..'F'.code -> this - ('A'.code - 0x0A)
        else -> -1
    }

    fun Int.fromHex() = when (this) {
        in '0'.code..'9'.code -> this - '0'.code
        in 'a'.code..'f'.code -> this - ('a'.code - 0x0A)
        in 'A'.code..'F'.code -> this - ('A'.code - 0x0A)
        else -> -1
    }

    fun String.fromHexString(): ByteArray {
        if (this.length % 2 != 0) throw IllegalArgumentException("Odd length? $length")
        val output = ByteArray(this.length / 2)

        val bytes = this.mutable()

        for (i in bytes.indices step 2) {
            val byte = bytes[i].fromHex() shl 4 or bytes[i + 1].fromHex()
            if (byte < 0) throw IllegalArgumentException("Invalid hex string at index $i!")
            output[i / 2] = byte.toByte()
        }

        return output
    }
}
