package de.bixilon.kutil.file.watcher

import java.nio.file.Path
import java.nio.file.WatchEvent.Kind

fun interface WatcherCallback {

    fun onFileChange(event: Kind<Path>, path: Path)
}
