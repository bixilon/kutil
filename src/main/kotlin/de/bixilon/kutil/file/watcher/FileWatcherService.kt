/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.file.watcher

import de.bixilon.kutil.cast.CastUtil.nullCast
import de.bixilon.kutil.cast.CastUtil.unsafeCast
import de.bixilon.kutil.concurrent.lock.Lock
import de.bixilon.kutil.concurrent.pool.DefaultThreadPool
import de.bixilon.kutil.exception.ExceptionUtil.ignoreAll
import de.bixilon.kutil.latch.AbstractLatch
import de.bixilon.kutil.latch.SimpleLatch
import java.nio.file.FileSystems
import java.nio.file.Path
import java.nio.file.WatchEvent.Kind
import java.nio.file.WatchKey
import java.nio.file.WatchService


object FileWatcherService {
    private val lock = Lock.lock()
    private val path: MutableMap<WatchKeyPair, Watcher> = hashMapOf()
    private val key: MutableMap<WatchKey, Watcher> = hashMapOf()
    private var stop = false
    private var service: WatchService? = null
    private lateinit var thread: Thread

    fun start(): WatchService {
        lock.lock()
        if (service != null) {
            lock.unlock()
            return this.service!!
        }
        this.stop = false
        val latch = SimpleLatch(1)
        this.thread = Thread({ startThread(latch) }, "FileWatcherService")
        thread.start()


        latch.await()
        lock.unlock()
        return this.service!!
    }

    private fun startThread(latch: AbstractLatch) {
        try {
            val service = FileSystems.getDefault().newWatchService()
            this.service = service
            latch.dec()
            while (true) {
                val key = service.take()
                if (stop) break

                lock.lock()
                ignoreAll { key.poll() }
                lock.unlock()

                key.reset()
            }

            for (key in key.keys) {
                key.cancel()
            }
            key.clear()
            path.clear()
        } catch (ignored: InterruptedException) {
        } finally {
            if (latch.count > 0) {
                latch.dec()
            }
            this.service = null
        }
    }

    private fun WatchKey.poll() {
        val watcher = key[this] ?: return
        watcher.lock.acquire()
        val events = pollEvents()
        for (event in events) {
            var path = event.context().nullCast<Path>() ?: continue
            if (!path.startsWith(watcher.path)) {
                path = watcher.path.resolve(path)
            }

            for (directory in watcher.directory) {
                ignoreAll { directory.onFileChange(event.kind().unsafeCast(), path) }
            }
            val filename = path.fileName?.toString() ?: return
            val files = watcher.files[filename] ?: continue

            for (file in files) {
                ignoreAll { file.onFileChange(event.kind().unsafeCast(), path) }
            }
        }
        watcher.lock.release()
    }

    fun watchAsync(path: Path, kind: Set<Kind<Path>>, callback: WatcherCallback) {
        DefaultThreadPool += { watch(path, kind, callback) }
    }

    @Synchronized
    fun watch(path: Path, kind: Set<Kind<Path>>, callback: WatcherCallback) {
        val service = this.service ?: start()
        var file = path.toFile()
        if (file.isFile) {
            // only directories allowed
            file = file.parentFile
        }
        if (!file.isDirectory) {
            throw IllegalStateException("Can not watch directory: $file")
        }

        val keyPair = WatchKeyPair(file.toPath(), kind)
        lock.lock()
        var watcher = this.path[keyPair]
        if (watcher == null) {
            val key = keyPair.path.register(service, kind.toTypedArray())
            watcher = Watcher(key, keyPair.path)
            this.key[key] = watcher
            this.path[keyPair] = watcher
        }
        watcher.lock.lock()
        if (keyPair.path == path) {
            watcher.directory += callback
        } else {
            val filename = path.fileName.toString()
            watcher.files.getOrPut(filename) { mutableListOf() } += callback
        }
        watcher.lock.unlock()
        lock.unlock()
    }

    fun stop() {
        if (service == null) {
            return
        }
        stop = true
        thread.interrupt()
    }
}
