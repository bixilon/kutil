package de.bixilon.kutil.file.watcher

import java.nio.file.Path
import java.nio.file.WatchEvent

data class WatchKeyPair(
    val path: Path,
    val kinds: Set<WatchEvent.Kind<Path>>,
)
