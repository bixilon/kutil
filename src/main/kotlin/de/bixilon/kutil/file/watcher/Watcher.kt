package de.bixilon.kutil.file.watcher

import de.bixilon.kutil.concurrent.lock.RWLock
import java.nio.file.Path
import java.nio.file.WatchKey

class Watcher(
    val key: WatchKey,
    val path: Path,
    val directory: MutableList<WatcherCallback> = mutableListOf(),
    val files: MutableMap<String, MutableList<WatcherCallback>> = mutableMapOf(),
) {
    val lock = RWLock.rwlock()
}
