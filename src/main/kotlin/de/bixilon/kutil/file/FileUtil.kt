/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.file

import de.bixilon.kutil.stream.InputStreamUtil.readAsString
import de.bixilon.kutil.string.StringUtil
import java.io.File
import java.io.FileInputStream
import java.io.FileWriter
import java.io.IOException
import java.nio.charset.Charset

object FileUtil {
    val LINE_SEPARATOR = System.lineSeparator() ?: "\n"


    fun safeSaveToFile(destination: File, content: String) {
        val parent = destination.parentFile
        if (!parent.exists()) {
            parent.mkdirs()
            if (!parent.isDirectory) {
                throw IOException("Could not create folder: ${parent.path}")
            }
        }

        val tempFile = File("${destination.path}.tmp")
        if (tempFile.exists()) {
            if (!tempFile.delete()) {
                throw IOException("Could not delete $tempFile!")
            }
        }
        FileWriter(tempFile).apply {
            write(content)
            close()
        }
        if (destination.exists() && !destination.delete()) {
            throw IOException("Could not delete $destination!")
        }
        if (!tempFile.renameTo(destination)) {
            throw IOException("Could not move $tempFile to $destination!")
        }
    }

    val File.slashPath: String
        get() = absolutePath.replace('\\', '/')

    fun File.read(charset: Charset = StringUtil.DEFAULT_CHARSET): String {
        return FileInputStream(this).readAsString(charset)
    }

    @Deprecated("mkdirParent", replaceWith = ReplaceWith("mkdirParent()"))
    fun File.createParent() = mkdirParent()

    fun File.mkdirParent(): Boolean {
        val parent = parentFile
        if (parent.exists()) {
            return false
        }
        parent.mkdirs()
        return true
    }
}
