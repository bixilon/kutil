/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.collections.iterator.async

import de.bixilon.kutil.concurrent.lock.Lock

class SynchronizedIterator<T>(val iterator: Iterator<T>) {
    private val lock = Lock.lock()
    private var ended = false


    fun getOrNull(): T? {
        if (ended) return null
        lock.lock()
        val hasNext = iterator.hasNext()
        if (!hasNext) {
            this.ended = true
            lock.unlock()
            return null
        }
        val next = iterator.next()
        lock.unlock()
        return next
    }


    companion object {

        fun <T> Iterator<T>.sync() = SynchronizedIterator(this)
    }
}
