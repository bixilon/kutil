/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.collections.iterator.async

import de.bixilon.kutil.collections.iterator.AsyncIterator
import de.bixilon.kutil.concurrent.pool.DefaultThreadPool
import de.bixilon.kutil.concurrent.pool.ThreadPool
import de.bixilon.kutil.concurrent.pool.runnable.ForcePooledRunnable
import de.bixilon.kutil.concurrent.pool.runnable.SimplePoolRunnable
import de.bixilon.kutil.latch.SimpleLatch
import java.util.concurrent.ArrayBlockingQueue
import java.util.concurrent.TimeUnit


class QueuedIterator<T>(
    var iterator: Iterator<T>,
    val pool: ThreadPool = DefaultThreadPool,
    val priority: Int = ThreadPool.NORMAL,
    val threads: Int = pool.threadCount,
    val queueSize: Int = 1000,
) : AsyncIterator<T> {
    private val queue: ArrayBlockingQueue<T> by lazy { ArrayBlockingQueue(queueSize) }
    private var fresh: Boolean = true

    fun reuse(iterator: Iterator<T>) {
        this.iterator = iterator
        fresh = true
    }


    private fun ArrayBlockingQueue<T>.offer(latch: SimpleLatch, iterator: Iterator<T>) {
        // async add all elements to queue
        // TODO: this could lead do deadlocks
        pool += ForcePooledRunnable(ThreadPool.HIGH) {
            latch.dec()
            for (entry in iterator) {
                while (!offer(entry)) {
                    Thread.sleep(1)
                }
            }
            latch.dec()
        }
    }

    private fun ArrayBlockingQueue<T>.work(latch: SimpleLatch, running: SimpleLatch, executor: (T) -> Unit) {
        while (true) {
            val element = poll(1L, TimeUnit.MICROSECONDS)
            if (element == null) {
                if (latch._count == 0) break  // done adding
                continue
            }
            executor(element)
        }
        running.dec()
    }

    private fun ArrayBlockingQueue<T>.work(latch: SimpleLatch, threads: Int, executor: (T) -> Unit) {
        val running = SimpleLatch(threads)
        for (i in 0 until running.count) {
            pool += SimplePoolRunnable(priority) {
                work(latch, running, executor)
            }
        }

        running.await()
    }

    override fun iterate(executor: (T) -> Unit) {
        if (!fresh) throw IllegalStateException("Iterator is not fresh anymore!")
        fresh = false

        if (!iterator.hasNext()) return

        val capacity = queue.remainingCapacity()

        val latch = SimpleLatch(2)

        queue.offer(latch, iterator)
        latch.waitIfGreater(1) // adding started
        queue.work(latch, if (capacity < threads) capacity else threads, executor)
    }
}
