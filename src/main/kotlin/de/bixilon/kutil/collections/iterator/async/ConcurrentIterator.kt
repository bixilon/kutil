/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.collections.iterator.async

import de.bixilon.kutil.collections.iterator.AsyncIterator
import de.bixilon.kutil.concurrent.pool.DefaultThreadPool
import de.bixilon.kutil.concurrent.pool.ThreadPool
import de.bixilon.kutil.concurrent.pool.runnable.ForcePooledRunnable
import de.bixilon.kutil.exception.ExceptionUtil.ignoreAll
import de.bixilon.kutil.latch.SimpleLatch


class ConcurrentIterator<T>(
    var iterator: SynchronizedIterator<T>,
    val pool: ThreadPool = DefaultThreadPool,
    val priority: Int = ThreadPool.NORMAL,
    val threads: Int = pool.threadCount,
) : AsyncIterator<T> {
    private var fresh: Boolean = true

    constructor(iterator: Iterator<T>, pool: ThreadPool = DefaultThreadPool, priority: Int = ThreadPool.NORMAL, threads: Int = pool.threadCount) : this(SynchronizedIterator(iterator), pool, priority, threads)


    private fun execute(executor: (T) -> Unit) {
        while (true) {
            val entry = iterator.getOrNull() ?: break
            ignoreAll { executor.invoke(entry) }
        }
    }

    private fun startThread(latch: SimpleLatch, executor: (T) -> Unit) {
        pool += ForcePooledRunnable(priority = priority) {
            latch.inc()
            execute(executor)
            latch.dec()
        }
    }

    override fun iterate(executor: (T) -> Unit) {
        if (!fresh) throw IllegalStateException("Iterator is not fresh anymore!")
        fresh = false

        if (!iterator.iterator.hasNext()) return

        val latch = SimpleLatch(0)

        for (thread in 0 until threads) {
            startThread(latch, executor)
        }
        execute(executor) // work on this thread too
        latch.await()
    }
}
