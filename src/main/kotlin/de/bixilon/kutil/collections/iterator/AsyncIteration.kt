package de.bixilon.kutil.collections.iterator

import de.bixilon.kutil.collections.iterator.async.ConcurrentIterator
import de.bixilon.kutil.collections.spliterator.async.ConcurrentSpliterator
import de.bixilon.kutil.concurrent.pool.ThreadPool

object AsyncIteration {

    fun <E> List<E>.async(priority: Int = ThreadPool.NORMAL, executor: (E) -> Unit) {
        return ConcurrentSpliterator(this.spliterator(), priority = priority).iterate(executor)
    }

    fun <E> Set<E>.async(priority: Int = ThreadPool.NORMAL, executor: (E) -> Unit) {
        return ConcurrentSpliterator(this.spliterator(), priority = priority).iterate(executor)
    }

    fun <E> Collection<E>.async(priority: Int = ThreadPool.NORMAL, executor: (E) -> Unit) {
        return ConcurrentIterator(this.iterator(), priority = priority).iterate(executor)
    }

    fun <E> Iterable<E>.async(priority: Int = ThreadPool.NORMAL, executor: (E) -> Unit) {
        return ConcurrentIterator(this.iterator(), priority = priority).iterate(executor)
    }
}
