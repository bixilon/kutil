package de.bixilon.kutil.collections.iterator

class RingIterator<T>(
    val collection: Iterable<T>,
) : Iterator<T> {
    private var iterator = collection.iterator()

    private fun update() {
        if (iterator.hasNext()) return
        iterator = collection.iterator()
    }

    override fun hasNext(): Boolean {
        update()
        return iterator.hasNext()
    }

    override fun next(): T {
        update()
        return iterator.next()
    }


    companion object {

        fun <T> Iterable<T>.ringIterator(): RingIterator<T> = RingIterator(this)
    }
}
