/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.collections.map.creator

import java.util.Collections.singletonMap

object MapCreator : AbstractMapCreator {

    override fun <K, V> uMutableMapOf() = hashMapOf<K, V>()
    override fun <K, V> uMutableMapOf(vararg pairs: Pair<K, V>) = hashMapOf(*pairs)
    override fun <K, V> uMapOf(pairs: Pair<K, V>): Map<K, V> = singletonMap(pairs.first, pairs.second)
    override fun <K, V> uMapOf(vararg pairs: Pair<K, V>): Map<K, V> = if (pairs.size > 1) hashMapOf(*pairs) else emptyMap()

    override fun <K, V> sMutableMapOf(): MutableMap<K, V> = linkedMapOf()
    override fun <K, V> sMutableMapOf(vararg pairs: Pair<K, V>): MutableMap<K, V> = linkedMapOf(*pairs)
    override fun <K, V> sMapOf(pairs: Pair<K, V>): Map<K, V> = linkedMapOf(pairs)
    override fun <K, V> sMapOf(vararg pairs: Pair<K, V>): Map<K, V> = linkedMapOf(*pairs)
}
