/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.collections.map.bi

import de.bixilon.kutil.collections.map.creator.MapCreator.uMutableMapOf

class MutableBiMap<K, V>(
    private val key2Value: MutableMap<K, V> = uMutableMapOf(),
) : AbstractMutableBiMap<K, V> {
    private val value2Key: MutableMap<V, K>
    override val size: Int by key2Value::size
    override val entries: MutableSet<MutableMap.MutableEntry<K, V>> by key2Value::entries
    override val keys: MutableSet<K> by key2Value::keys
    override val values: MutableCollection<V> by key2Value::values

    init {
        val value2Key: MutableMap<V, K> = uMutableMapOf()
        for ((key, value) in key2Value) {
            value2Key.put(value, key)?.let { throw IllegalArgumentException("Value duplicated: $value (key=$key)") }
        }
        this.value2Key = value2Key
    }

    override fun containsKey(key: K): Boolean {
        return key2Value.containsKey(key)
    }

    override fun containsValue(value: V): Boolean {
        return value2Key.containsKey(value)
    }

    override fun get(key: K): V? {
        return key2Value[key]
    }

    override fun getValue(key: K): V? {
        return key2Value[key]
    }

    override fun getKey(value: V): K? {
        return value2Key[value]
    }

    override fun isEmpty(): Boolean {
        return key2Value.isEmpty()
    }

    override fun clear() {
        key2Value.clear()
        value2Key.clear()
    }

    override fun put(key: K, value: V): V? {
        val previous = value2Key.putIfAbsent(value, key)
        if (previous != null && key != previous) {
            throw IllegalArgumentException("Duplicated value: $value at $key")
        }
        return key2Value.put(key, value)
    }

    override fun putAll(from: Map<out K, V>) {
        for ((key, value) in from) {
            this[key] = value
        }
    }

    override fun remove(key: K): V? {
        val removed = key2Value.remove(key)
        value2Key.remove(removed)
        return removed
    }

    @JvmName("removeValue2")
    fun remove(value: V): K? {
        val removed = value2Key.remove(value)
        key2Value.remove(removed)
        return removed
    }

    override fun removeValue(value: V): K? {
        val removed = value2Key.remove(value)
        key2Value.remove(removed)
        return removed
    }

    override fun toString(): String {
        return key2Value.toString()
    }

    override fun hashCode(): Int {
        return key2Value.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        if (other !is MutableBiMap<*, *>) return false // ToDo: abstract bimap
        return value2Key == other.value2Key
    }
}
