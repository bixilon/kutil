/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.collections.map

import de.bixilon.kutil.collections.CollectionUtil.toSynchronizedList
import de.bixilon.kutil.collections.CollectionUtil.toSynchronizedSet
import de.bixilon.kutil.collections.map.creator.MapCreator.uMutableMapOf
import de.bixilon.kutil.concurrent.lock.RWLock
import java.util.function.BiConsumer
import java.util.function.BiFunction
import java.util.function.Function

@Suppress("DEPRECATION")
open class LockMap<K, V>(
    open val unsafe: MutableMap<K, V> = uMutableMapOf(),
    @Transient val lock: RWLock = RWLock.rwlock(),
) : MutableMap<K, V> {
    override val size: Int
        get() {
            lock.acquire()
            val returnValue = unsafe.size
            lock.release()
            return returnValue
        }

    override fun containsKey(key: K): Boolean {
        lock.acquire()
        val returnValue = unsafe.containsKey(key)
        lock.release()
        return returnValue
    }

    override fun containsValue(value: V): Boolean {
        lock.acquire()
        val returnValue = unsafe.containsValue(value)
        lock.release()
        return returnValue
    }

    override fun get(key: K): V? {
        lock.acquire()
        val returnValue = unsafe[key]
        lock.release()
        return returnValue
    }

    override fun isEmpty(): Boolean {
        lock.acquire()
        val returnValue = unsafe.isEmpty()
        lock.release()
        return returnValue
    }

    override val entries: MutableSet<MutableMap.MutableEntry<K, V>>
        get() {
            lock.acquire()
            val returnValue = unsafe.entries.toSynchronizedSet()
            lock.release()
            return returnValue
        }
    override val keys: MutableSet<K>
        get() {
            lock.acquire()
            val returnValue = unsafe.keys.toSynchronizedSet()
            lock.release()
            return returnValue
        }
    override val values: MutableCollection<V>
        get() {
            lock.acquire()
            val returnValue = unsafe.values.toSynchronizedList()
            lock.release()
            return returnValue
        }

    override fun clear() {
        lock.lock()
        unsafe.clear()
        lock.unlock()
    }

    override fun put(key: K, value: V): V? {
        lock.lock()
        val returnValue = unsafe.put(key, value)
        lock.unlock()
        return returnValue
    }

    override fun putAll(from: Map<out K, V>) {
        lock.lock()
        val returnValue = unsafe.putAll(from)
        lock.unlock()
        return returnValue
    }

    override fun remove(key: K): V? {
        lock.lock()
        val returnValue = unsafe.remove(key)
        lock.unlock()
        return returnValue
    }

    override fun hashCode(): Int {
        lock.acquire()
        val returnValue = unsafe.hashCode()
        lock.release()
        return returnValue
    }

    override fun toString(): String {
        lock.acquire()
        val returnValue = unsafe.toString()
        lock.release()
        return returnValue
    }

    override fun putIfAbsent(key: K, value: V): V? {
        lock.lock()
        val returnValue = unsafe.putIfAbsent(key, value)
        lock.unlock()
        return returnValue
    }

    override fun forEach(action: BiConsumer<in K, in V>) {
        lock.acquire()
        val returnValue = unsafe.forEach(action)
        lock.release()
        return returnValue
    }

    override fun getOrDefault(key: K, defaultValue: V): V {
        lock.acquire()
        val returnValue = unsafe.getOrDefault(key, defaultValue)
        lock.release()
        return returnValue
    }

    fun synchronizedGetOrPut(key: K, defaultValue: () -> V): V {
        try {
            lock.lock()
            var value = unsafe[key]
            if (value != null) return value
            value = defaultValue()
            unsafe[key] = value
            return value
        } finally {
            lock.unlock()
        }
    }

    @Deprecated("Use explicit synchronized!", ReplaceWith("synchronizedGetOrPut(key, defaultValue)"))
    fun getOrPut(key: K, defaultValue: () -> V): V {
        return synchronizedGetOrPut(key, defaultValue)
    }

    override fun remove(key: K, value: V): Boolean {
        lock.lock()
        val returnValue = unsafe.remove(key, value)
        lock.unlock()
        return returnValue
    }

    override fun equals(other: Any?): Boolean {
        lock.acquire()
        val returnValue = unsafe == other
        lock.release()
        return returnValue
    }

    override fun replaceAll(function: BiFunction<in K, in V, out V>) {
        lock.lock()
        val returnValue = unsafe.replaceAll(function)
        lock.unlock()
        return returnValue
    }

    override fun compute(key: K, remappingFunction: BiFunction<in K, in V?, out V?>): V? {
        lock.acquire()
        val returnValue = unsafe.compute(key, remappingFunction)
        lock.release()
        return returnValue
    }

    override fun computeIfAbsent(key: K, mappingFunction: Function<in K, out V>): V {
        lock.acquire()
        val returnValue = unsafe.computeIfAbsent(key, mappingFunction)
        lock.release()
        return returnValue
    }

    override fun computeIfPresent(key: K, remappingFunction: BiFunction<in K, in V & Any, out V?>): V? {
        lock.acquire()
        val returnValue = unsafe.computeIfPresent(key, remappingFunction)
        lock.release()
        return returnValue
    }

    override fun replace(key: K, value: V): V? {
        lock.lock()
        val returnValue = unsafe.replace(key, value)
        lock.unlock()
        return returnValue
    }

    override fun merge(key: K, value: V & Any, remappingFunction: BiFunction<in V & Any, in V & Any, out V?>): V? {
        lock.lock()
        val returnValue = unsafe.merge(key, value, remappingFunction)
        lock.unlock()
        return returnValue
    }

    override fun replace(key: K, oldValue: V, newValue: V): Boolean {
        lock.lock()
        val returnValue = unsafe.replace(key, oldValue, newValue)
        lock.unlock()
        return returnValue
    }
}
