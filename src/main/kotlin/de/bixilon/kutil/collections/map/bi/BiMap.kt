/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.collections.map.bi

import de.bixilon.kutil.collections.map.creator.MapCreator.uMutableMapOf

class BiMap<K, V>(
    private val key2Value: Map<K, V> = emptyMap(),
) : AbstractBiMap<K, V> {
    private val value2Key: Map<V, K>
    override val entries: Set<Map.Entry<K, V>> by key2Value::entries
    override val keys: Set<K> by key2Value::keys
    override val size: Int by key2Value::size
    override val values: Collection<V> by key2Value::values

    init {
        val value2Key: MutableMap<V, K> = uMutableMapOf()
        for ((key, value) in key2Value) {
            value2Key.put(value, key)?.let { throw IllegalArgumentException("Value duplicated: $value (key=$key)") }
        }
        this.value2Key = value2Key
    }

    override fun containsKey(key: K): Boolean {
        return key2Value.containsKey(key)
    }

    override fun containsValue(value: V): Boolean {
        return value2Key.containsKey(value)
    }

    override fun get(key: K): V? {
        return key2Value[key]
    }

    override fun getKey(value: V): K? {
        return value2Key[value]
    }

    override fun getValue(key: K): V? {
        return key2Value[key]
    }

    override fun isEmpty(): Boolean {
        return key2Value.isEmpty()
    }

    override fun toString(): String {
        return value2Key.toString()
    }

    override fun hashCode(): Int {
        return value2Key.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        if (other !is BiMap<*, *>) return false // ToDo: abstract bimap
        return value2Key == other.value2Key
    }
}
