package de.bixilon.kutil.collections.map

import de.bixilon.kutil.observer.map.ObservedMap

object MapUtil {

    private fun <K, V> MutableMap<K, V>.retainIterator(values: Map<K, V>) {
        val iterator = this.iterator()
        while (iterator.hasNext()) {
            val key = iterator.next().key
            if (key in values) continue
            iterator.remove()
        }
    }

    private fun <K, V> MutableMap<K, V>.retainSet(values: Map<K, V>) {
        val iterator = this.iterator()
        val remove: MutableSet<K> = HashSet(this.size - values.size)
        while (iterator.hasNext()) {
            val key = iterator.next().key
            if (key in values) continue
            remove += key
        }

        for (entry in remove) { // TODO: Optimize, but trigger map changes
            this.remove(entry)
        }
    }

    fun <K, V> MutableMap<K, V>.replace(values: Map<K, V>) {
        if (values.isEmpty()) return this.clear()
        this.putAll(values)

        if (this.size == values.size) return

        // observed map does not support iterator removing
        if (this is ObservedMap<K, V>) {
            retainSet(values)
        } else {
            retainIterator(values)
        }
    }


    fun <K, V> Map<K, V>.mut(): MutableMap<K, V> {
        if (this is MutableMap<K, V>) return this

        return this.toMutableMap()
    }
}
