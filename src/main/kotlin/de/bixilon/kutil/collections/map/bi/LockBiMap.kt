/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.collections.map.bi

import de.bixilon.kutil.collections.map.LockMap

@Suppress("DEPRECATION")
class LockBiMap<K, V>(
    override val unsafe: AbstractMutableBiMap<K, V>,
) : LockMap<K, V>(unsafe), AbstractMutableBiMap<K, V> {

    override fun getKey(value: V): K? {
        lock.acquire()
        val key = unsafe.getKey(value)
        lock.release()
        return key
    }

    override fun getValue(key: K): V? {
        lock.acquire()
        val value = unsafe.getValue(key)
        lock.release()
        return value
    }

    override fun removeValue(value: V): K? {
        lock.acquire()
        val key = unsafe.removeValue(value)
        lock.release()
        return key
    }
}
