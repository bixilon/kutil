/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.collections.map

import de.bixilon.kutil.collections.CollectionUtil.toSynchronizedList
import de.bixilon.kutil.collections.CollectionUtil.toSynchronizedSet
import de.bixilon.kutil.collections.map.creator.MapCreator.uMutableMapOf
import java.util.function.BiConsumer
import java.util.function.BiFunction
import java.util.function.Function

open class SynchronizedMap<K, V>(
    open val unsafe: MutableMap<K, V> = uMutableMapOf(),
) : MutableMap<K, V> {
    val lock = Object()
    override val size: Int
        get() = synchronized(lock) { unsafe.size }

    override fun containsKey(key: K): Boolean {
        synchronized(lock) {
            return unsafe.containsKey(key)
        }
    }

    override fun containsValue(value: V): Boolean {
        synchronized(lock) {
            return unsafe.containsValue(value)
        }
    }

    override fun get(key: K): V? {
        synchronized(lock) {
            return unsafe[key]
        }
    }

    override fun isEmpty(): Boolean {
        synchronized(lock) {
            return unsafe.isEmpty()
        }
    }

    override val entries: MutableSet<MutableMap.MutableEntry<K, V>>
        get() {
            synchronized(lock) {
                return unsafe.entries.toSynchronizedSet()
            }
        }
    override val keys: MutableSet<K>
        get() {
            synchronized(lock) {
                return unsafe.keys.toSynchronizedSet()
            }
        }
    override val values: MutableCollection<V>
        get() {
            synchronized(lock) {
                return unsafe.values.toSynchronizedList()
            }
        }

    override fun clear() {
        synchronized(lock) {
            unsafe.clear()
        }
    }

    override fun put(key: K, value: V): V? {
        synchronized(lock) {
            return unsafe.put(key, value)
        }
    }

    override fun putAll(from: Map<out K, V>) {
        synchronized(lock) {
            return unsafe.putAll(from)
        }
    }

    override fun remove(key: K): V? {
        synchronized(lock) {
            return unsafe.remove(key)
        }
    }

    override fun hashCode(): Int {
        synchronized(lock) {
            return unsafe.hashCode()
        }
    }

    override fun toString(): String {
        synchronized(lock) {
            return unsafe.toString()
        }
    }

    override fun putIfAbsent(key: K, value: V): V? {
        synchronized(lock) {
            return unsafe.putIfAbsent(key, value)
        }
    }

    override fun forEach(action: BiConsumer<in K, in V>) {
        synchronized(lock) {
            return unsafe.forEach(action)
        }
    }

    override fun getOrDefault(key: K, defaultValue: V): V {
        synchronized(lock) {
            return unsafe.getOrDefault(key, defaultValue)
        }
    }

    fun synchronizedGetOrPut(key: K, defaultValue: () -> V): V {
        synchronized(lock) {
            var value = get(key)
            if (value != null) return value
            value = defaultValue()
            put(key, value)
            return value
        }
    }

    @Deprecated("Use explicit synchronized!", ReplaceWith("synchronizedGetOrPut(key, defaultValue)"))
    fun getOrPut(key: K, defaultValue: () -> V): V {
        return synchronizedGetOrPut(key, defaultValue)
    }

    override fun remove(key: K, value: V): Boolean {
        synchronized(lock) {
            return unsafe.remove(key, value)
        }
    }

    override fun equals(other: Any?): Boolean {
        synchronized(lock) {
            return unsafe == other
        }
    }

    override fun replaceAll(function: BiFunction<in K, in V, out V>) {
        synchronized(lock) {
            return unsafe.replaceAll(function)
        }
    }

    override fun compute(key: K, remappingFunction: BiFunction<in K, in V?, out V?>): V? {
        synchronized(lock) {
            return unsafe.compute(key, remappingFunction)
        }
    }

    override fun computeIfAbsent(key: K, mappingFunction: Function<in K, out V>): V {
        synchronized(lock) {
            return unsafe.computeIfAbsent(key, mappingFunction)
        }
    }

    override fun computeIfPresent(key: K, remappingFunction: BiFunction<in K, in V & Any, out V?>): V? {
        synchronized(lock) {
            return unsafe.computeIfPresent(key, remappingFunction)
        }
    }

    override fun replace(key: K, value: V): V? {
        synchronized(lock) {
            return unsafe.replace(key, value)
        }
    }

    override fun merge(key: K, value: V & Any, remappingFunction: BiFunction<in V & Any, in V & Any, out V?>): V? {
        synchronized(lock) {
            return unsafe.merge(key, value, remappingFunction)
        }
    }

    override fun replace(key: K, oldValue: V, newValue: V): Boolean {
        synchronized(lock) {
            return unsafe.replace(key, oldValue, newValue)
        }
    }
}
