/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.collections.set.creator

interface AbstractSetCreator {

    fun <E> uMutableSetOf(): MutableSet<E>
    fun <E> uMutableSetOf(vararg entries: E): MutableSet<E>
    fun <E> uSetOf(entries: E): Set<E>
    fun <E> uSetOf(vararg entries: E): Set<E>

    fun <E> sMutableSetOf(): MutableSet<E>
    fun <E> sMutableSetOf(vararg entries: E): MutableSet<E>
    fun <E> sSetOf(entries: E): Set<E>
    fun <E> sSetOf(vararg entries: E): Set<E>
}
