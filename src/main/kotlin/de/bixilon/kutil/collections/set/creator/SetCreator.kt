/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.collections.set.creator

import java.util.Collections.singleton

object SetCreator : AbstractSetCreator {

    override fun <E> uMutableSetOf(): MutableSet<E> = hashSetOf()
    override fun <E> uMutableSetOf(vararg entries: E): MutableSet<E> = hashSetOf(*entries)
    override fun <E> uSetOf(entries: E): Set<E> = hashSetOf(entries)
    override fun <E> uSetOf(vararg entries: E): Set<E> = hashSetOf(*entries)

    override fun <E> sMutableSetOf(): MutableSet<E> = linkedSetOf()
    override fun <E> sMutableSetOf(vararg entries: E): MutableSet<E> = linkedSetOf()
    override fun <E> sSetOf(entries: E): Set<E> = singleton(entries)
    override fun <E> sSetOf(vararg entries: E): Set<E> = linkedSetOf(*entries)
}
