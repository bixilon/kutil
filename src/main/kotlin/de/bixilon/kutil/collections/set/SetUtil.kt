package de.bixilon.kutil.collections.set

import de.bixilon.kutil.cast.CastUtil.unsafeCast
import de.bixilon.kutil.enums.BitEnumSet

object SetUtil {

    fun <V> MutableSet<V>.replace(values: Set<V>) {
        this.addAll(values)

        if (this.size == values.size) return

        val iterator = this.iterator()
        while (iterator.hasNext()) {
            val key = iterator.next()
            if (key in values) continue
            iterator.remove()
        }
    }

    fun <T> Set<T>.copy(): MutableSet<T> = when (this) {
        is BitEnumSet<*> -> copy().unsafeCast()
        else -> this.toMutableSet()
    }


    fun <T> Set<T>.mut(): MutableSet<T> {
        if (this is MutableSet<T>) return this

        return this.copy()
    }
}
