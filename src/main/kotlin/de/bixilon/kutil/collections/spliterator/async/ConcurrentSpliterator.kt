/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.collections.spliterator.async

import de.bixilon.kutil.collections.spliterator.SpliteratorUtil.split
import de.bixilon.kutil.concurrent.pool.DefaultThreadPool
import de.bixilon.kutil.concurrent.pool.ThreadPool
import de.bixilon.kutil.concurrent.pool.runnable.SimplePoolRunnable
import de.bixilon.kutil.latch.SimpleLatch
import java.util.*


class ConcurrentSpliterator<T>(
    override var spliterator: Spliterator<T>,
    val pool: ThreadPool = DefaultThreadPool,
    val priority: Int = ThreadPool.NORMAL,
    val threads: Int = pool.threadCount,
) : AsyncSpliterator<T> {
    private var fresh = true


    private fun Spliterator<T>.work(latch: SimpleLatch, executor: (T) -> Unit) {
        forEachRemaining(executor)
        latch.dec()
    }

    private fun List<Spliterator<T>>.work(executor: (T) -> Unit) {
        val latch = SimpleLatch(size)
        for (split in this) {
            pool += SimplePoolRunnable(priority) {
                split.work(latch, executor)
            }
        }
        latch.await()
    }


    override fun iterate(executor: (T) -> Unit) {
        if (!fresh) throw IllegalStateException("Spliterator is not fresh anymore!")
        fresh = false

        val estimated = spliterator.estimateSize()
        if (estimated == 0L) return

        val splits = splits(estimated)

        splits.work(executor)
    }

    private fun splits(estimated: Long): List<Spliterator<T>> {
        var count = estimated.toInt()
        if (count > threads || count < 0) count = threads

        return this.spliterator.split(count, estimated)
    }
}
