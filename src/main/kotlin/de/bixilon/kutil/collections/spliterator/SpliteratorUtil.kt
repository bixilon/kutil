/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.collections.spliterator

import java.util.*

object SpliteratorUtil {

    // thanks https://stackoverflow.com/questions/56925405/java-spliterator-how-to-process-large-stream-splits-equally
    fun <T> Spliterator<T>.split(count: Int, estimated: Long = this.estimateSize()): List<Spliterator<T>> {
        if (count == 1) return listOf(this)

        val deque: Deque<Spliterator<T>> = ArrayDeque(count)
        val out: MutableList<Spliterator<T>> = ArrayList(count)

        deque += this
        val expectedSize = estimated / count

        while (!deque.isEmpty()) {
            var current = deque.pop()
            while (true) {
                if (current.estimateSize() <= expectedSize) break
                val next = current.trySplit() ?: break

                deque.push(current)
                current = next
            }
            out += current
        }

        return out
    }

    fun <T> Spliterator<T>.collect(): List<T> {
        val list: MutableList<T> = mutableListOf()
        forEachRemaining { list += it }

        return list
    }
}
