/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.collections.spliterator.async

import de.bixilon.kutil.concurrent.pool.DefaultThreadPool
import de.bixilon.kutil.concurrent.pool.ThreadPool
import de.bixilon.kutil.concurrent.pool.runnable.ForcePooledRunnable
import de.bixilon.kutil.concurrent.pool.runnable.SimplePoolRunnable
import de.bixilon.kutil.latch.SimpleLatch
import java.util.*
import java.util.concurrent.ArrayBlockingQueue
import java.util.concurrent.TimeUnit


class QueuedSpliterator<T>(
    override var spliterator: Spliterator<T>,
    val pool: ThreadPool = DefaultThreadPool,
    val priority: Int = ThreadPool.NORMAL,
    val threads: Int = pool.threadCount,
    val queueSize: Int = 0,
) : AsyncSpliterator<T> {
    private var queue: ArrayBlockingQueue<T>? = null
    private var fresh: Boolean = true

    fun reuse(spliterator: Spliterator<T>) {
        this.spliterator = spliterator
        fresh = true
    }

    private fun createQueue(estimated: Long): ArrayBlockingQueue<T> {
        var queue = this.queue
        if (queue != null) return queue

        val size = when {
            queueSize > 0 -> queueSize
            estimated < 0 -> 100
            estimated > 1000L -> 1000
            else -> estimated.toInt()
        }

        queue = ArrayBlockingQueue(size)
        this.queue = queue

        return queue
    }


    private fun ArrayBlockingQueue<T>.offer(cacheExceeded: Boolean, latch: SimpleLatch, spliterator: Spliterator<T>) {
        // async add all elements to queue
        val runnable = Runnable {
            latch.dec()
            spliterator.forEachRemaining {
                while (!offer(it)) {
                    Thread.sleep(1)
                }
            }
            latch.dec()
        }
        if (!cacheExceeded) {
            pool += SimplePoolRunnable(ThreadPool.HIGH, runnable)
            return
        }
        // TODO: this could lead do deadlocks
        pool += ForcePooledRunnable(ThreadPool.HIGH, runnable)
    }

    private fun ArrayBlockingQueue<T>.work(latch: SimpleLatch, running: SimpleLatch, executor: (T) -> Unit) {
        while (true) {
            val element = poll(1L, TimeUnit.MICROSECONDS)
            if (element == null) {
                if (latch._count == 0) break  // done adding
                continue
            }
            executor(element)
        }
        running.dec()
    }

    private fun ArrayBlockingQueue<T>.work(latch: SimpleLatch, threads: Int, executor: (T) -> Unit) {
        val running = SimpleLatch(threads)
        for (i in 0 until running.count) {
            pool += SimplePoolRunnable(priority) {
                work(latch, running, executor)
            }
        }

        running.await()
    }

    override fun iterate(executor: (T) -> Unit) {
        if (!fresh) throw IllegalStateException("Spliterator is not fresh anymore!")
        fresh = false

        val estimated = spliterator.estimateSize()
        if (estimated == 0L) return

        val queue = createQueue(estimated)
        val capacity = queue.remainingCapacity()

        val latch = SimpleLatch(2)

        queue.offer(capacity < estimated, latch, spliterator)
        latch.waitIfGreater(1) // adding started
        queue.work(latch, if (capacity < threads) capacity else threads, executor)
    }
}
