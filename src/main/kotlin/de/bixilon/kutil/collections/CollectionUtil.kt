/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.collections

import de.bixilon.kutil.cast.CastUtil.unsafeCast
import de.bixilon.kutil.collections.map.LockMap
import de.bixilon.kutil.collections.map.SynchronizedMap
import de.bixilon.kutil.collections.map.bi.BiMap
import de.bixilon.kutil.collections.map.bi.LockBiMap
import de.bixilon.kutil.collections.map.bi.MutableBiMap
import de.bixilon.kutil.collections.map.bi.SynchronizedBiMap
import de.bixilon.kutil.collections.map.creator.MapCreator.uMutableMapOf
import de.bixilon.kutil.collections.set.creator.SetCreator.uMutableSetOf
import java.util.*

object CollectionUtil {

    inline fun <K, V> biMapOf(vararg pair: Pair<K, V>): BiMap<K, V> {
        return BiMap(uMutableMapOf(*pair))
    }

    inline fun <K, V> mutableBiMapOf(vararg pair: Pair<K, V>): MutableBiMap<K, V> {
        return MutableBiMap(uMutableMapOf(*pair))
    }

    inline fun <K, V> lockBiMapOf(vararg pair: Pair<K, V>): LockBiMap<K, V> {
        return LockBiMap(mutableBiMapOf(*pair))
    }

    inline fun <K, V> synchronizedBiMapOf(vararg pair: Pair<K, V>): SynchronizedBiMap<K, V> {
        return SynchronizedBiMap(mutableBiMapOf(*pair))
    }

    inline fun <K, V> lockMapOf(vararg pairs: Pair<K, V>): LockMap<K, V> {
        return LockMap(uMutableMapOf(*pairs))
    }

    inline fun <K, V> synchronizedMapOf(vararg pairs: Pair<K, V>): SynchronizedMap<K, V> {
        return SynchronizedMap(uMutableMapOf(*pairs))
    }

    inline fun <V> synchronizedSetOf(vararg values: V): MutableSet<V> {
        return Collections.synchronizedSet(uMutableSetOf(*values))
    }

    inline fun <V> synchronizedListOf(vararg values: V): MutableList<V> {
        return Collections.synchronizedList(mutableListOf(*values))
    }

    private fun <K> Any.synchronizedCopy(lock: Any? = null, copier: () -> K): K {
        val ret: K
        synchronized(lock ?: this) {
            ret = copier()
        }
        return ret
    }

    fun <K, V> Map<K, V>.toSynchronizedMap(): SynchronizedMap<K, V> {
        return when (this) {
            is LockMap<*, *> -> {
                lock.acquire()
                val map: SynchronizedMap<K, V> = SynchronizedMap(this.toMutableMap()).unsafeCast()
                lock.release()
                map
            }

            is SynchronizedMap<*, *> -> {
                val map: SynchronizedMap<K, V>
                synchronized(this.lock) {
                    map = SynchronizedMap(this.toMutableMap()).unsafeCast()
                }
                map
            }

            else -> synchronizedCopy { SynchronizedMap(this.toMutableMap()) }
        }
    }

    fun <V> Collection<V>.toSynchronizedList(): MutableList<V> {
        return synchronizedCopy { Collections.synchronizedList(this.toMutableList()) }
    }

    fun <V> Collection<V>.toSynchronizedSet(): MutableSet<V> {
        return synchronizedCopy { Collections.synchronizedSet(this.toMutableSet()) }
    }


    fun <K, V> Map<K, Any>.extend(vararg pairs: Pair<K, Any>): Map<K, V> {
        val map: MutableMap<K, V> = mutableMapOf()

        for ((key, value) in this) {
            map[key] = value.unsafeCast()
        }

        for (pair in pairs) {
            map[pair.first] = pair.second.unsafeCast()
        }
        return map
    }

    fun <V> Collection<V>.extend(vararg values: Any): List<V> {
        val list: MutableList<V> = mutableListOf()

        for (value in this) {
            list += value.unsafeCast<V>()
        }

        fun add(value: Any?) {
            when (value) {
                is Collection<*> -> {
                    for (element in value) {
                        add(element)
                    }
                }

                else -> list += value.unsafeCast<V>()
            }
        }

        for (value in values) {
            add(value)
        }
        return list
    }


    inline operator fun <T> List<T>.get(enum: Enum<*>): T {
        return this[enum.ordinal]
    }

    inline operator fun <T> Array<T>.get(enum: Enum<*>): T {
        return this[enum.ordinal]
    }

    fun <T : Collection<*>> T.toNull(): T? {
        if (isEmpty()) {
            return null
        }
        return this
    }

    fun <T> List<T>.next(current: T): T {
        val index = this.indexOf(current)
        check(index >= 0) { "List does not contain $current" }

        if (index == this.size - 1) {
            return this[0]
        }
        return this[index + 1]
    }
}
