/*
 * Minosoft
 * Copyright (C) 2020-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * This software is not affiliated with Mojang AB, the original developer of Minecraft.
 */

package de.bixilon.kutil.collections.primitive.floats

import de.bixilon.kutil.collections.primitive.AbstractPrimitiveList
import java.nio.FloatBuffer

abstract class AbstractFloatList : AbstractPrimitiveList<Float>() {

    abstract fun add(value: Float)
    abstract fun add(array: FloatArray)
    operator fun plusAssign(array: FloatArray) = add(array)
    abstract fun add(floatList: AbstractFloatList)
    operator fun plusAssign(floatList: AbstractFloatList) = add(floatList)
    abstract fun add(buffer: FloatBuffer)
    operator fun plusAssign(buffer: FloatBuffer) = add(buffer)

    abstract fun toArray(): FloatArray

    open fun add(value1: Float, value2: Float) {
        add(value1); add(value2)
    }

    open fun add(value1: Float, value2: Float, value3: Float) {
        add(value1); add(value2); add(value3)
    }

    open fun add(value1: Float, value2: Float, value3: Float, value4: Float) {
        add(value1); add(value2); add(value3); add(value4)
    }

    open fun add(value1: Float, value2: Float, value3: Float, value4: Float, value5: Float) {
        add(value1); add(value2); add(value3); add(value4); add(value5)
    }

    open fun add(value1: Float, value2: Float, value3: Float, value4: Float, value5: Float, value6: Float) {
        add(value1); add(value2); add(value3); add(value4); add(value5); add(value6)
    }

    open fun add(value1: Float, value2: Float, value3: Float, value4: Float, value5: Float, value6: Float, value7: Float) {
        add(value1); add(value2); add(value3); add(value4); add(value5); add(value6); add(value7)
    }

    open fun add(value1: Float, value2: Float, value3: Float, value4: Float, value5: Float, value6: Float, value7: Float, value8: Float) {
        add(value1); add(value2); add(value3); add(value4); add(value5); add(value6); add(value7); add(value8)
    }

    open fun add(value1: Float, value2: Float, value3: Float, value4: Float, value5: Float, value6: Float, value7: Float, value8: Float, value9: Float) {
        add(value1); add(value2); add(value3); add(value4); add(value5); add(value6); add(value7); add(value8); add(value9)
    }

    open fun add(value1: Float, value2: Float, value3: Float, value4: Float, value5: Float, value6: Float, value7: Float, value8: Float, value9: Float, value10: Float) {
        add(value1); add(value2); add(value3); add(value4); add(value5); add(value6); add(value7); add(value8); add(value9); add(value10)
    }
}
