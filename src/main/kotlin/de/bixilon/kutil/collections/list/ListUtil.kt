package de.bixilon.kutil.collections.list

object ListUtil {

    fun <T> List<T>.mut(): MutableList<T> {
        if (this is MutableList<T>) return this

        return this.toMutableList()
    }
}
