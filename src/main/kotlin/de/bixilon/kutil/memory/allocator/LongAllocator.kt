package de.bixilon.kutil.memory.allocator

class LongAllocator : TemporaryAllocator<LongArray>() {

    override fun getSize(value: LongArray) = value.size

    override fun create(size: Int) = LongArray(size)
}
