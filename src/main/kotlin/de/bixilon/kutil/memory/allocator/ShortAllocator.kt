package de.bixilon.kutil.memory.allocator

class ShortAllocator : TemporaryAllocator<ShortArray>() {

    override fun getSize(value: ShortArray) = value.size

    override fun create(size: Int) = ShortArray(size)
}
