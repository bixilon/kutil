package de.bixilon.kutil.memory.allocator

class ByteAllocator : TemporaryAllocator<ByteArray>() {

    override fun getSize(value: ByteArray) = value.size

    override fun create(size: Int) = ByteArray(size)
}
