package de.bixilon.kutil.memory.allocator

class IntAllocator : TemporaryAllocator<IntArray>() {

    override fun getSize(value: IntArray) = value.size

    override fun create(size: Int) = IntArray(size)
}
