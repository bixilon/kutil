/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.http

import java.net.URLDecoder
import java.net.URLEncoder
import java.nio.charset.StandardCharsets

object QueryUtil {

    fun Map<*, *>.toQuery(): String {
        val builder = StringBuilder()

        for ((key, value) in this) {
            if (builder.isNotEmpty()) {
                builder.append('&')
            }
            builder.append(URLEncoder.encode(key.toString(), StandardCharsets.UTF_8))
            builder.append('=')
            builder.append(URLEncoder.encode(value.toString(), StandardCharsets.UTF_8))
        }

        return builder.toString()
    }

    fun String.fromQuery(): Map<String, String> {
        val map: MutableMap<String, String> = mutableMapOf()
        for (pair in this.split("&")) {
            val (key, value) = pair.split("=")
            map[URLDecoder.decode(key, StandardCharsets.UTF_8)] = URLDecoder.decode(value, StandardCharsets.UTF_8)
        }
        return map
    }
}
