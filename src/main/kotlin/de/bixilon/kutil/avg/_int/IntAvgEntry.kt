package de.bixilon.kutil.avg._int

import de.bixilon.kutil.avg.AvgEntry
import kotlin.time.ComparableTimeMark

data class IntAvgEntry(
    override val time: ComparableTimeMark,
    val value: Int,
) : AvgEntry
