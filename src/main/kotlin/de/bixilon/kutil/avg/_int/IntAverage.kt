/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.avg._int

import de.bixilon.kutil.avg.Average
import de.bixilon.kutil.cast.CastUtil.unsafeCast
import de.bixilon.kutil.concurrent.lock.Lock
import de.bixilon.kutil.time.Interval
import kotlin.time.ComparableTimeMark
import kotlin.time.TimeSource

class IntAverage(
    override val interval: Interval,
    val default: Int = 0,
) : Average {
    private val lock = Lock.lock()
    private val data: MutableList<IntAvgEntry> = ArrayList()
    private var updated = false
    private var lastAVG = 0

    val avg: Int
        get() {
            lock.lock()
            cleanup()
            if (!updated) {
                lock.unlock()
                return lastAVG
            }
            if (data.size == 0) {
                lock.unlock()
                return this.default
            }

            var total = 0
            for (entry in data) {
                total += entry.value
            }

            lastAVG = total / data.size
            updated = false
            lock.unlock()
            return lastAVG
        }

    private fun cleanup(time: ComparableTimeMark = TimeSource.Monotonic.markNow()) {
        this.updated = this.cleanup(time, this.data.unsafeCast()) || this.updated
    }

    fun add(value: Int) {
        val now = TimeSource.Monotonic.markNow()
        lock.lock()
        cleanup(now)
        data += IntAvgEntry(now, value)
        updated = true
        lock.unlock()
    }

    operator fun plusAssign(value: Int) = add(value)
}
