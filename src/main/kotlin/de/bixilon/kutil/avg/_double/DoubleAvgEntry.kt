package de.bixilon.kutil.avg._double

import de.bixilon.kutil.avg.AvgEntry
import kotlin.time.ComparableTimeMark

data class DoubleAvgEntry(
    override val time: ComparableTimeMark,
    val value: Double,
) : AvgEntry
