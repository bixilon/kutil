package de.bixilon.kutil.avg._float

import de.bixilon.kutil.avg.AvgEntry
import kotlin.time.ComparableTimeMark

data class FloatAvgEntry(
    override val time: ComparableTimeMark,
    val value: Float,
) : AvgEntry
