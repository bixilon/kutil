package de.bixilon.kutil.avg._long

import de.bixilon.kutil.avg.AvgEntry
import kotlin.time.ComparableTimeMark

data class LongAvgEntry(
    override val time: ComparableTimeMark,
    val value: Long,
) : AvgEntry
