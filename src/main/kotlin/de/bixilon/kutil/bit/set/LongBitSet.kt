/*
 * Minosoft
 * Copyright (C) 2020-2023 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * This software is not affiliated with Mojang AB, the original developer of Minecraft.
 */

package de.bixilon.kutil.bit.set

class LongBitSet(private var data: Long = 0L) : AbstractBitSet {

    override operator fun get(index: Int): Boolean {
        checkIndex(index)
        val mask = 1L shl index
        return (data and mask) != 0L
    }

    override operator fun set(index: Int, value: Boolean) {
        checkIndex(index)
        val mask = 1L shl index
        data = if (value) {
            data or mask
        } else {
            data and mask.inv()
        }
    }

    override fun clear() {
        this.data = 0L
    }

    override fun setAll() {
        this.data = -1L
    }

    override fun capacity() = Long.SIZE_BITS

    override fun length(): Int {
        var data = data
        var bit = 0

        while (data != 0L) {
            data = data ushr 1
            bit++
        }
        return bit
    }

    override fun clone(): LongBitSet {
        return LongBitSet(data)
    }

    override fun hashCode(): Int {
        return (data xor (data shr 32)).toInt()
    }

    override fun equals(other: Any?): Boolean {
        if (other !is LongBitSet) return false
        return data == other.data
    }


    private inline fun checkIndex(index: Int) {
        if (index < 0 || index >= Long.SIZE_BITS) throw IndexOutOfBoundsException("Index out of bounds $index")
    }
}
