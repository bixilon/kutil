/*
 * Minosoft
 * Copyright (C) 2020-2023 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * This software is not affiliated with Mojang AB, the original developer of Minecraft.
 */

package de.bixilon.kutil.bit.set


interface AbstractBitSet : Cloneable {

    operator fun get(index: Int): Boolean

    operator fun set(index: Int, value: Boolean)
    fun clear()
    fun setAll()

    fun capacity(): Int
    fun length(): Int

    public override fun clone(): AbstractBitSet


    companion object {

        fun of(capacity: Int) = when {
            capacity < 0 -> throw IllegalArgumentException("Capacity must be non negative: $capacity")
            capacity <= Int.SIZE_BITS -> IntBitSet()
            capacity <= Long.SIZE_BITS -> LongBitSet()
            else -> ArrayBitSet(capacity)
        }
    }
}
