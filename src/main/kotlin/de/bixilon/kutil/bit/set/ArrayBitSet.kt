/*
 * Minosoft
 * Copyright (C) 2020-2023 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * This software is not affiliated with Mojang AB, the original developer of Minecraft.
 */

package de.bixilon.kutil.bit.set

import java.util.*

class ArrayBitSet(private var data: LongArray) : AbstractBitSet {
    private val capacity = data.size * Long.SIZE_BITS

    constructor(capacity: Int) : this(LongArray(capacity + (Long.SIZE_BITS - 1) shr ELEMENT_BITS))

    override operator fun get(index: Int): Boolean {
        checkIndex(index)
        val long = index ushr ELEMENT_BITS
        val data = this.data[long]
        val mask = 1L shl (index and ELEMENT_MASK)
        return (data and mask) != 0L
    }

    override operator fun set(index: Int, value: Boolean) {
        checkIndex(index)
        val long = index ushr ELEMENT_BITS
        val data = this.data[long]
        val mask = 1L shl (index and ELEMENT_MASK)
        this.data[long] = if (value) {
            data or mask
        } else {
            data and mask.inv()
        }
    }

    override fun clear() {
        Arrays.fill(data, 0L)
    }

    override fun setAll() {
        Arrays.fill(data, -1)
    }

    override fun capacity() = capacity

    override fun length(): Int {
        for ((index, long) in data.withIndex()) {
            if (long == 0L) continue
            val offset = (this.data.size - index - 1) * Long.SIZE_BITS
            var bit = 0
            var data = long

            while (data != 0L) {
                data = data ushr 1
                bit++
            }
            return bit + offset

        }
        return 0
    }

    override fun clone(): ArrayBitSet {
        return ArrayBitSet(data.clone())
    }

    override fun hashCode(): Int {
        return data.contentHashCode()
    }

    override fun equals(other: Any?): Boolean {
        if (other !is ArrayBitSet) return false
        return data.contentEquals(other.data)
    }


    private inline fun checkIndex(index: Int) {
        if (index < 0 || index >= capacity) throw IndexOutOfBoundsException("Index out of bounds $index")
    }

    companion object {
        const val ELEMENT_BITS = 6
        const val ELEMENT_MASK = Long.SIZE_BITS - 1
    }
}
