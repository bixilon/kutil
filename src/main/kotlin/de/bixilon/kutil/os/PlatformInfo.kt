/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package de.bixilon.kutil.os

object PlatformInfo {
    @JvmStatic
    val OS: OSTypes

    @JvmStatic
    val ARCHITECTURE: Architectures

    init {
        val os = System.getProperty("os.name").lowercase()
        OS = when {
            os.startsWith("win") -> OSTypes.WINDOWS
            os.startsWith("linux") -> OSTypes.LINUX
            os.startsWith("mac") -> OSTypes.MAC
            os.contains("aix") || os.contains("nix") -> OSTypes.UNIX
            os.contains("sunos") -> OSTypes.SOLARIS
            else -> {
                System.err.println("Can not determinate operating system: $os")
                OSTypes.OTHER
            }
        }

        ARCHITECTURE = when (val architecture = System.getProperty("os.arch").lowercase()) {
            "amd64", "x86_64" -> Architectures.AMD64
            "x86", "i386", "i486", "i586", "i686" -> Architectures.X86
            "arm" -> Architectures.ARM
            "aarch64" -> Architectures.AARCH64
            "powerpc", "ppc" -> Architectures.POWERPC
            "ia64" -> Architectures.IA64
            else -> {
                System.err.println("Can not determinate architecture: $architecture")
                Architectures.OTHER
            }
        }
    }

    @JvmStatic
    val isArm = ARCHITECTURE == Architectures.ARM || ARCHITECTURE == Architectures.AARCH64

    @JvmStatic
    val isX86 = ARCHITECTURE == Architectures.AMD64 || ARCHITECTURE == Architectures.X86

    @JvmStatic
    val is32Bit = ARCHITECTURE == Architectures.ARM || ARCHITECTURE == Architectures.X86
}
