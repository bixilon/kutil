/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.buffer

import de.bixilon.kutil.array.ByteArrayUtil
import java.nio.ByteBuffer

object ByteBufferUtil {

    fun ByteBuffer.readRemaining(): ByteArray {
        val array = ByteArray(this.remaining())
        this.get(array)
        return array
    }

    fun ByteBuffer.toByteArray(): ByteArray {
        val position = this.position()
        position(0)
        val array = ByteArray(this.limit())
        this.get(array)
        position(position)
        return array
    }

    fun createBuffer(max: Int = BufferDefinition.DEFAULT_BUFFER_SIZE) = when {
        max < 0 -> throw IllegalArgumentException("Buffer can not have a negative size: $max")
        max == 0 -> ByteArrayUtil.EMPTY
        max < DEFAULT_BUFFER_SIZE -> ByteArray(max)
        else -> ByteArray(DEFAULT_BUFFER_SIZE)
    }
}
