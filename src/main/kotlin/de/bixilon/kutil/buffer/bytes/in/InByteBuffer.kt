/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package de.bixilon.kutil.buffer.bytes.`in`

import de.bixilon.kutil.array.ArrayUtil.cast
import de.bixilon.kutil.uuid.UUIDUtil.toUUID
import java.nio.ByteOrder
import java.nio.charset.StandardCharsets
import java.util.*
import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.InvocationKind
import kotlin.contracts.contract


open class InByteBuffer {
    private val bytes: ByteArray
    private val bigEndian: Boolean
    var pointer = 0

    constructor(bytes: ByteArray, order: ByteOrder = ByteOrder.BIG_ENDIAN) {
        this.bytes = bytes
        this.bigEndian = order == ByteOrder.BIG_ENDIAN
    }

    constructor(buffer: InByteBuffer) {
        bytes = buffer.bytes.clone()
        pointer = buffer.pointer
        this.bigEndian = buffer.bigEndian
    }

    val size: Int
        get() = bytes.size

    val remaining: Int
        get() = size - pointer

    inline fun <reified T> readArray(length: Int = readVarInt(), reader: () -> T): Array<T> {
        require(length, 1) // 1 is the minimum length
        val array: Array<T?> = arrayOfNulls(length)
        for (i in 0 until length) {
            array[i] = reader()
        }
        return array.cast()
    }

    inline fun <reified T> readSet(length: Int = readVarInt(), reader: () -> T): Set<T> {
        require(length, 1) // 1 is the minimum length
        val set: MutableSet<T> = LinkedHashSet(length)
        for (i in 0 until length) {
            set += reader()
        }
        return set
    }

    inline fun <reified K, reified V> readMap(length: Int = readVarInt(), key: () -> K, value: () -> V): Map<K, V> {
        require(length, 1) // 1 is the minimum length
        val map: MutableMap<K, V> = LinkedHashMap(length)
        for (i in 0 until length) {
            map[key()] = value()
        }
        return map
    }

    fun require(count: Int, single: Int) {
        val next = pointer + (count * single)
        if (next > bytes.size) throw ByteBufferUnderflowException(next, size)
    }

    fun readByte(): Byte {
        if (pointer >= size) {
            throw ByteBufferUnderflowException(pointer, size)
        }
        return bytes[pointer++]
    }

    open fun readByteArray(array: ByteArray) {
        System.arraycopy(bytes, pointer, array, 0, array.size)
        pointer += array.size
    }

    open fun readByteArray(length: Int = readVarInt()): ByteArray {
        require(length, 1)
        val array = ByteArray(length)
        System.arraycopy(bytes, pointer, array, 0, length)
        pointer += length
        return array
    }

    fun readUnsignedByte(): Int {
        return readByte().toInt() and ((1 shl Byte.SIZE_BITS) - 1)
    }


    fun readShort(): Short {
        val data = if (bigEndian) (readUnsignedByte() shl Byte.SIZE_BITS or readUnsignedByte()) else (readUnsignedByte() or (readUnsignedByte() shl Byte.SIZE_BITS))
        return data.toShort()
    }

    fun readShortArray(length: Int = readVarInt()): ShortArray {
        require(length, Short.SIZE_BYTES)
        val array = ShortArray(length)
        for (i in 0 until length) {
            array[i] = readShort()
        }
        return array
    }

    fun readUnsignedShort(): Int {
        return readShort().toInt() and ((1 shl Short.SIZE_BITS) - 1)
    }

    fun readInt(): Int {
        val data = if (bigEndian) (readUnsignedShort() shl Short.SIZE_BITS or readUnsignedShort()) else (readUnsignedShort() or (readUnsignedShort() shl Short.SIZE_BITS))
        return data
    }

    fun readIntArray(length: Int = readVarInt()): IntArray {
        require(length, Int.SIZE_BYTES)
        val array = IntArray(length)
        for (i in 0 until length) {
            array[i] = readInt()
        }
        return array
    }

    fun readUnsignedInt(): Long {
        return readInt().toLong() and ((1L shl Int.SIZE_BITS) - 1)
    }


    fun readVarInt(): Int {
        var index = 0
        var result = 0
        var read: Int
        do {
            read = readUnsignedByte()
            result = result or (read and 0x7F shl (Byte.SIZE_BITS - 1) * index)
            index++
            require(index <= Int.SIZE_BYTES + 1) { "VarInt is too big" }
        } while (read and 0x80 != 0)

        return result
    }


    @JvmOverloads
    fun readVarIntArray(length: Int = readVarInt()): IntArray {
        require(length, 1) // 1 is the minimum length
        val array = IntArray(length)
        for (i in 0 until length) {
            array[i] = readVarInt()
        }
        return array
    }


    fun readLong(): Long {
        val data = if (bigEndian) (readUnsignedInt() shl Int.SIZE_BITS or readUnsignedInt()) else (readUnsignedInt() or (readUnsignedInt() shl Int.SIZE_BITS))
        return data
    }

    fun readLongArray(length: Int = readVarInt()): LongArray {
        require(length, Long.SIZE_BYTES)
        val array = LongArray(length)
        for (i in 0 until length) {
            array[i] = readLong()
        }
        return array
    }

    fun readLongArray(target: LongArray, length: Int = readVarInt()) {
        require(length, Long.SIZE_BYTES) // 1 is the minimum length
        for (i in 0 until length) {
            target[i] = readLong()
        }
    }


    fun readVarLong(): Long {
        var index = 0
        var result = 0L
        var read: Int
        do {
            read = readUnsignedByte()
            result = result or ((read and 0x7F shl (Byte.SIZE_BITS - 1) * index).toLong())
            index++
            require(index <= Long.SIZE_BYTES + 1) { "VarLong is too big" }
        } while (read and 0x80 != 0)

        return result
    }

    fun readVarLongArray(length: Int = readVarInt()): LongArray {
        require(length, 1) // 1 is the minimum length
        val array = LongArray(length)
        for (i in 0 until length) {
            array[i] = readVarLong()
        }
        return array
    }

    open fun readFloat(): Float {
        return Float.fromBits(readInt())
    }

    fun readFloatArray(length: Int = readVarInt()): FloatArray {
        require(length, Float.SIZE_BYTES) // 1 is the minimum length
        val array = FloatArray(length)
        for (i in 0 until length) {
            array[i] = readFloat()
        }
        return array
    }


    open fun readDouble(): Double {
        return Double.fromBits(readLong())
    }

    fun readDoubleArray(length: Int = readVarInt()): DoubleArray {
        require(length, Double.SIZE_BYTES) // 1 is the minimum length
        val array = DoubleArray(length)
        for (i in 0 until length) {
            array[i] = readDouble()
        }
        return array
    }


    fun readBoolean(): Boolean {
        return readUnsignedByte() == 1
    }

    open fun readString(length: Int = readVarInt()): String {
        return String(readByteArray(length), StandardCharsets.UTF_8)
    }

    fun readNullString(length: Int = readVarInt()): String? {
        val string = readString(length)
        if (string.isBlank()) {
            return null
        }
        return string
    }

    fun readUUID(): UUID {
        return UUID(readLong(), readLong())
    }

    fun readUUIDString(): UUID {
        return readString().toUUID()
    }

    fun readRemaining(): ByteArray {
        return readByteArray(length = size - pointer)
    }

    fun getBase64(): String {
        return String(Base64.getEncoder().encode(readRemaining()))
    }

    @OptIn(ExperimentalContracts::class)
    fun <T> readOptional(reader: InByteBuffer.() -> T): T? {
        contract {
            callsInPlace(reader, InvocationKind.EXACTLY_ONCE)
        }
        return if (readBoolean()) {
            reader(this)
        } else {
            null
        }
    }
}
