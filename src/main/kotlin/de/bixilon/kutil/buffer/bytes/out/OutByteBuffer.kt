/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package de.bixilon.kutil.buffer.bytes.out

import de.bixilon.kutil.collections.primitive.bytes.HeapArrayByteList
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.charset.StandardCharsets
import java.util.*

open class OutByteBuffer(order: ByteOrder = ByteOrder.BIG_ENDIAN) {
    private val bytes = HeapArrayByteList()
    private val bigEndian = order == ByteOrder.BIG_ENDIAN

    val size: Int get() = bytes.size

    constructor(buffer: OutByteBuffer) : this(if (buffer.bigEndian) ByteOrder.BIG_ENDIAN else ByteOrder.LITTLE_ENDIAN) {
        bytes.addAll(buffer.bytes)
    }

    fun writeByte(byte: Byte) {
        bytes.add(byte)
    }

    fun writeBytes(count: Int, byte: Byte) {
        bytes.ensureSize(count)
        for (i in 0 until count) {
            writeByte(byte)
        }
    }

    fun writeBytes(count: Int, byte: Int) {
        writeBytes(count, byte.toByte())
    }

    fun writeByte(byte: Int) {
        writeByte((byte and 0xFF).toByte())
    }

    fun writeByte(long: Long) {
        writeByte((long and 0xFF).toByte())
    }

    open fun writeBareByteArray(data: ByteArray) {
        bytes.addAll(data)
    }

    open fun writeByteArray(data: ByteArray) {
        writeVarInt(data.size)
        writeBareByteArray(data)
    }

    fun writeShort(short: Short) {
        writeShort(short.toInt())
    }

    fun writeShort(short: Int) {
        if (bigEndian) {
            writeByte(short ushr Byte.SIZE_BITS)
            writeByte(short)
        } else {
            writeByte(short)
            writeByte(short ushr Byte.SIZE_BITS)
        }
    }

    open fun writeBareShortArray(data: ShortArray) {
        for (short in data) {
            writeShort(short)
        }
    }

    open fun writeShortArray(data: ShortArray) {
        writeVarInt(data.size)
        writeBareShortArray(data)
    }

    fun writeInt(int: Int) {
        if (bigEndian) {
            writeShort(int shr Short.SIZE_BITS)
            writeShort(int)
        } else {
            writeShort(int)
            writeShort(int shr Short.SIZE_BITS)
        }
    }

    fun writeInt(int: Long) {
        writeInt(int.toInt())
    }

    open fun writeBareIntArray(data: IntArray) {
        for (short in data) {
            writeInt(short)
        }
    }

    open fun writeIntArray(data: IntArray) {
        writeVarInt(data.size)
        writeBareIntArray(data)
    }

    fun writeLong(value: Long) {
        if (bigEndian) {
            writeInt((value shr Int.SIZE_BITS).toInt())
            writeInt(value.toInt())
        } else {
            writeInt(value.toInt())
            writeInt((value shr Int.SIZE_BITS).toInt())
        }
    }

    fun writeBareLongArray(data: LongArray) {
        for (l in data) {
            writeLong(l)
        }
    }

    fun writeLongArray(data: LongArray) {
        writeVarInt(data.size)
        writeBareLongArray(data)
    }

    open fun writeBareString(string: String) {
        writeBareByteArray(string.toByteArray(StandardCharsets.UTF_8))
    }

    open fun writeString(string: String) {
        val bytes = string.toByteArray(StandardCharsets.UTF_8)
        writeVarInt(bytes.size)
        writeBareByteArray(bytes)
    }

    fun writeVarInt(int: Int) {
        // thanks https://wiki.vg/Protocol#VarInt_and_VarLong
        var value = int
        do {
            var write = value and 0x7F
            value = value ushr 7
            if (value != 0) {
                write = write or 0x80 // set carry bit
            }
            writeByte(write)
        } while (value != 0)
    }

    fun writeVarLong(long: Long) {
        var value = long
        do {
            var write = (value and 0x7F).toInt()
            value = value ushr 7
            if (value != 0L) {
                write = write or 0x80 // set carry bit
            }
            writeByte(write)
        } while (value != 0L)
    }

    open fun writeFloat(float: Float) {
        writeInt(float.toBits())
    }

    fun writeFloat(float: Double) {
        writeFloat(float.toFloat())
    }

    open fun writeDouble(double: Double) {
        writeLong(double.toBits())
    }

    fun writeDouble(float: Float) {
        writeDouble(float.toDouble())
    }

    fun writeUUID(uuid: UUID) {
        writeLong(uuid.mostSignificantBits)
        writeLong(uuid.leastSignificantBits)
    }

    fun writeBoolean(value: Boolean) {
        writeByte(if (value) 0x01 else 0x00)
    }

    fun toArray(): ByteArray {
        return bytes.toArray()
    }

    fun writeTo(target: ByteArray, offset: Int) {
        bytes.writeTo(target, offset)
    }

    fun writeTo(target: ByteBuffer) {
        bytes.writeTo(target)
    }

    fun <T> writeArray(array: Array<T>, writer: (T) -> Unit) {
        writeVarInt(array.size)
        for (entry in array) {
            writer(entry)
        }
    }

    fun <T> writeArray(collection: Collection<T>, writer: (T) -> Unit) {
        writeVarInt(collection.size)
        for (entry in collection) {
            writer(entry)
        }
    }

    fun <T> writeOptional(value: T?, writer: (T) -> Unit) {
        writeBoolean(value != null)
        value?.let(writer)
    }
}
