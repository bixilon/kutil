/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package de.bixilon.kutil.latch


class CallbackLatch @JvmOverloads constructor(count: Int, val parent: AbstractLatch? = null) : AbstractLatch {
    private var callbacks: MutableSet<() -> Unit>? = null

    @Deprecated("unsafe!")
    override val notify = Object()
    private var _count = 0
        set(value) {
            val diff = value - field
            check(value >= 0) { "Can not set negative count (previous=$field, value=$value)" }
            if (diff > 0) {
                total += diff
            }
            field = value
        }

    override var count: Int
        get() = synchronized(notify) {
            return _count
        }
        set(value) {
            val diff: Int
            synchronized(notify) {
                diff = value - _count
                _count = value
            }
            notify()
            parent?.plus(diff)
        }

    override var total: Int = 0
        get() = synchronized(notify) {
            return field
        }
        private set(value) {
            check(value >= 0) { "Total can not be < 0: $value" }
            synchronized(notify) {
                check(value >= field) { "Total can not decrement! (current=$field, wanted=$value)" }
                field = value
            }
        }


    init {
        check(parent !== this)
        this.count += count
    }

    @JvmName("Notify2")
    private fun notify() = synchronized(notify) {
        notifyCallback()
        notify.notifyAll()
    }

    private fun notifyCallback() {
        val callbacks = this.callbacks ?: return
        for (callback in callbacks) {
            callback.invoke()
        }
    }


    operator fun plusAssign(callback: () -> Unit) = synchronized(notify) {
        var callbacks = this.callbacks
        if (callbacks == null) {
            callbacks = linkedSetOf()
            this.callbacks = callbacks
        }
        callbacks += callback
    }

    override fun toString() = "$count / $total"
}
