/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package de.bixilon.kutil.latch

import de.bixilon.kutil.time.TimeUtil.millis
import kotlin.time.Duration


interface AbstractLatch {
    @Deprecated("unsafe!")
    val notify: Object

    var count: Int
    val total: Int

    fun await(timeout: Duration) = await(timeout.inWholeMilliseconds)
    fun await(timeout: Long = 0L) {
        val start = if (timeout > 0) millis() else 0L

        synchronized(notify) {
            while (true) {
                if (count == 0) {
                    return
                }
                notify.wait(timeout)
                LatchUtil.checkTimeout(start, timeout)
            }
        }
    }

    operator fun inc(): AbstractLatch {
        plus(1)
        return this
    }

    operator fun dec(): AbstractLatch {
        minus(1)
        return this
    }

    fun countUp() {
        plus(1)
    }

    fun countDown() {
        minus(1)
    }

    fun plus(value: Int) = synchronized(notify) {
        count += value
    }

    fun minus(value: Int) {
        return plus(-value)
    }

    fun waitForChange(timeout: Duration) = waitForChange(timeout.inWholeMilliseconds)
    fun waitForChange(timeout: Long = 0L) {
        val start = if (timeout > 0) millis() else 0L

        synchronized(notify) {
            val count = count
            val total = total
            while (true) {
                if (this.count != count || this.total != total) {
                    return
                }

                notify.wait(timeout)
                LatchUtil.checkTimeout(start, timeout)
            }
        }
    }

    fun awaitWithChange(timeout: Duration) = awaitWithChange(timeout.inWholeMilliseconds)
    fun awaitWithChange(timeout: Long = 0L) = synchronized(notify) {
        if (count == 0) {
            waitForChange(timeout)
        }
        await(timeout)
    }

    fun awaitOrChange(timeout: Duration) = awaitOrChange(timeout.inWholeMilliseconds)
    fun awaitOrChange(timeout: Long = 0L) = synchronized(notify) {
        if (count == 0) {
            return
        }
        waitForChange(timeout)
    }

    fun waitIfGreater(value: Int, timeout: Duration) = waitIfGreater(value, timeout.inWholeMilliseconds)
    fun waitIfGreater(value: Int, timeout: Long = 0L) = synchronized(notify) {
        while (this.count > value) {
            waitForChange(timeout)
        }
    }

    fun waitIfLess(value: Int, timeout: Duration) = waitIfLess(value, timeout.inWholeMilliseconds)
    fun waitIfLess(value: Int, timeout: Long = 0L) = synchronized(notify) {
        while (this.count < value) {
            waitForChange(timeout)
        }
    }

    companion object {

        fun AbstractLatch?.child(count: Int): AbstractLatch {
            if (this == null) return SimpleLatch(count)
            return ParentLatch(count, this)
        }
    }
}
