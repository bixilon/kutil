/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.shutdown

import de.bixilon.kutil.collections.CollectionUtil.synchronizedSetOf
import de.bixilon.kutil.concurrent.pool.DefaultThreadPool
import de.bixilon.kutil.file.watcher.FileWatcherService
import kotlin.system.exitProcess

object ShutdownManager {
    private val hooks: MutableSet<Runnable> = synchronizedSetOf()
    private var initialized = false
    @Volatile var shuttingDown = false
        private set
    private var viaHook = false


    fun shutdown(message: String? = null, reason: AbstractShutdownReason = AbstractShutdownReason.DEFAULT) {
        if (shuttingDown) {
            return
        }
        shuttingDown = true
        val message = message ?: reason.message
        if (message != null) {
            println("Shutting down: $message")
        } else {
            println("Shutting down...")
        }
        for (hook in hooks) {
            hook.run()
        }
        FileWatcherService.stop()
        DefaultThreadPool.shutdownNow()
        if (!viaHook) {
            exitProcess(reason.exitCode)
        }
    }

    private fun init() {
        if (initialized) {
            return
        }
        Runtime.getRuntime().addShutdownHook(Thread({
            if (shuttingDown) {
                return@Thread
            }
            viaHook = true

            shutdown()
        }, "Shutdown Hook"))
        initialized = true
    }

    fun addHook(hook: Runnable) {
        if (shuttingDown) {
            throw IllegalStateException("Already shutting down!")
        }
        hooks += hook
    }

    operator fun plusAssign(hook: Runnable) = addHook(hook)

    init {
        init()
    }
}
