/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.array

object ByteArrayUtil {
    val EMPTY = byteArrayOf()

    private fun StringBuilder.appendNibble(nibble: Int) {
        when (nibble) {
            in 0x00..0x09 -> append('0' + nibble)
            in 0x0A..0x0F -> append('a' - 0x0A + nibble)
        }
    }

    fun ByteArray.toHex(): String {
        val result = StringBuilder(size * 2) // hex: 4bit, byte: 8bit

        for (byte in this) {
            val int = byte.toInt() and 0xFF
            result.appendNibble(int ushr 4)
            result.appendNibble(int and 0x0F)
        }

        return result.toString()
    }

    inline fun ByteArray.isIndex(index: Int): Boolean {
        return index > 0 && index < size
    }
}
