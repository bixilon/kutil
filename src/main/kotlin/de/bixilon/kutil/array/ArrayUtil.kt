/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.array

import de.bixilon.kutil.cast.CastUtil.unsafeCast

object ArrayUtil {

    inline fun <T> Array<T>.index(value: T): Int? {
        val index = indexOf(value)
        if (index < 0) {
            return null
        }
        return index
    }

    fun modifyArrayIndex(value: Int, size: Int): Int {
        if (size <= 0) {
            throw IllegalArgumentException("Size must be > 1: $size")
        }
        var ret = value % size

        if (ret < 0) {
            ret += size
        }

        return ret
    }

    inline fun <T> Array<T?>.cast(): Array<T> {
        return this.unsafeCast()
    }

    inline fun <T> Array<T>.isIndex(index: Int): Boolean {
        return index >= 0 && index < size
    }

    inline fun <T> Array<T>.trySet(index: Int, value: T): Boolean {
        if (index < 0 || index >= size) {
            return false
        }
        this[index] = value
        return true
    }

    fun <T> Array<T?>.isEmptyOrOnlyNull(): Boolean {
        for (entry in this) {
            if (entry != null) {
                return false
            }
        }
        return true
    }

    inline fun <T> Array<T>.getFirst(): T {
        return this[0]
    }

    inline fun <T> Array<T>.getLast(): T {
        return this[lastIndex]
    }


    inline fun <reified T> Array<T?>.trim(): Array<T> {
        val list: MutableList<T> = ArrayList(this.size)
        for (entry in this) {
            if (entry == null) {
                continue
            }
            list += entry
        }
        return list.toTypedArray()
    }

    fun <T> Array<T>.next(current: T): T {
        val index = this.indexOf(current)
        check(index >= 0) { "Array does not contain $current" }

        if (index == this.size - 1) {
            return this[0]
        }
        return this[index + 1]
    }
}
