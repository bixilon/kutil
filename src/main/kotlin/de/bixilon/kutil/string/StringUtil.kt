/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.string

import de.bixilon.kutil.cast.CastUtil.unsafeCast
import de.bixilon.kutil.reflection.ReflectionUtil.getFieldOrNull
import java.nio.charset.StandardCharsets

object StringUtil {
    private val VALUE by lazy { String::class.java.getFieldOrNull("value") }
    val DEFAULT_CHARSET = StandardCharsets.UTF_8

    fun String.toSnakeCase(): String {
        if (isEmpty()) return ""
        val result = StringBuilder(length)
        val codes = codePoints().iterator()
        var uppercased = 1 // prevent first char
        while (codes.hasNext()) {
            val code = codes.nextInt()
            if (Character.isUpperCase(code)) {
                if (uppercased == 0) {
                    result.append('_')
                }
                uppercased++
            } else {
                uppercased = 0
            }
            result.appendCodePoint(Character.toLowerCase(code))
        }
        return result.toString()
    }

    fun String.toCamelCase(): String {
        if (isEmpty()) return ""
        val result = StringBuilder(length)
        val codes = codePoints().iterator()

        var underscore = false
        while (codes.hasNext()) {
            val code = Character.toLowerCase(codes.nextInt())

            if (code == '_'.code) {
                underscore = true
                continue
            }
            if (underscore) {
                result.appendCodePoint(Character.toUpperCase(code))
                underscore = false
                continue
            }
            result.appendCodePoint(code)
        }

        return result.toString()
    }

    fun String.isLowercase(): Boolean {
        for (code in codePoints()) {
            if (Character.isUpperCase(code)) {
                return false
            }
        }
        return true
    }

    fun String.isUppercase(): Boolean {
        for (code in codePoints()) {
            if (Character.isLowerCase(code)) {
                return false
            }
        }
        return true
    }

    private fun String.formatPlaceholder(key: String, value: Any?): String {
        return this.replace("\${$key}", value.toString())
    }

    fun String.formatPlaceholder(format: Map<String, Any?>): String {
        var output = this
        for ((key, value) in format) {
            output = output.formatPlaceholder(key, value)
        }
        return output
    }

    fun String.formatPlaceholder(vararg format: Pair<String, Any?>): String {
        var output = this
        for ((key, value) in format) {
            output = output.formatPlaceholder(key, value)
        }
        return output
    }


    fun String.getBetween(first: String, second: String): String {
        val result = this.substring(this.indexOf(first) + first.length)
        return result.substring(0, result.indexOf(second))
    }


    fun String.codePointAtOrNull(index: Int): Int? {
        if (index < 0 || index >= this.length) {
            return null
        }
        return this.codePointAt(index)
    }

    fun String.truncate(length: Int): String {
        if (length < 0) {
            throw IllegalArgumentException("Length must not be < 0: $length")
        }
        if (this.length <= length) {
            return this
        }
        return this.substring(0, length)
    }


    fun String.fill(char: Char, length: Int): String {
        if (this.length >= length) return this
        val fill = char.toString().repeat(length - this.length)

        return fill + this
    }

    @Deprecated("fucking unsafe")
    fun String.mutable(): ByteArray {
        return VALUE!!.get(this).unsafeCast()
    }
}
