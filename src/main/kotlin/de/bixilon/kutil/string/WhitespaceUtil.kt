/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.string

object WhitespaceUtil {

    fun String.removeTrailingWhitespaces(): String {
        return this.trim { Character.isWhitespace(it) }
    }

    fun String.removeMultipleWhitespaces(): String {
        val builder = StringBuilder(this.length)
        var whitespace = false
        for (char in this) {
            if (char.isWhitespace()) {
                if (whitespace) {
                    continue
                }
                whitespace = true
            } else {
                whitespace = false
            }
            builder.append(char)
        }

        return builder.toString()
    }

    fun String.trimWhitespaces(): String {
        return this.removeTrailingWhitespaces().removeMultipleWhitespaces()
    }

    fun String.removeWhitespaces(): String {
        val builder = StringBuilder(this.length)
        for (char in this) {
            if (char.isWhitespace()) continue
            builder.append(char)
        }
        if (builder.length == this.length) return this // reduce allocation

        return builder.toString()
    }
}
