/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.string

object TextUtil {

    fun getOverlappingText(start: String, end: String): Int {
        var overlapping = 0

        shift@ for (shift in 1..end.length) {
            if (start.length < shift) {
                break
            }
            for (i in 0 until shift) {
                if (end.codePointAt(i) != start.codePointAt(start.length - (shift - i))) {
                    continue@shift
                }
            }
            overlapping = shift
        }

        return overlapping
    }
}
