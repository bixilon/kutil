/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.locale

import de.bixilon.kutil.collections.map.creator.MapCreator.uMutableMapOf
import java.util.*

object LanguageUtil {
    private val LOCALES = arrayOf(
        Locale.GERMAN,
        Locale.ENGLISH,
        Locale.FRENCH,
        Locale.ITALIAN,
        Locale.JAPANESE,
        Locale.KOREAN,
        Locale.CHINESE,
        Locale.SIMPLIFIED_CHINESE,
        Locale.TRADITIONAL_CHINESE,
    )
    private val LOCALES_MAP: MutableMap<String, Locale> = uMutableMapOf()

    init {
        for (locale in LOCALES) {
            if (locale.language.isBlank()) {
                continue
            }
            LOCALES_MAP[locale.language.lowercase()] = locale
        }
    }

    operator fun get(name: String): Locale? {
        var language = name
        if ("-" in name) {
            language = name.split("-")[0]
        }
        language = language.lowercase()
        return LOCALES_MAP[language]
    }

    val Locale.fullName: String
        get() = language + "_" + country.ifEmpty { language.uppercase() }

}
