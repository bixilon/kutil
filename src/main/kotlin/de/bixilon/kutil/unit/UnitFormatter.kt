/*
 * Minosoft
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.unit

import kotlin.math.abs
import kotlin.time.Duration

object UnitFormatter {
    private const val BYTE_FACTOR = 1024L
    private val BYTE_UNITS = arrayOf("B", "KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB", "YiB")
    private val SI_PREFIXES = arrayOf("", "k", "M", "G", "T", "P", "E", "Z", "Y")
    private val FRACTIONAL_TIME_UNITS = arrayOf("ns", "µs", "ms", "s")

    fun Long.formatBytes(): String {
        if (this < 0) {
            return "?"
        }
        if (this < 1024L) {
            return "${this}B"
        }
        return formatUnit(this, BYTE_UNITS, BYTE_FACTOR)
    }

    fun Int.formatBytes(): String {
        return this.toLong().formatBytes()
    }

    fun Long.formatSI(): String {
        return formatUnit(this, SI_PREFIXES, 1000L)
    }

    fun Int.formatSI(): String {
        return this.toLong().formatSI()
    }


    private fun formatUnit(number: Long, units: Array<String>, factor: Long): String {
        var currentFactor = 1L

        for (unit in units) {
            val fractionalFactor = currentFactor * 10
            if (number < fractionalFactor) {
                // comma
                val nonFractional = number / currentFactor
                val fractional = (number - (nonFractional * currentFactor)) * 10 / currentFactor
                return "$nonFractional.$fractional $unit"
            }
            val nextFactor = currentFactor * factor
            if (number < nextFactor) {
                return "${number / currentFactor} $unit"
            }

            currentFactor = nextFactor
        }

        return number.toString() // This number is far too big, no clue what we should do with it...
    }
    fun Duration.format(): String = inWholeNanoseconds.formatNanos()

    private fun Long._formatNanos(): String {
        var currentFactor = 1L

        for ((index, unit) in FRACTIONAL_TIME_UNITS.withIndex()) {
            val fractionalFactor = currentFactor * 10
            if (this < fractionalFactor) {
                // comma
                val nonFractional = this / currentFactor
                val fractional = (this - (nonFractional * currentFactor)) * 10 / currentFactor
                return "$nonFractional.$fractional $unit"
            }
            val nextFactor = currentFactor * 1000
            if (index != 3 && this < nextFactor) {
                return "${this / currentFactor} $unit"
            }

            currentFactor = nextFactor
        }

        val seconds = this / 1_000_000_000
        if (seconds < 60) return "$seconds s"
        if (seconds < 60 * 60) return "${seconds / 60} m"
        if (seconds < 60 * 60 * 24) return "${seconds / (60 * 60)} h"

        val days = seconds / (60 * 60 * 24)
        if (days < 7) return "$days d"
        if (days < 30) return "${days / 7} w"
        if (days < 365) return "${days / 30} M"

        return "${days / 365} y"
    }

    fun Long.formatNanos(): String {
        if (this < 0) {
            return "-" + abs(this)._formatNanos()
        }
        return this._formatNanos()
    }

    fun Int.formatNanos(): String {
        return this.toLong().formatNanos()
    }

    fun Long.formatMillis(): String {
        return (this * 1_000_000L).formatNanos()
    }

    fun Int.formatMillis(): String {
        return this.toLong().formatMillis()
    }

    fun Long.formatSeconds(): String {
        return (this * 1_000_000_000L).formatNanos()
    }

    fun Int.formatSeconds(): String {
        return this.toLong().formatSeconds()
    }
}
