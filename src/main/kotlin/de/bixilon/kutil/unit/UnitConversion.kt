package de.bixilon.kutil.unit

object UnitConversion {

    fun millisToNanos(millis: Long): Long {
        return millis * 1_000_000L
    }

    fun nanosToMillis(nanos: Long): Long {
        return nanos / 1_000_000L
    }
}
