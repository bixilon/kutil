/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.compression.zlib

import de.bixilon.kutil.buffer.ByteBufferUtil.createBuffer
import java.io.ByteArrayOutputStream
import java.util.zip.Deflater
import java.util.zip.Inflater

object ZlibUtil {

    fun ByteArray.decompress(expected: Int? = null): ByteArray {
        val inflater = Inflater()
        inflater.setInput(this, 0, this.size)
        val buffer = createBuffer()
        val stream = ByteArrayOutputStream(expected ?: this.size)
        val maxSize = expected ?: Int.MAX_VALUE
        while (!inflater.finished()) {
            if (stream.size() > maxSize) {
                throw OutOfMemoryError("Stream too large!")
            }
            stream.write(buffer, 0, inflater.inflate(buffer))
        }
        stream.close()
        return stream.toByteArray()
    }


    fun ByteArray.compress(): ByteArray {
        val deflater = Deflater()
        deflater.setInput(this)
        deflater.finish()
        val buffer = createBuffer(this.size)
        val stream = ByteArrayOutputStream(this.size)
        while (!deflater.finished()) {
            stream.write(buffer, 0, deflater.deflate(buffer))
        }
        stream.close()
        return stream.toByteArray()
    }
}
