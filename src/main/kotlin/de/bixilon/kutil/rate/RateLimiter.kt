/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.rate

import de.bixilon.kutil.collections.CollectionUtil.synchronizedSetOf
import de.bixilon.kutil.time.TimeUtil.millis
import java.util.concurrent.locks.ReentrantLock

typealias RateAction = (() -> Unit)

class RateLimiter(
    val limit: Int = 20,
    val inTime: Long = 1000,
    val allowForcePerform: Boolean = true,
    val dependencies: MutableSet<RateLimiter> = synchronizedSetOf(),
) {
    private val lock = ReentrantLock()
    private var queued: RateAction? = null
    private var executions: MutableList<Long> = mutableListOf()

    val upToDate: Boolean
        get() {
            lock.lock()
            val upToDate = queued == null
            lock.unlock()
            return upToDate
        }

    val canWork: Boolean
        get() {
            cleanup(true)
            return executions.size < limit
        }

    private val _canWork: Boolean
        get() {
            cleanup(false)
            return executions.size < limit
        }

    /**
     * Tries to perform a specific action
     *
     * @return If the action could be performed or has to wait
     */
    fun perform(action: RateAction): Boolean {
        lock.lock()
        if (!_canWork) {
            queued = action
            lock.unlock()
            return false
        }

        internalPerform(action)
        lock.unlock()
        return true
    }

    operator fun plusAssign(action: RateAction) {
        perform(action)
    }

    private fun internalPerform(action: RateAction) {
        for (dependency in dependencies) {
            if (!dependency.upToDate) {
                dependency.work()
            }
            check(dependency.upToDate) { "RateLimiter dependency is not upToDate!" }
        }
        lock.lock()
        queued = null
        try {
            action.invoke()
        } catch (error: Throwable) {
            error.printStackTrace()
        }
        val time = millis()
        executions += time
        lock.unlock()
    }

    fun forcePerform(action: RateAction) {
        check(allowForcePerform) { "RateLimiter does not allow force performing!" }
        lock.lock()
        internalPerform(action)
        lock.unlock()
    }

    fun work() {
        lock.lock()
        cleanup(false)
        if (!_canWork) {
            lock.unlock()
            return
        }
        queued?.let { internalPerform(it) }
        lock.unlock()
    }

    private fun cleanup(lock: Boolean) {
        if (lock) {
            this.lock.lock()
        }
        val time = millis()

        while (executions.isNotEmpty()) {
            val execution = executions.first()
            val addDelta = time - execution
            if (addDelta - inTime < 0L) {
                break
            }
            // remove
            this.executions.removeFirst()
        }
        if (lock) {
            this.lock.unlock()
        }
    }
}

