/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.primitive

object IntUtil {

    fun Any?.toInt(): Int {
        return when (this) {
            is Int -> this
            is Number -> this.toInt()
            is String -> Integer.parseInt(this)
            is Boolean -> if (this) 1 else 0
            else -> throw IllegalArgumentException("Unknown int value: $this")
        }
    }

    fun Int.thousands(): String {
        return String.format("%,d", this)
    }

    fun Int.toHex(digits: Int = 0): String {
        val string = Integer.toHexString(this)

        if (string.length >= digits) {
            return string
        }
        return "0".repeat(digits - string.length) + string
    }

    fun String.checkInt(): Int? {
        var first = true
        for (point in codePoints()) {
            if (first) {
                if (point == '-'.code) {
                    continue
                }
                first = false
            }
            if (point < '0'.code || point > '9'.code) {
                return null
            }
        }
        return this.toInt()
    }
}
