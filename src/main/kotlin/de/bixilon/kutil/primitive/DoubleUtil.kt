/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.primitive

import kotlin.math.abs

object DoubleUtil {
    const val DEFAULT_MARGIN = 0.00000001


    fun Any?.toDouble(): Double {
        return when (this) {
            is Double -> this
            is Number -> this.toDouble()
            is String -> java.lang.Double.parseDouble(this)
            else -> throw IllegalArgumentException("Unknown double value: $this")
        }
    }

    fun Double.matches(other: Double, margin: Double = DEFAULT_MARGIN): Boolean {
        val diff = other - this
        if (abs(diff) < margin) {
            // fine, is in expected range
            return true
        }
        return false
    }
}
