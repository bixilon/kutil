package de.bixilon.kutil.environment

object Environment {

    fun isInCI(): Boolean {
        val env = System.getenv("GITHUB_ACTIONS") ?: System.getenv("GITLAB_CI") ?: System.getenv("TRAVIS") ?: System.getenv("CIRCLECI")
        return env != null
    }
}
