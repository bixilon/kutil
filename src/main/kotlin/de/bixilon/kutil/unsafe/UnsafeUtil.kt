/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.unsafe

import de.bixilon.kutil.cast.CastUtil.unsafeNull
import de.bixilon.kutil.exception.Broken
import de.bixilon.kutil.exception.ExceptionUtil.catchAll
import de.bixilon.kutil.exception.ExceptionUtil.ignoreAll
import de.bixilon.kutil.primitive.BooleanUtil.toBoolean
import sun.misc.Unsafe
import java.lang.reflect.AccessibleObject
import java.lang.reflect.Modifier

object UnsafeUtil {
    val UNSAFE = ignoreAll { retrieve() } ?: unsafeNull()
    private val OVERRIDE_OFFSET = getOverrideFieldOffset()
    var allow = (UNSAFE != null) && System.getenv("KUTIL_ALLOW_UNSAFE")?.toBoolean() ?: true

    private fun retrieve(): Unsafe {
        val field = Unsafe::class.java.declaredFields.find { it.type == Unsafe::class.java && Modifier.isStatic(it.modifiers) } ?: Broken("Can not find unsafe!")
        // can not use get field, the name differs from platform to platform
        field.isAccessible = true
        return field[null] as Unsafe
    }


    fun AccessibleObject.setUnsafeAccessible() {
        when {
            allow -> UNSAFE.putBoolean(this, OVERRIDE_OFFSET, true)
            else -> isAccessible = true
        }
    }

    fun hardCrash() {
        UNSAFE.putAddress(0, 0)
    }

    private fun getOverrideFieldOffset(): Long {
        catchAll { UNSAFE.objectFieldOffset(AccessibleObject::class.java.getDeclaredField("override")) }
        // TODO: make more robust, the in java 18 the native method for getting the declared fields in patched for the class `AccessibleObject`.
        // we just assume the field offset won't change in the future (currently its 12), but this might do in the future. We need a robust method of getting the offset
        // one option would be using only unsafe (i.e. all UnsafeFields) they don't require it, but that won't work for methods.
        return 12
    }
}
