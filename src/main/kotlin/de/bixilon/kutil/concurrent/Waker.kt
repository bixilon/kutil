package de.bixilon.kutil.concurrent

import kotlin.time.Duration

@Suppress("PLATFORM_CLASS_MAPPED_TO_KOTLIN")
class Waker(
    private val lock: Object = Object(),
) {

    @JvmName("wait2")
    fun wait() = synchronized(lock) {
        lock.wait()
    }

    fun wait(timeout: Duration) = wait(timeout.inWholeMilliseconds)

    @JvmName("wait2")
    fun wait(timeout: Long) = synchronized(lock) {
        lock.wait(timeout)
    }

    @JvmName("notify2")
    fun notify() = synchronized(lock) {
        lock.notifyAll()
    }
}
