package de.bixilon.kutil.concurrent.lock

import de.bixilon.kutil.concurrent.lock.locks.reentrant.ReentrantRWLock
import kotlin.time.Duration

interface RWLock : Lock {
    fun acquire()
    fun acquire(timeout: Duration): Boolean

    fun release()

    companion object {
        fun rwlock(): RWLock = ReentrantRWLock()
    }
}
