package de.bixilon.kutil.concurrent.lock

import de.bixilon.kutil.concurrent.lock.locks.reentrant.ReentrantLock
import kotlin.time.Duration

interface Lock {
    fun lock(timeout: Duration): Boolean
    fun lock()
    fun unlock()


    companion object {
        fun lock(): Lock = ReentrantLock()
    }
}
