package de.bixilon.kutil.concurrent.lock.locks.reentrant

import de.bixilon.kutil.concurrent.lock.RWLock
import java.util.concurrent.TimeUnit
import java.util.concurrent.locks.ReentrantReadWriteLock
import kotlin.time.Duration


class ReentrantRWLock : RWLock {
    private val lock = ReentrantReadWriteLock()

    override fun acquire() = lock.readLock().lock()
    override fun acquire(timeout: Duration) = lock.readLock().tryLock(timeout.inWholeNanoseconds, TimeUnit.NANOSECONDS)
    override fun release() = lock.readLock().unlock()

    override fun lock() = lock.writeLock().lock()
    override fun lock(timeout: Duration) = lock.writeLock().tryLock(timeout.inWholeNanoseconds, TimeUnit.NANOSECONDS)
    override fun unlock() = lock.writeLock().unlock()

    override fun toString() = lock.toString()
}
