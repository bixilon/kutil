package de.bixilon.kutil.concurrent.lock

object LockUtil {

    inline fun <T> Lock.locked(runnable: () -> T): T {
        try {
            lock()
            return runnable.invoke()
        } finally {
            unlock()
        }
    }

    inline fun <T> RWLock.acquired(runnable: () -> T): T {
        try {
            acquire()
            return runnable.invoke()
        } finally {
            release()
        }
    }
}
