/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.concurrent.lock.locks

import de.bixilon.kutil.concurrent.lock.RWLock
import kotlin.time.Duration


class DebugLock(
    val lock: RWLock,
) : RWLock {

    override fun acquire(timeout: Duration): Boolean {
        val acquired = lock.acquire(timeout)
        Exception("acquire:${timeout.inWholeMilliseconds}:$acquired").printStackTrace()
        return acquired
    }


    override fun acquire() {
        lock.acquire()
        Exception("acquire").printStackTrace()
    }

    override fun release() {
        lock.release()
        Exception("release").printStackTrace()
    }

    override fun lock(timeout: Duration): Boolean {
        val locked = lock.lock(timeout)
        Exception("lock:${timeout.inWholeMilliseconds}:$locked").printStackTrace()
        return locked
    }

    override fun lock() {
        lock.lock()
        Exception("lock").printStackTrace()
    }

    override fun unlock() {
        lock.unlock()
        Exception("unlock").printStackTrace()
    }

    override fun toString() = lock.toString()
}
