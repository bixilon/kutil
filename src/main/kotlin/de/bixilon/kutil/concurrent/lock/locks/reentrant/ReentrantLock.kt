package de.bixilon.kutil.concurrent.lock.locks.reentrant

import de.bixilon.kutil.concurrent.lock.Lock
import java.util.concurrent.TimeUnit
import kotlin.time.Duration


class ReentrantLock : Lock {
    private val lock = java.util.concurrent.locks.ReentrantLock()

    override fun lock() = lock.lock()
    override fun lock(timeout: Duration) = lock.tryLock(timeout.inWholeNanoseconds, TimeUnit.NANOSECONDS)
    override fun unlock() = lock.unlock()
    
    override fun toString() = lock.toString()
}
