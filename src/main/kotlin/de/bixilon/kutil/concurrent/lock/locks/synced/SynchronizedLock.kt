/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.concurrent.lock.locks.synced

import de.bixilon.kutil.concurrent.lock.Lock
import de.bixilon.kutil.exception.ExceptionUtil.ignoreAll
import kotlin.time.Duration
import kotlin.time.TimeSource

@Suppress("PLATFORM_CLASS_MAPPED_TO_KOTLIN")
class SynchronizedLock(
    private val lock: Object = Object(),
) : Lock {
    @Volatile
    private var locked = false

    override fun lock(timeout: Duration): Boolean = synchronized(lock) {
        if (timeout.isNegative()) throw IllegalArgumentException("timeout <= 0")
        val start = TimeSource.Monotonic.markNow()
        var time = start
        while (locked) {
            val waited = time - start
            if (waited >= timeout) return false
            ignoreAll { lock.wait((timeout - waited).inWholeMilliseconds) }
            time = TimeSource.Monotonic.markNow()
        }
        locked = true
        lock.notifyAll()
        return true
    }

    override fun lock() = synchronized(lock) {
        while (locked) {
            ignoreAll { lock.wait() }
        }
        locked = true
        lock.notifyAll()
    }

    override fun unlock() = synchronized(lock) {
        if (!locked) throw IllegalMonitorStateException()
        locked = false
        lock.notifyAll()
    }

    override fun toString() = "SynchronizedLock(locked=$locked)"
}
