package de.bixilon.kutil.concurrent.lock.locks

import de.bixilon.kutil.concurrent.lock.RWLock
import kotlin.time.Duration

class ParentLock(
    private var parents: MutableList<RWLock> = mutableListOf(),
    val lock: RWLock = RWLock.rwlock(),
) : RWLock {

    fun add(parent: RWLock) {
        lock.lock()
        parents += parent
        lock.unlock()
    }

    operator fun plusAssign(parent: RWLock) = add(parent)

    fun remove(parent: RWLock) {
        lock.lock()
        parents -= parent
        lock.unlock()
    }

    operator fun minusAssign(parent: RWLock) = remove(parent)

    override fun acquire(timeout: Duration) = throw UnsupportedOperationException("ParentLock can not be timeout acquired!")
    override fun lock(timeout: Duration) = throw UnsupportedOperationException("ParentLock can not be timeout locked!")

    override fun acquire() {
        lock.acquire()
        for (parent in parents) {
            parent.acquire()
        }
    }

    override fun release() {
        lock.release()
        for (parent in parents) {
            parent.release()
        }
    }


    override fun lock() {
        lock.lock()
        for (parent in parents) {
            parent.lock()
        }
    }

    override fun unlock() {
        lock.unlock()
        for (parent in parents) {
            parent.unlock()
        }
    }
}
