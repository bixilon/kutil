/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.concurrent.pool.threads

import de.bixilon.kutil.concurrent.pool.ThreadPoolStates
import de.bixilon.kutil.concurrent.pool.runnable.ThreadPoolRunnable

class ThreadPoolThread(
    val manager: ThreadManager,
    name: String,
    priority: Int,
) : Thread() {

    init {
        this.name = name
        this.priority = priority
    }

    override fun run() {
        try {
            manager.threads.incrementAndGet()
            loop()
        } finally {
            manager.threads.decrementAndGet()
        }
    }


    private fun shouldStop() = manager.pool.state == ThreadPoolStates.STOPPING || manager.pool.state == ThreadPoolStates.STOPPED

    private fun loop() {
        var runnable: ThreadPoolRunnable
        while (true) {
            if (shouldStop()) break
            try {
                runnable = manager.queue.take()
            } catch (exception: InterruptedException) {
                continue
            }
            manager.pool.executor.execute(runnable)

            if (shouldStop()) break
        }
    }
}
