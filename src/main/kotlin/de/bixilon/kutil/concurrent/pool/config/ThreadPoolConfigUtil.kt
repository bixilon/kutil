package de.bixilon.kutil.concurrent.pool.config

import de.bixilon.kutil.primitive.IntUtil.toInt

object ThreadPoolConfigUtil {

    fun threads(property: String, environment: String, default: Int): Int {
        val property = System.getProperty(property) ?: System.getenv(environment)
        if (property != null) {
            val count = property.toInt()
            if (count < 0) {
                throw IllegalArgumentException("Invalid thread count: $count")
            }
            if (count == 0) {
                return Runtime.getRuntime().availableProcessors()
            }
            return count
        }

        return default
    }
}
