/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.concurrent.pool.runnable

import de.bixilon.kutil.concurrent.pool.ThreadPool

interface ThreadPoolRunnable : Comparable<ThreadPoolRunnable> {
    val priority: Int get() = ThreadPool.NORMAL
    val forcePool: Boolean
    val runnable: Runnable?
    val creation: Long

    override fun compareTo(other: ThreadPoolRunnable): Int {
        val difference = other.priority - priority
        if (difference != 0) {
            return difference
        }
        if (creation > other.creation) {
            return 1
        } else if (creation < other.creation) {
            return -1
        }

        return 0
    }
}
