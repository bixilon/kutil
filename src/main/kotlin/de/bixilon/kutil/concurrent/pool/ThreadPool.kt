/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.concurrent.pool

import de.bixilon.kutil.concurrent.pool.runnable.SimplePoolRunnable
import de.bixilon.kutil.concurrent.pool.runnable.ThreadPoolRunnable
import de.bixilon.kutil.concurrent.pool.threads.ThreadManager
import java.util.concurrent.*
import java.util.concurrent.atomic.AtomicInteger

open class ThreadPool(
    val threadCount: Int = Runtime.getRuntime().availableProcessors(),
    name: String = "Worker#%d",
    priority: Int = Thread.MIN_PRIORITY,
    var errorHandler: ((exception: Throwable) -> Unit)? = null,
) : ExecutorService {
    var state = ThreadPoolStates.STARTING
        private set

    private val queue: PriorityBlockingQueue<ThreadPoolRunnable> = PriorityBlockingQueue()
    private val running = AtomicInteger()
    internal val submitter = PoolSubmitter(queue, running, threadCount)
    private val threads = ThreadManager(name, threadCount, priority, running, this, queue)
    internal val executor = ThreadPoolExecutor(running, submitter, this)

    init {
        check(threadCount >= 1) { "Can not have < 1 thread!" }
        threads.start()
        state = ThreadPoolStates.STARTED
    }

    val queueSize: Int get() = queue.size


    fun execute(runnable: ThreadPoolRunnable) {
        if (submitter.submit(runnable)) return
        executor.execute(runnable)
    }

    override fun execute(runnable: Runnable) = execute(SimplePoolRunnable(runnable = runnable))
    operator fun plusAssign(runnable: ThreadPoolRunnable) = execute(runnable)
    operator fun plusAssign(runnable: Runnable) = execute(runnable)

    override fun shutdown() {
        state = ThreadPoolStates.STOPPING
        while (threads.running.get() > 0) {
            Thread.sleep(1L)
        }
        state = ThreadPoolStates.STOPPED
    }

    override fun shutdownNow(): List<Runnable> {
        state = ThreadPoolStates.STOPPING
        state = ThreadPoolStates.STOPPED
        return emptyList()
    }

    override fun isShutdown(): Boolean {
        return state == ThreadPoolStates.STOPPING
    }

    override fun isTerminated(): Boolean {
        return state == ThreadPoolStates.STOPPED
    }

    override fun awaitTermination(p0: Long, p1: TimeUnit): Boolean = TODO()
    override fun <T : Any?> submit(p0: Callable<T>): Future<T> = TODO()
    override fun <T : Any?> submit(p0: Runnable, p1: T): Future<T> = TODO()
    override fun submit(p0: Runnable): Future<*> = TODO()
    override fun <T : Any?> invokeAll(p0: MutableCollection<out Callable<T>>): MutableList<Future<T>> = TODO()
    override fun <T : Any?> invokeAll(p0: MutableCollection<out Callable<T>>, p1: Long, p2: TimeUnit): MutableList<Future<T>> = TODO()
    override fun <T : Any?> invokeAny(p0: MutableCollection<out Callable<T>>): T = TODO()
    override fun <T : Any?> invokeAny(p0: MutableCollection<out Callable<T>>, p1: Long, p2: TimeUnit): T = TODO()

    fun isBacklogging(): Boolean {
        return queue.size > 0
    }

    fun init() = Unit // does nothing, it gets initialized on class init

    companion object Priorities {
        const val HIGHEST = Int.MAX_VALUE
        const val HIGHER = 500
        const val HIGH = 100
        const val NORMAL = 0
        const val LOW = -HIGH
        const val LOWER = -HIGHER
        const val LOWEST = Int.MIN_VALUE
    }
}
