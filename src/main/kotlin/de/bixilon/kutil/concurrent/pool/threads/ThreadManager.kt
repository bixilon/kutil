/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.concurrent.pool.threads

import de.bixilon.kutil.concurrent.pool.ThreadPool
import de.bixilon.kutil.concurrent.pool.runnable.ThreadPoolRunnable
import java.util.concurrent.PriorityBlockingQueue
import java.util.concurrent.atomic.AtomicInteger

class ThreadManager(
    val name: String,
    val count: Int,
    val priority: Int,
    val running: AtomicInteger,
    val pool: ThreadPool,
    val queue: PriorityBlockingQueue<ThreadPoolRunnable>,
) {
    val threads = AtomicInteger()
    private var id = AtomicInteger(0)


    fun start() {
        for (i in 0 until count) {
            val name = name.replace("%d", this.id.getAndIncrement().toString())
            ThreadPoolThread(this, name, priority).start()
        }
        for (i in 0..WAIT_TIME) {
            if (threads.get() == count) return
            Thread.sleep(1L)
        }
        throw IllegalStateException("Waited ${WAIT_TIME}ms for the thread pool to come up, but it did not. Maybe something is wrong?")
    }


    private companion object {
        const val WAIT_TIME = 300
    }
}
