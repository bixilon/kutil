/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.concurrent.pool

import de.bixilon.kutil.concurrent.pool.runnable.InterruptableRunnable
import de.bixilon.kutil.concurrent.pool.runnable.ThreadPoolRunnable
import java.util.concurrent.atomic.AtomicInteger

class ThreadPoolExecutor(
    val running: AtomicInteger,
    val submitter: PoolSubmitter,
    val pool: ThreadPool,
) {

    private fun ThreadPoolRunnable.run() {
        runnable?.run()
    }

    private fun InterruptableRunnable.run() {
        try {
            thread = Thread.currentThread()
            runnable?.run()
            thread = null
        } catch (exception: InterruptedException) {
            interrupted = true
            submitter.forceSubmit(this)
        } finally {
            thread = null
        }
    }

    fun execute(runnable: ThreadPoolRunnable) {
        try {
            running.incrementAndGet()
            if (runnable is InterruptableRunnable) runnable.run() else runnable.run()
        } catch (exception: Throwable) {
            val handler = pool.errorHandler
            if (handler == null) {
                exception.printStackTrace()
            } else {
                handler(exception)
            }
        } finally {
            running.decrementAndGet()
        }
    }
}
