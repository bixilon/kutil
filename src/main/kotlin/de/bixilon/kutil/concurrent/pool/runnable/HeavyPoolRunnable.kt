/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.concurrent.pool.runnable

import de.bixilon.kutil.concurrent.pool.ThreadPool
import de.bixilon.kutil.time.TimeUtil.nanos
import java.util.concurrent.locks.ReentrantLock

class HeavyPoolRunnable(
    override val priority: Int = ThreadPool.NORMAL,
    interruptable: Boolean = false,
    override val forcePool: Boolean = true,
    override var runnable: Runnable? = null,
) : ThreadPoolRunnable, InterruptableRunnable {
    private val interruptLock = ReentrantLock()
    override val creation = nanos()
    override var interrupted = false
    override var thread: Thread? = null
        set(value) {
            if (value == null) {
                Thread.interrupted() // clear interrupted state
            }
            interruptLock.lock()
            field = value
            interruptLock.unlock()
        }


    override var interruptable: Boolean = interruptable
        set(value) {
            interruptLock.lock()
            if (field == value) {
                interruptLock.unlock()
                return
            }
            field = value
            interruptLock.unlock()
        }

    override fun interrupt() {
        interruptLock.lock()
        try {
            val thread = this.thread
            if (!interruptable || thread == null || interrupted) {
                return
            }
            thread.interrupt()
            interrupted = true
        } finally {
            interruptLock.unlock()
        }
    }

    override fun toString() = "HeavyPoolRunnable(priority=$priority, interruptable=${interruptable}, forcePool=$forcePool, runnable=$runnable)"
}
