/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.concurrent.pool

import de.bixilon.kutil.concurrent.lock.Lock
import de.bixilon.kutil.concurrent.pool.runnable.ThreadPoolRunnable
import java.util.concurrent.PriorityBlockingQueue
import java.util.concurrent.atomic.AtomicInteger

class PoolSubmitter(
    val queue: PriorityBlockingQueue<ThreadPoolRunnable>,
    val running: AtomicInteger,
    val count: Int,
) {
    private val lock = Lock.lock()


    fun submit(runnable: ThreadPoolRunnable): Boolean {
        lock.lock()
        val submit = runnable.forcePool || queue.size == 0 && running.get() < count
        if (!submit) {
            lock.unlock()
            return false
        }
        queue += runnable
        lock.unlock()
        return true
    }

    fun forceSubmit(runnable: ThreadPoolRunnable): Boolean {
        lock.lock()
        queue += runnable
        lock.unlock()
        return true
    }
}
