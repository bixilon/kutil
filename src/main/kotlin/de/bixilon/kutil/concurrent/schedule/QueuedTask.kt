/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.concurrent.schedule

import de.bixilon.kutil.concurrent.pool.ThreadPool
import de.bixilon.kutil.time.Delay
import kotlin.time.ComparableTimeMark
import kotlin.time.Duration
import kotlin.time.TimeSource

data class QueuedTask(
    val delay: Delay,
    override val maxDelay: Delay = Delay.INFINITE,
    override val priority: Int = ThreadPool.Priorities.NORMAL,
    override val runnable: Runnable,
) : SchedulerTask {
    private val created = TimeSource.Monotonic.markNow()
    override var thread: Thread? = null

    override fun getRemaining(time: ComparableTimeMark): Duration {
        return created - time + delay
    }


    override fun run(time: ComparableTimeMark) {
        if (time - created >= maxDelay) {
            return
        }
        try {
            thread = Thread.currentThread()
            runnable.run()
        } catch (exception: Throwable) {
            exception.printStackTrace()
        }
        thread = null
    }

    override fun toString() = "QueuedTask(delay=$delay, priority=$priority, runnable=$runnable)"
}
