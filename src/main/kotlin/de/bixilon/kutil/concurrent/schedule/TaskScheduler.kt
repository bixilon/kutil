/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.concurrent.schedule

import de.bixilon.kutil.concurrent.lock.Lock
import de.bixilon.kutil.concurrent.pool.DefaultThreadPool
import de.bixilon.kutil.concurrent.pool.runnable.ForcePooledRunnable
import de.bixilon.kutil.shutdown.AbstractShutdownReason
import de.bixilon.kutil.shutdown.ShutdownManager
import de.bixilon.kutil.time.Delay
import kotlin.time.Duration
import kotlin.time.Duration.Companion.milliseconds
import kotlin.time.TimeSource

object TaskScheduler {
    private val tasks: MutableSet<SchedulerTask> = mutableSetOf()
    private val lock = Lock.lock()
    private val thread = Thread({ Thread.currentThread().setOptions(); loop() }, "TaskSchedulerThread").apply { start() }

    @Volatile
    private var sleeping = false

    init {
        DefaultThreadPool.init()
    }

    private fun Thread.setOptions() {
        priority = Thread.MAX_PRIORITY
        uncaughtExceptionHandler = Thread.UncaughtExceptionHandler { _: Thread, error: Throwable ->
            error.printStackTrace()
            ShutdownManager.shutdown("TaskScheduler quit unexpectedly!", AbstractShutdownReason.CRASH)
        }
    }

    private fun work() {
        lock.lock()
        val time = TimeSource.Monotonic.markNow()
        var sleep = Duration.INFINITE
        val iterator = tasks.iterator()
        for (task in iterator) {
            val remaining = task.getRemaining(time)
            if (remaining < sleep) sleep = remaining
            if (task.isExecuting) continue
            if (remaining.isPositive()) continue

            DefaultThreadPool += ForcePooledRunnable(priority = task.priority) { task.run(time) }
            if (task is QueuedTask) {
                iterator.remove()
            } else {
                val next = task.getRemaining(time)
                if (next < sleep) sleep = next
            }
        }
        sleep -= 1.milliseconds // remove one ms jitter
        if (sleep < 1.milliseconds) sleep = 1.milliseconds // minimum sleep, be sure not to draw 100% cpu
        lock.unlock()
        try {
            sleeping = true
            Thread.sleep(sleep.inWholeMilliseconds)
        } catch (_: InterruptedException) {
        }
        sleeping = false
    }

    private fun loop() {
        while (true) {
            work()
        }
    }


    fun runLater(delay: Delay, runnable: Runnable): QueuedTask {
        val task = QueuedTask(
            delay = delay,
            runnable = runnable,
        )
        addTask(task)
        return task
    }

    private fun wakeThread() {
        var tries = 10
        while (!sleeping) {
            Thread.sleep(1) // wait some time, maybe then?
            if (tries-- < 0) {
                return // don't wake it, it might bork the state
            }
        }
        thread.interrupt()
    }

    fun addTask(task: SchedulerTask) {
        lock.lock()
        tasks += task
        wakeThread()
        lock.unlock()
    }

    operator fun plusAssign(task: SchedulerTask) = addTask(task)

    fun removeTask(task: SchedulerTask) {
        lock.lock()
        tasks -= task
        wakeThread()
        lock.unlock()
    }

    operator fun minusAssign(task: SchedulerTask) = removeTask(task)
}
