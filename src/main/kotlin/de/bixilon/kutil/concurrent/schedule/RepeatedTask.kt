/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.concurrent.schedule

import de.bixilon.kutil.concurrent.pool.ThreadPool
import de.bixilon.kutil.time.Delay
import de.bixilon.kutil.time.Interval
import java.util.concurrent.TimeUnit
import java.util.concurrent.locks.ReentrantLock
import kotlin.time.ComparableTimeMark
import kotlin.time.Duration
import kotlin.time.Duration.Companion.milliseconds
import kotlin.time.TimeSource

data class RepeatedTask(
    val interval: Interval,
    override val maxDelay: Delay = interval - 1.milliseconds,
    override val priority: Int = ThreadPool.Priorities.NORMAL,
    override val runnable: Runnable,
) : SchedulerTask {
    val lock = ReentrantLock()
    override var thread: Thread? = null
    private var executions = 0
    private var lastExecution: ComparableTimeMark = TimeSource.Monotonic.markNow()


    override fun getRemaining(time: ComparableTimeMark): Duration {
        if (executions == 0) return Duration.ZERO
        return lastExecution - time + interval
    }

    override fun run(time: ComparableTimeMark) {
        if (!lock.tryLock(1L, TimeUnit.MILLISECONDS)) {
            return
        }
        try {
            if (isExecuting) return

            val delta = time - lastExecution

            if (executions > 0 && (delta < interval && delta >= maxDelay)) { // too long due
                return
            }
            try {
                thread = Thread.currentThread()
                runnable.run()
            } catch (exception: Throwable) {
                exception.printStackTrace()
            }
            thread = null

            executions++
            lastExecution = time
        } finally {
            lock.unlock()
        }
    }

    override fun toString() = "RepeatedTask(interval=$interval, maxDelay=$maxDelay, priority=$priority, runnable=$runnable)"
}
