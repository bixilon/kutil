/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.concurrent.worker.unconditional

import de.bixilon.kutil.collections.CollectionUtil.synchronizedListOf
import de.bixilon.kutil.concurrent.pool.DefaultThreadPool
import de.bixilon.kutil.concurrent.pool.ThreadPool
import de.bixilon.kutil.concurrent.pool.runnable.SimplePoolRunnable
import de.bixilon.kutil.concurrent.worker.WorkerExecutor
import de.bixilon.kutil.concurrent.worker.WorkerStates
import de.bixilon.kutil.latch.AbstractLatch
import de.bixilon.kutil.latch.ParentLatch
import de.bixilon.kutil.latch.SimpleLatch

class UnconditionalWorker(
    val pool: ThreadPool = DefaultThreadPool,
    val forcePool: Boolean = false,
    val threadSafe: Boolean = false,
    val autoWork: Boolean = true,
    var errorHandler: ((Throwable) -> Unit)? = null,
) {
    private val tasks: MutableList<UnconditionalTask> = if (threadSafe) synchronizedListOf() else mutableListOf()
    private val running = SimpleLatch(0)
    var state: WorkerStates = WorkerStates.PREPARING
        private set

    fun add(task: UnconditionalTask) {
        if (state != WorkerStates.PREPARING) {
            throw IllegalStateException("Invalid state: $state")
        }
        if (autoWork) {
            running.inc()
            val runnable = task.createRunnable(null)
            if (pool.submitter.submit(runnable)) {
                return
            }
            running.dec()
        }
        tasks += task
    }

    operator fun plusAssign(task: UnconditionalTask) = add(task)

    fun add(executor: WorkerExecutor) = add(UnconditionalTask(executor = executor))

    operator fun plusAssign(executor: WorkerExecutor) = add(executor)

    private fun UnconditionalTask.createRunnable(latch: AbstractLatch?) = SimplePoolRunnable(priority) { run(this.executor, latch) }


    private fun run(executor: (AbstractLatch?) -> Unit, latch: AbstractLatch?) {
        try {
            executor.invoke(latch)
        } catch (error: Throwable) {
            val handler = this.errorHandler
            if (handler == null) {
                error.printStackTrace()
            } else {
                handler.invoke(error)
            }
        }
        latch?.dec()
        running.dec()
    }

    fun work(latch: AbstractLatch? = null) {
        if (state != WorkerStates.PREPARING) {
            throw IllegalStateException("Invalid state: $state")
        }
        state = WorkerStates.WORKING

        val innerLatch = if (latch == null) SimpleLatch(1) else ParentLatch(1, latch)

        tasks.sortWith { a, b -> b.priority - a.priority }
        innerLatch.plus(tasks.size)

        for (task in tasks) {
            running.inc()
            val runnable = task.createRunnable(innerLatch)
            if (forcePool) {
                pool.submitter.forceSubmit(runnable)
                running.waitIfGreater(pool.threadCount)
                continue
            }
            val submitted = pool.submitter.submit(runnable)
            if (submitted) continue
            run(task.executor, innerLatch)
        }


        innerLatch.dec()
        innerLatch.await()
        running.await()

        state = WorkerStates.FINISHED
    }

    fun reset() {
        tasks.clear()
        state = WorkerStates.PREPARING
    }
}
