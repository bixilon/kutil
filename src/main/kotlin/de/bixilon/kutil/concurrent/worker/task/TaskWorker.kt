/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.concurrent.worker.task

import de.bixilon.kutil.collections.CollectionUtil.synchronizedListOf
import de.bixilon.kutil.collections.CollectionUtil.synchronizedMapOf
import de.bixilon.kutil.concurrent.lock.RWLock
import de.bixilon.kutil.concurrent.pool.DefaultThreadPool
import de.bixilon.kutil.concurrent.pool.ThreadPool
import de.bixilon.kutil.concurrent.pool.runnable.ForcePooledRunnable
import de.bixilon.kutil.concurrent.pool.runnable.SimplePoolRunnable
import de.bixilon.kutil.concurrent.pool.runnable.ThreadPoolRunnable
import de.bixilon.kutil.concurrent.worker.WorkerExecutor
import de.bixilon.kutil.concurrent.worker.WorkerStates
import de.bixilon.kutil.latch.AbstractLatch
import de.bixilon.kutil.latch.ParentLatch
import de.bixilon.kutil.latch.SimpleLatch
import java.util.concurrent.atomic.AtomicBoolean

class TaskWorker(
    var errorHandler: (task: WorkerTask, exception: Throwable) -> Unit = { _, error -> error.printStackTrace() },
    private val pool: ThreadPool = DefaultThreadPool,
    private val forcePool: Boolean = false,
) {
    private val tasks: MutableMap<Any, WorkerTask> = synchronizedMapOf()
    private val unnamedTasks: MutableList<WorkerTask> = synchronizedListOf()
    var state: WorkerStates = WorkerStates.PREPARING
        private set

    operator fun plusAssign(task: WorkerTask) {
        check(state == WorkerStates.PREPARING) { "Task worker is already working!" }
        if (task.dependencies.contains(task.identifier)) {
            throw IllegalArgumentException("Task can not depend on itself!")
        }
        if (task.identifier != null) {
            val previous = tasks.put(task.identifier, task)
            if (previous != null) {
                System.err.println("Task ${task.identifier} replaced existing task!")
            }
        } else {
            unnamedTasks += task
        }
    }

    operator fun minusAssign(identifier: Any) {
        this.tasks -= identifier
    }

    operator fun plusAssign(runnable: WorkerExecutor) {
        this += WorkerTask(executor = runnable)
    }

    private fun canWork(task: WorkerTask, lock: RWLock, done: Set<Any>): Boolean {
        if (task.dependencies.isEmpty()) {
            return true
        }
        lock.acquire()
        for (dependency in task.dependencies) {
            if (dependency in done) {
                continue
            }
            lock.release()
            return false
        }
        lock.release()
        return true
    }

    private fun collectTasks(): MutableList<WorkerTask> {
        val tasks = this.tasks.values.toMutableList()
        tasks += unnamedTasks
        tasks.sortWith { a, b -> b.priority - a.priority }

        return tasks
    }

    @Synchronized
    fun work(progress: AbstractLatch? = null) {
        if (state != WorkerStates.PREPARING) throw IllegalStateException("Invalid state: $state")
        state = WorkerStates.WORKING
        val tasks = collectTasks()

        val done: MutableSet<Any> = mutableSetOf()
        val doneLock = RWLock.rwlock()

        val workerProgress = if (progress == null) SimpleLatch(1) else ParentLatch(1, progress)

        val stop = AtomicBoolean()

        while (tasks.isNotEmpty()) {
            val iterator = tasks.iterator()

            var waiting = workerProgress.count
            task@ for (task in iterator) {
                if (!canWork(task, doneLock, done)) {
                    continue
                }
                iterator.remove()

                val taskProgress = ParentLatch(1, workerProgress)
                waiting += 1
                val runnable = Runnable {
                    try {
                        task.executor(taskProgress)
                        taskProgress.countDown()
                    } catch (error: Throwable) {
                        this.errorHandler.invoke(task, error)
                        taskProgress.count = 0
                        stop.set(true)
                        return@Runnable
                    }
                    task.identifier?.let { doneLock.lock(); done += it;doneLock.unlock() }
                }


                pool += createRunnable(task.priority, runnable)
            }
            if (stop.get()) break
            workerProgress.waitIfGreater(waiting)
            if (stop.get()) break
        }

        workerProgress.dec()
        workerProgress.await()
        state = WorkerStates.FINISHED
    }

    private fun createRunnable(priority: Int, runnable: Runnable): ThreadPoolRunnable {
        if (this.forcePool) {
            return ForcePooledRunnable(priority, runnable)
        }
        return SimplePoolRunnable(priority, runnable)
    }
}
