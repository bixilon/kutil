/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.concurrent.queue

import de.bixilon.kutil.concurrent.Waker
import de.bixilon.kutil.exception.ExceptionUtil.catchAll
import kotlin.time.Duration
import kotlin.time.TimeSource

class Queue(initialCapacity: Int = 100) {
    private val queue: MutableList<Runnable> = ArrayList(initialCapacity)
    private val lock = Object()
    private val waker = Waker(lock)

    val size: Int
        get() = queue.size

    fun add(runnable: Runnable) = synchronized(lock) {
        queue += runnable
        waker.notify()
    }

    operator fun plusAssign(runnable: Runnable) {
        add(runnable)
    }

    fun work(maxJobs: Int = Int.MAX_VALUE) {
        if (queue.size == 0) return

        var done = 0
        while (true) {
            val item: Runnable
            synchronized(lock) {
                if (queue.size <= 0) return
                item = queue.removeFirst()
            }
            catchAll { item.run() }

            if (++done >= maxJobs) {
                break
            }
        }
    }

    fun workTimeLimited(time: Duration) {
        if (!time.isPositive()) throw IllegalArgumentException("No time to do jobs?")
        if (queue.size == 0) return

        val start = TimeSource.Monotonic.markNow()

        while (true) {
            val item: Runnable
            synchronized(lock) {
                if (queue.size <= 0) return
                item = queue.removeFirst()
            }
            catchAll { item.run() }

            if (TimeSource.Monotonic.markNow() - start >= time) {
                break
            }
        }
    }

    fun workBlocking(timeout: Duration = Duration.ZERO) {
        while (true) {
            val item: Runnable
            synchronized(lock) {
                if (queue.size <= 0) {
                    if (timeout.isPositive()) {
                        waker.wait(timeout)
                    } else {
                        waker.wait()
                    }
                }
                if (queue.size <= 0) return
                item = queue.removeFirst()
            }
            catchAll { item.run() }
        }
    }


    @JvmName("wait2")
    fun wait() = waker.wait()

    fun wait(timeout: Duration) = wait(timeout.inWholeMilliseconds)

    @JvmName("wait2")
    fun wait(timeout: Long) = waker.wait(timeout)

    fun clear() = synchronized(lock) {
        queue.clear()
    }
}
