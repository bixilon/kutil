package de.bixilon.kutil.buffer.bytes.out

import de.bixilon.kutil.buffer.ByteBufferUtil.toByteArray
import org.junit.jupiter.api.Test
import java.nio.ByteBuffer
import kotlin.test.assertContentEquals


class OutByteBufferTest {

    @Test
    fun `single bytes`() {
        val buffer = OutByteBuffer()
        buffer.writeByte(0x01.toByte())
        buffer.writeByte(0x02)
        buffer.writeByte(0x03)
        assertContentEquals(buffer.toArray(), byteArrayOf(0x01, 0x02, 0x03))
    }

    @Test
    fun `bare byte array`() {
        val buffer = OutByteBuffer()
        buffer.writeBareByteArray(byteArrayOf(0x05, 0x06))
        assertContentEquals(buffer.toArray(), byteArrayOf(0x05, 0x06))
    }

    @Test
    fun `byte array`() {
        val buffer = OutByteBuffer()
        buffer.writeByteArray(byteArrayOf(0x05, 0x06))
        assertContentEquals(buffer.toArray(), byteArrayOf(0x02, 0x05, 0x06))
    }

    @Test
    fun `single shorts`() {
        val buffer = OutByteBuffer()
        buffer.writeShort(0x1234.toShort())
        buffer.writeShort(0x5678)
        assertContentEquals(buffer.toArray(), byteArrayOf(0x12, 0x34, 0x56, 0x78))
    }

    @Test
    fun `bare short array`() {
        val buffer = OutByteBuffer()
        buffer.writeBareShortArray(shortArrayOf(0x1234, 0x5678))
        assertContentEquals(buffer.toArray(), byteArrayOf(0x12, 0x34, 0x56, 0x78))
    }

    @Test
    fun `short array`() {
        val buffer = OutByteBuffer()
        buffer.writeShortArray(shortArrayOf(0x1234, 0x5678))
        assertContentEquals(buffer.toArray(), byteArrayOf(0x02, 0x12, 0x34, 0x56, 0x78))
    }

    @Test
    fun `single ints`() {
        val buffer = OutByteBuffer()
        buffer.writeInt(0x12345678)
        buffer.writeInt(0x87654321)
        assertContentEquals(buffer.toArray(), byteArrayOf(0x12, 0x34, 0x56, 0x78, 0x87.toByte(), 0x65, 0x43, 0x21))
    }

    @Test
    fun `bare int array`() {
        val buffer = OutByteBuffer()
        buffer.writeBareIntArray(intArrayOf(0x12345678, 0x87654321.toInt()))
        assertContentEquals(buffer.toArray(), byteArrayOf(0x12, 0x34, 0x56, 0x78, 0x87.toByte(), 0x65, 0x43, 0x21))
    }

    @Test
    fun `int array`() {
        val buffer = OutByteBuffer()
        buffer.writeIntArray(intArrayOf(0x12345678, 0x87654321.toInt()))
        assertContentEquals(buffer.toArray(), byteArrayOf(0x02, 0x12, 0x34, 0x56, 0x78, 0x87.toByte(), 0x65, 0x43, 0x21))
    }

    @Test
    fun `single longs`() {
        val buffer = OutByteBuffer()
        buffer.writeLong(0x1234567812345678)
        buffer.writeLong(0x7765432187654321)
        assertContentEquals(buffer.toArray(), byteArrayOf(0x12, 0x34, 0x56, 0x78, 0x12, 0x34, 0x56, 0x78.toByte(), 0x77, 0x65, 0x43, 0x21, 0x87.toByte(), 0x65, 0x43, 0x21))
    }

    @Test
    fun `bare long array`() {
        val buffer = OutByteBuffer()
        buffer.writeBareLongArray(longArrayOf(0x1234567812345678, 0x7765432187654321))
        assertContentEquals(buffer.toArray(), byteArrayOf(0x12, 0x34, 0x56, 0x78, 0x12, 0x34, 0x56, 0x78.toByte(), 0x77, 0x65, 0x43, 0x21, 0x87.toByte(), 0x65, 0x43, 0x21))
    }

    @Test
    fun `long array`() {
        val buffer = OutByteBuffer()
        buffer.writeLongArray(longArrayOf(0x1234567812345678, 0x7765432187654321))
        assertContentEquals(buffer.toArray(), byteArrayOf(0x02, 0x12, 0x34, 0x56, 0x78, 0x12, 0x34, 0x56, 0x78.toByte(), 0x77, 0x65, 0x43, 0x21, 0x87.toByte(), 0x65, 0x43, 0x21))
    }

    @Test
    fun `var int`() {
        val buffer = OutByteBuffer()
        buffer.writeVarInt(0)
        buffer.writeVarInt(0x7F)
        buffer.writeVarInt(0x80)
        buffer.writeVarInt(0x81)
        buffer.writeVarInt(-1)
        assertContentEquals(buffer.toArray(), byteArrayOf(0x0, 0x7F, 0x80.toByte(), 0x01, 0x81.toByte(), 0x01, 0xFF.toByte(), 0xFF.toByte(), 0xFF.toByte(), 0xFF.toByte(), 0x0F))
    }

    @Test
    fun `var long`() {
        val buffer = OutByteBuffer()
        buffer.writeVarLong(0)
        buffer.writeVarLong(0x7F)
        buffer.writeVarLong(0x80)
        buffer.writeVarLong(0x81)
        buffer.writeVarLong(-1)
        assertContentEquals(buffer.toArray(), byteArrayOf(0x0, 0x7F, 0x80.toByte(), 0x01, 0x81.toByte(), 0x01, 0xFF.toByte(), 0xFF.toByte(), 0xFF.toByte(), 0xFF.toByte(), 0xFF.toByte(), 0xFF.toByte(), 0xFF.toByte(), 0xFF.toByte(), 0xFF.toByte(), 0x01))
    }

    @Test
    fun `write to byte array`() {
        val target = ByteArray(5) { 0x77 }
        val buffer = OutByteBuffer()
        buffer.writeByte(0x03); buffer.writeByte(0x04); buffer.writeByte(0x08)
        buffer.writeTo(target, 1)
        assertContentEquals(target, byteArrayOf(0x77, 0x03, 0x04, 0x08, 0x77))
    }

    @Test
    fun `write to byte buffer`() {
        val target = ByteBuffer.allocate(5)
        val buffer = OutByteBuffer()
        target.put(0x77)
        buffer.writeByte(0x03); buffer.writeByte(0x04); buffer.writeByte(0x08)
        buffer.writeTo(target)
        target.put(0x77)
        assertContentEquals(target.toByteArray(), byteArrayOf(0x77, 0x03, 0x04, 0x08, 0x77))
    }
}
