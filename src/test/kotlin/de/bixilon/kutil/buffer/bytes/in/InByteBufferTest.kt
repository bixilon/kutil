package de.bixilon.kutil.buffer.bytes.`in`

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import kotlin.test.assertContentEquals
import kotlin.test.assertEquals

class InByteBufferTest {

    @Test
    fun `single bytes`() {
        val buffer = InByteBuffer(byteArrayOf(0x01, 0x02, 0x04))
        assertEquals(buffer.readByte(), 0x01)
        assertEquals(buffer.readByte(), 0x02)
        assertEquals(buffer.readByte(), 0x04)
        assertThrows<ByteBufferUnderflowException> { buffer.readByte() }
    }

    @Test
    fun `unsigned bytes`() {
        val buffer = InByteBuffer(byteArrayOf(0xFF.toByte(), 0x8F.toByte()))
        assertEquals(buffer.readUnsignedByte(), 0xFF)
        assertEquals(buffer.readUnsignedByte(), 0x8F)
        assertThrows<ByteBufferUnderflowException> { buffer.readUnsignedByte() }
    }

    @Test
    fun `byte array`() {
        val buffer = InByteBuffer(byteArrayOf(0x01, 0x02, 0x03, 0x04, 0x05))
        assertEquals(buffer.readByte(), 0x01)
        assertContentEquals(buffer.readByteArray(2), byteArrayOf(0x02, 0x03))
        assertContentEquals(buffer.readByteArray(2), byteArrayOf(0x04, 0x05))
        assertThrows<ByteBufferUnderflowException> { buffer.readByteArray(1) }
    }

    @Test
    fun `single shorts`() {
        val buffer = InByteBuffer(byteArrayOf(0x01, 0x02, 0x04, 0x05))
        assertEquals(buffer.readShort(), 0x0102)
        assertEquals(buffer.readShort(), 0x0405)
        assertThrows<ByteBufferUnderflowException> { buffer.readShort() }
    }

    @Test
    fun `unsigned shorts`() {
        val buffer = InByteBuffer(byteArrayOf(0xFF.toByte(), 0x8F.toByte(), 0x8D.toByte(), 0xAB.toByte()))
        assertEquals(buffer.readUnsignedShort(), 0xFF8F)
        assertEquals(buffer.readUnsignedShort(), 0x8DAB)
        assertThrows<ByteBufferUnderflowException> { buffer.readUnsignedShort() }
    }

    @Test
    fun `short array`() {
        val buffer = InByteBuffer(byteArrayOf(0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09))
        assertEquals(buffer.readByte(), 0x01)
        assertContentEquals(buffer.readShortArray(2), shortArrayOf(0x0203, 0x0405))
        assertContentEquals(buffer.readShortArray(2), shortArrayOf(0x0607, 0x0809))
        assertThrows<ByteBufferUnderflowException> { buffer.readShortArray(1) }
    }

    @Test
    fun `single ints`() {
        val buffer = InByteBuffer(byteArrayOf(0x01, 0x02, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09))
        assertEquals(buffer.readInt(), 0x01020405)
        assertEquals(buffer.readInt(), 0x06070809)
        assertThrows<ByteBufferUnderflowException> { buffer.readInt() }
    }

    @Test
    fun `unsigned ints`() {
        val buffer = InByteBuffer(byteArrayOf(0xFF.toByte(), 0x8F.toByte(), 0x8D.toByte(), 0xAB.toByte()))
        assertEquals(buffer.readUnsignedInt(), 0xFF8F8DAB)
        assertThrows<ByteBufferUnderflowException> { buffer.readUnsignedInt() }
    }

    @Test
    fun `int array`() {
        val buffer = InByteBuffer(byteArrayOf(0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09))
        assertEquals(buffer.readByte(), 0x01)
        assertContentEquals(buffer.readIntArray(2), intArrayOf(0x02030405, 0x06070809))
        assertThrows<ByteBufferUnderflowException> { buffer.readIntArray(1) }
    }

    @Test
    fun `var int`() {
        val buffer = InByteBuffer(byteArrayOf(0x01, 0x7F, 0x80.toByte(), 0x01))
        assertEquals(buffer.readVarInt(), 0x01)
        assertEquals(buffer.readVarInt(), 0x07F)
        assertEquals(buffer.readVarInt(), 0x80)
        assertThrows<ByteBufferUnderflowException> { buffer.readVarInt() }
    }

    @Test
    fun `var int array`() {
        val buffer = InByteBuffer(byteArrayOf(0x01, 0x01, 0x7F, 0x80.toByte(), 0x01))
        assertEquals(buffer.readByte(), 0x01)
        assertContentEquals(buffer.readVarIntArray(3), intArrayOf(0x01, 0x7F, 0x80))
        assertThrows<ByteBufferUnderflowException> { buffer.readVarIntArray(1) }
    }
}
