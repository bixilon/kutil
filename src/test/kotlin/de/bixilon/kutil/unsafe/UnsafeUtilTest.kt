/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.unsafe

import de.bixilon.kutil.unsafe.UnsafeUtil.setUnsafeAccessible
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import java.util.*
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class UnsafeUtilTest {
    private val field = "test"

    @Test
    fun retrieve() {
        assertNotNull(UnsafeUtil.UNSAFE)
    }

    @Test
    fun `make same module accessible field unsafe accessible`() {
        val field = UnsafeUtilTest::class.java.getDeclaredField("field")
        field.setUnsafeAccessible()
        assertEquals(field.get(this), "test")
    }

    @Test
    fun `make system module accessible field unsafe accessible`() {
        val field = System::class.java.getDeclaredField("in")
        field.setUnsafeAccessible()
        assertEquals(field.get(this), System.`in`)
    }

    @Test
    fun `make private system module accessible field unsafe accessible`() {
        val field = System::class.java.getDeclaredField("props")
        field.setUnsafeAccessible()
        assertTrue(field.get(this) is Properties)
    }
}
