package de.bixilon.kutil.ansi

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class ANSITest {

    @Test
    fun `generate ansi code 0`() {
        assertEquals(ANSI.formatting(0), "\u001b[0m")
    }

    @Test
    fun `reset code`() {
        assertEquals(ANSI.RESET, "\u001b[0m")
    }
}
