/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.string

import de.bixilon.kutil.string.WhitespaceUtil.removeMultipleWhitespaces
import de.bixilon.kutil.string.WhitespaceUtil.removeTrailingWhitespaces
import de.bixilon.kutil.string.WhitespaceUtil.removeWhitespaces
import de.bixilon.kutil.string.WhitespaceUtil.trimWhitespaces
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals


internal class WhitespaceUtilTest {

    @Test
    fun `normal String`() {
        assertEquals("test", "test".removeTrailingWhitespaces())
    }

    @Test
    fun `empty String`() {
        assertEquals("", "".removeTrailingWhitespaces())
    }

    @Test
    fun `remove prefixed Whitespaces`() {
        assertEquals("456789", "   456789".removeTrailingWhitespaces())
    }

    @Test
    fun `remove suffixed Whitespaces`() {
        assertEquals("hjkk", "hjkk     ".removeTrailingWhitespaces())
    }

    @Test
    fun `remove prefixed and suffixed Whitespaces`() {
        assertEquals("840m", "  840m     ".removeTrailingWhitespaces())
    }

    @Test
    fun `remove spaces and tab`() {
        assertEquals("840m", "\t840m     ".removeTrailingWhitespaces())
    }

    @Test
    fun `do not remove middle spaces`() {
        assertEquals("z2  4ß0", " z2  4ß0  ".removeTrailingWhitespaces())
    }

    @Test
    fun `remove multiple spaces 1`() {
        assertEquals("ua", "ua".removeMultipleWhitespaces())
    }

    @Test
    fun `remove multiple spaces 2`() {
        assertEquals("aa33 ", "aa33 ".removeMultipleWhitespaces())
    }

    @Test
    fun `remove multiple spaces 3`() {
        assertEquals(" hfi9 ", " hfi9 ".removeMultipleWhitespaces())
    }

    @Test
    fun `remove multiple spaces 4`() {
        assertEquals(" 145", "  145".removeMultipleWhitespaces())
    }

    @Test
    fun `remove multiple spaces 5`() {
        assertEquals(" 83pk", "   83pk".removeMultipleWhitespaces())
    }

    @Test
    fun `remove multiple spaces 6`() {
        assertEquals(" 094i ", "   094i           ".removeMultipleWhitespaces())
    }

    @Test
    fun `remove multiple spaces 7`() {
        assertEquals(" 2o9 32uoiu 234 ", "   2o9  32uoiu                    234 ".removeMultipleWhitespaces())
    }

    @Test
    fun `remove multiple spaces and trailing`() {
        assertEquals("3225 345", "  3225    345  ".trimWhitespaces())
    }

    @Test
    fun `remove all whitespaces`() {
        assertEquals("3225345", " 3225     34 5  ".removeWhitespaces())
    }
}
