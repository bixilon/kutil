/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.string

import de.bixilon.kutil.string.StringUtil.fill
import de.bixilon.kutil.string.StringUtil.formatPlaceholder
import de.bixilon.kutil.string.StringUtil.getBetween
import de.bixilon.kutil.string.StringUtil.isLowercase
import de.bixilon.kutil.string.StringUtil.isUppercase
import de.bixilon.kutil.string.StringUtil.toCamelCase
import de.bixilon.kutil.string.StringUtil.toSnakeCase
import de.bixilon.kutil.string.StringUtil.truncate
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue


internal class StringUtilTest {

    @Test
    fun `test snakecase 1`() {
        assertEquals("test_super_cool", "testSuperCool".toSnakeCase())
    }

    @Test
    fun `test snakecase 2`() {
        assertEquals("test_super_cool", "test_superCool".toSnakeCase())
    }

    @Test
    fun `test snakecase 3`() {
        assertEquals("test", "Test".toSnakeCase())
    }

    @Test
    fun `test snakecase 4`() {
        assertEquals("test", "TEST".toSnakeCase())
    }

    @Test
    fun `test snakecase 5`() {
        assertEquals("abcd_ef", "ABcdEF".toSnakeCase())
    }

    @Test
    fun `test camelcase 1`() {
        assertEquals("testSuperCool", "test_super_cool".toCamelCase())
    }

    @Test
    fun `test camelcase 2`() {
        assertEquals("test", "test".toCamelCase())
    }

    @Test
    fun `test camelcase 3`() {
        assertEquals("testThis", "test__this".toCamelCase())
    }

    @Test
    fun `test uppercase 1`() {
        assertFalse("NoTUpPeRcAsE".isUppercase())
    }

    @Test
    fun `test uppercase 2`() {
        assertFalse("nOTUPPERCASE".isUppercase())
    }

    @Test
    fun `test uppercase 3`() {
        assertTrue("UPPERCASE".isUppercase())
    }

    @Test
    fun `test uppercase 4`() {
        assertTrue("UPPER_CASE".isUppercase())
    }

    @Test
    fun `test uppercase 5`() {
        assertFalse("UpPER_CASE".isUppercase())
    }

    @Test
    fun `test lowercase 1`() {
        assertFalse("NoTLoWeRcAsE".isLowercase())
    }

    @Test
    fun `test lowercase 2`() {
        assertFalse("Notlowercase".isLowercase())
    }

    @Test
    fun `test lowercase 3`() {
        assertTrue("lowercase".isLowercase())
    }

    @Test
    fun `test lowercase 4`() {
        assertTrue("lower_case".isLowercase())
    }

    @Test
    fun `test lowercase 5`() {
        assertFalse("loWer_case".isUppercase())
    }

    @Test
    fun `truncate test too short`() {
        assertEquals("test", "test".truncate(1000))
    }

    @Test
    fun `truncate test too long`() {
        assertEquals("tes", "test".truncate(3))
    }

    @Test
    fun `truncate test length fits`() {
        assertEquals("test", "test".truncate(4))
    }

    @Test
    fun `truncate test invalid length`() {
        assertThrows<IllegalArgumentException> { "test".truncate(-1) }
    }

    @Test
    fun `fill with more`() {
        val filled = "abc".fill('f', 5)
        assertEquals(filled, "ffabc")
    }

    @Test
    fun `fill with equal`() {
        val filled = "abcde".fill('f', 5)
        assertEquals(filled, "abcde")
    }

    @Test
    fun `fill with less`() {
        val filled = "abcdef".fill('f', 5)
        assertEquals(filled, "abcdef")
    }

    @Test
    fun `placeholder formatting 1`() {
        val template = "abc\${var}"
        val formatted = template.formatPlaceholder("var" to "filled")
        assertEquals("abcfilled", formatted)
    }

    @Test
    fun `placeholder formatting 2`() {
        val template = "abc\${var}"
        val formatted = template.formatPlaceholder("another" to "me", "var" to "filled")
        assertEquals("abcfilled", formatted)
    }

    @Test
    fun `placeholder formatting 3`() {
        val template = "abc\${var} and \${another}"
        val formatted = template.formatPlaceholder("another" to "me", "var" to "filled")
        assertEquals("abcfilled and me", formatted)
    }

    @Test
    fun `between single`() {
        val string = "abcdef"
        val between = string.getBetween("ab", "ef")
        assertEquals(between, "cd")
    }
}
