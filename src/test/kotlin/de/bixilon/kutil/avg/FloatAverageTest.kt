package de.bixilon.kutil.avg

import de.bixilon.kutil.avg._float.FloatAverage
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import kotlin.time.Duration.Companion.milliseconds

class FloatAverageTest {

    @Test
    fun testEmpty() {
        val avg = FloatAverage(10.milliseconds)
        assertEquals(avg.avg, 0.0f)
    }

    @Test
    fun testSingleValue() {
        val avg = FloatAverage(10.milliseconds)
        avg += 9.0f
        assertEquals(avg.avg, 9.0f)
    }

    @Test
    fun test2Values() {
        val avg = FloatAverage(10.milliseconds)
        avg += 9.0f
        avg += 5.0f
        assertEquals(avg.avg, 7.0f)
    }

    @Test
    fun test3Values() {
        val avg = FloatAverage(10.milliseconds)
        avg += 10.0f
        avg += 5.0f
        avg += 0.0f
        assertEquals(avg.avg, 5.0f)
    }

    @Test
    fun testExpiringValues() {
        val avg = FloatAverage(10.milliseconds)
        avg += 10.0f
        Thread.sleep(11L)
        avg += 16.0f
        avg += 20.0f
        assertEquals(avg.avg, 18.0f)
    }
}
