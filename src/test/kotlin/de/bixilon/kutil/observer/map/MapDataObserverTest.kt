/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.observer.map

import de.bixilon.kutil.observer.map.MapObserver.Companion.observeMap
import de.bixilon.kutil.observer.map.MapObserver.Companion.observedMap
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import kotlin.test.assertEquals

@TestInstance(TestInstance.Lifecycle.PER_METHOD)
internal class MapDataObserverTest {
    private val value by observedMap(mutableMapOf("Included" to "first"))

    @Test
    fun testPut() {
        val toAdd = "Changed"
        val toAddValue = "value"
        var observerNotified = false
        this::value.observeMap(this) {
            assert(it.removes.isEmpty())
            assert(it.adds.size == 1)
            val added = it.adds.first()
            assertEquals(added.key, toAdd)
            assertEquals(added.value, "value")
            observerNotified = true
        }
        value[toAdd] = toAddValue
        assert(observerNotified)
    }

    @Test
    fun testRemove() {
        val toRemove = "Included"
        var observerNotified = false
        this::value.observeMap(this) {
            assert(it.adds.isEmpty())
            assert(it.removes.size == 1)
            val removed = it.removes.first()
            assertEquals(removed.key, toRemove)
            assertEquals(removed.value, "first")
            observerNotified = true
        }
        value -= toRemove
        assert(observerNotified)
    }

    // TODO @Test
    fun mutableIterator() {
        val toRemove = "Included"
        var observerNotified = false
        this::value.observeMap(this) {
            assert(it.adds.isEmpty())
            assert(it.removes.size == 1)
            val removed = it.removes.first()
            assertEquals(removed.key, toRemove)
            assertEquals(removed.value, "first")
            observerNotified = true
        }
        val iterator = this.value.iterator()
        for (value in iterator) {
            iterator.remove()
        }
        assert(observerNotified)
    }
}
