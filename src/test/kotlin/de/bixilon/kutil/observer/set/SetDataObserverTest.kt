/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.observer.set

import de.bixilon.kutil.observer.set.SetObserver.Companion.observeSet
import de.bixilon.kutil.observer.set.SetObserver.Companion.observedSet
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import kotlin.test.assertEquals


@TestInstance(TestInstance.Lifecycle.PER_METHOD)
internal class SetDataObserverTest {
    private val value by observedSet(mutableSetOf("Included"))

    @Test
    fun testAdd() {
        val toAdd = "Changed"
        var observerNotified = false
        this::value.observeSet(this) {
            assert(it.removes.isEmpty())
            assert(it.adds.size == 1)
            assertEquals(it.adds.first(), toAdd)
            observerNotified = true
        }
        value += toAdd
        assert(observerNotified)
    }

    @Test
    fun testRemove() {
        val toRemove = "Included"
        var observerNotified = false
        this::value.observeSet(this) {
            assert(it.adds.isEmpty())
            assert(it.removes.size == 1)
            assertEquals(it.removes.first(), toRemove)
            observerNotified = true
        }
        value -= toRemove
        assert(observerNotified)
    }

    @Test
    fun mutableIterator() {
        val toRemove = "Included"
        var observerNotified = false
        this::value.observeSet(this) {
            assert(it.adds.isEmpty())
            assert(it.removes.size == 1)
            assertEquals(it.removes.first(), toRemove)
            observerNotified = true
        }

        val iterator = value.iterator()
        for (value in iterator) {
            iterator.remove()
        }
        assert(observerNotified)
    }
}
