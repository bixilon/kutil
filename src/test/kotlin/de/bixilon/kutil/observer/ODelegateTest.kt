package de.bixilon.kutil.observer

import de.bixilon.kutil.observer.DataObserver.Companion.observed

object ODelegateTest {
    var with by observed("Test")
    var plain = 0
}
