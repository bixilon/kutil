package de.bixilon.kutil.observer

import de.bixilon.kutil.observer.ObserveUtil.delegate
import de.bixilon.kutil.reflection.ReflectionUtil.forceSet
import de.bixilon.kutil.reflection.ReflectionUtil.getFieldOrNull
import de.bixilon.kutil.reflection.ReflectionUtil.jvmField
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class ObserveUtilTest {
    val field = 0


    @Test
    fun `no delegate retrieving`() {
        val test = DelegateTest()
        val delegate = test::plain.delegate

        assertNull(delegate)
    }

    @Test
    fun `object no delegate retrieving`() {
        val delegate = ODelegateTest::plain.delegate

        assertNull(delegate)
    }

    @Test
    fun `delegate retrieving`() {
        val test = DelegateTest()
        val delegate = test::with.delegate

        assertTrue(delegate is DataObserver<*>)
    }

    @Test
    fun `object delegate retrieving`() {
        val delegate = ODelegateTest::with.delegate

        assertTrue(delegate is DataObserver<*>)
    }

    @Test
    fun `object delegate  2`() {
        val delegate = ODelegateTest::with.delegate

        assertTrue(delegate is DataObserver<*>)
    }

    @Test
    fun `inherited delegate retrieving`() {
        val test = DelegateTest()
        val delegate = test::inherited.delegate

        assertTrue(delegate is DataObserver<*>)
    }

    @Test
    fun `field force setting`() {
        val test = DelegateTest()
        test.plain = 1

        test::plain.forceSet(102)
        assertEquals(test.plain, 102)
    }

    @Test
    fun `jvm field0 retrieving`() {
        val field = this::field.jvmField
        assertEquals(field.name, "field")
    }

    @Test
    fun `jvm field1 retrieving`() {
        val field = ObserveUtilTest::field.jvmField
        assertEquals(field.name, "field")
    }

    @Test
    fun `get field or null`() {
        val field = ObserveUtilTest::class.java.getFieldOrNull("field")
        assertEquals(field?.name, "field")
    }

    @Test
    fun `get null field`() {
        val field = ObserveUtilTest::class.java.getFieldOrNull("nonexistent")
        assertNull(field)
    }

    @Test
    fun `get parent field a`() {
        val field = A::class.java.getFieldOrNull("a")
        assertEquals(field?.name, "a")
    }

    @Test
    fun `get parent field b`() {
        val field = B::class.java.getFieldOrNull("a")
        assertEquals(field?.name, "a")
    }

    @Test
    fun `get child field`() {
        val field = B::class.java.getFieldOrNull("b")
        assertEquals(field?.name, "b")
    }

    @Test
    fun `get inner class field`() {
        val field = C::class.java.getFieldOrNull("c")
        assertEquals(field?.name, "c")
    }

    private open class A {
        var a = 0
    }

    private open class B : A() {
        var b = 0
    }

    private inner class C : B() {
        var c = 0
    }
}
