package de.bixilon.kutil.observer

import de.bixilon.kutil.observer.DataObserver.Companion.observed

class DelegateTest : SuperDelegate() {
    var with by observed("Test")
    var plain = 0
}

abstract class SuperDelegate {
    var inherited by observed("Yah!")
}
