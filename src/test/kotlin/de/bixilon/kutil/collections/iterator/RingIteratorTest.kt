package de.bixilon.kutil.collections.iterator

import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.assertThrows
import kotlin.test.Test
import kotlin.test.assertEquals

class RingIteratorTest {

    @Test
    fun empty() {
        val iterator = RingIterator<Any>(emptyList())
        assertFalse(iterator.hasNext())
        assertThrows<NoSuchElementException> { iterator.next() }
    }

    @Test
    fun `single element with hasNext`() {
        val iterator = RingIterator(listOf("a"))
        assertTrue(iterator.hasNext())
        assertEquals("a", iterator.next())
        assertTrue(iterator.hasNext())
        assertEquals("a", iterator.next())
        assertTrue(iterator.hasNext())
        assertEquals("a", iterator.next())
    }

    @Test
    fun `single element without hasNext`() {
        val iterator = RingIterator(listOf("a"))
        assertEquals("a", iterator.next())
        assertEquals("a", iterator.next())
        assertEquals("a", iterator.next())
    }

    @Test
    fun `dual element without hasNext`() {
        val iterator = RingIterator(listOf("a", "b"))
        assertEquals("a", iterator.next())
        assertEquals("b", iterator.next())
        assertEquals("a", iterator.next())
        assertEquals("b", iterator.next())
    }

    @Test
    fun `dual element with hasNext`() {
        val iterator = RingIterator(listOf("a", "b"))
        assertTrue(iterator.hasNext())
        assertEquals("a", iterator.next())
        assertTrue(iterator.hasNext())
        assertEquals("b", iterator.next())
        assertTrue(iterator.hasNext())
        assertEquals("a", iterator.next())
        assertTrue(iterator.hasNext())
        assertEquals("b", iterator.next())
    }
}
