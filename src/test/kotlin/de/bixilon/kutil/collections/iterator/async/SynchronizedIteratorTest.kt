package de.bixilon.kutil.collections.iterator.async

import de.bixilon.kutil.collections.iterator.async.SynchronizedIterator.Companion.sync
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test

class SynchronizedIteratorTest {

    @Test
    fun `iterate single threaded`() {
        val iterator = listOf("a", "b", "c", "d", "e").iterator().sync()
        assertEquals(iterator.getOrNull(), "a")
        assertEquals(iterator.getOrNull(), "b")
        assertEquals(iterator.getOrNull(), "c")
        assertEquals(iterator.getOrNull(), "d")
        assertEquals(iterator.getOrNull(), "e")
        assertNull(iterator.getOrNull())
    }
}
