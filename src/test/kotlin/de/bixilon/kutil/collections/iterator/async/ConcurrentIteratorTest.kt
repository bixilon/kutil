/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.collections.iterator.async

import de.bixilon.kutil.collections.CollectionUtil.synchronizedSetOf
import de.bixilon.kutil.collections.spliterator.async.AsyncSpliteratorTest
import de.bixilon.kutil.concurrent.pool.ThreadPool
import de.bixilon.kutil.time.TimeUtil.millis
import de.bixilon.kutil.time.TimeUtil.nanos
import de.bixilon.kutil.unit.UnitFormatter.formatNanos
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import java.util.concurrent.atomic.AtomicInteger
import java.util.stream.Stream

class ConcurrentIteratorTest {


    @Test
    fun singleElement() {
        val list = listOf("first")
        val output = synchronizedSetOf<String>()
        val count = AtomicInteger()

        ConcurrentIterator(list.iterator()).iterate {
            count.incrementAndGet()
            output += it
        }

        assertEquals(count.get(), 1)
        assertEquals(output, setOf("first"))
    }

    @Test
    fun fiveElements() {
        val list = Stream.of("first", "second", "third", "fourth", "fifth").iterator()
        val output = synchronizedSetOf<String>()
        val count = AtomicInteger()

        ConcurrentIterator(list).iterate {
            count.incrementAndGet()
            output += it
        }

        assertEquals(count.get(), 5)
        assertEquals(output, setOf("first", "second", "third", "fourth", "fifth"))
    }

    @Test
    fun thousandElements() {
        val list = Stream.of(*AsyncSpliteratorTest.buildList(1000).toTypedArray()).iterator()
        val output = synchronizedSetOf<String>()
        val count = AtomicInteger()

        ConcurrentIterator(list).iterate {
            count.incrementAndGet()
            output += it
        }

        assertEquals(count.get(), 1000)
        assertEquals(output, AsyncSpliteratorTest.buildList(1000).toSet())
    }

    @Test
    fun `check really async`() {
        val list = Stream.of(*AsyncSpliteratorTest.buildList(500).toTypedArray()).iterator()
        val output = synchronizedSetOf<String>()
        val count = AtomicInteger()
        val start = nanos()

        ConcurrentIterator(list).iterate {
            count.incrementAndGet()
            output += it
        }

        val end = nanos()
        val delta = end - start

        if (delta > 300L * 10L * 1000_000L) {
            throw AssertionError("Operation was not async, took ${delta.formatNanos()}, expected < 3s")
        }

        assertEquals(count.get(), 500)
        assertEquals(output, AsyncSpliteratorTest.buildList(500).toSet())
    }

    @Test
    fun `single thread`() {
        val list = AsyncSpliteratorTest.buildList(1000)
        val output = synchronizedSetOf<String>()
        val count = AtomicInteger()

        val pool = ThreadPool(1)
        val iterator = ConcurrentIterator(list.iterator(), pool = pool)

        iterator.iterate {
            count.incrementAndGet()
            output += it
        }

        assertEquals(count.get(), 1000)
        assertEquals(output, AsyncSpliteratorTest.buildList(1000).toSet())
        pool.shutdownNow()
    }

    @Test
    fun `no threads`() {
        val list = AsyncSpliteratorTest.buildList(1000)
        val output = synchronizedSetOf<String>()
        val count = AtomicInteger()

        val pool = ThreadPool(1)
        pool += { Thread.sleep(10000) }
        val start = millis()
        val iterator = ConcurrentIterator(list.iterator(), pool)

        iterator.iterate {
            count.incrementAndGet()
            output += it
        }
        val end = millis()
        assertTrue((end - start) < 1000, "Took to long!")

        assertEquals(count.get(), 1000)
        assertEquals(output, AsyncSpliteratorTest.buildList(1000).toSet())
        pool.shutdownNow()
    }
}
