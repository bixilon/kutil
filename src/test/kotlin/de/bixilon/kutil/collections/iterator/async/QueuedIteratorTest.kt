/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.collections.iterator.async

import de.bixilon.kutil.collections.CollectionUtil.synchronizedSetOf
import de.bixilon.kutil.collections.spliterator.async.AsyncSpliteratorTest
import de.bixilon.kutil.concurrent.pool.ThreadPool
import de.bixilon.kutil.concurrent.pool.runnable.ForcePooledRunnable
import de.bixilon.kutil.time.TimeUtil.nanos
import de.bixilon.kutil.unit.UnitFormatter.formatNanos
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.util.concurrent.atomic.AtomicInteger

class QueuedIteratorTest {


    @Test
    fun queueSingleElement() {
        val list = mutableListOf("first")
        val output = synchronizedSetOf<String>()
        val count = AtomicInteger()

        QueuedIterator(list.iterator()).iterate {
            count.incrementAndGet()
            output += it
        }

        assertEquals(count.get(), 1)
        assertEquals(output, setOf("first"))
    }

    @Test
    fun queueFiveElements() {
        val list = mutableListOf("first", "second", "third", "fourth", "fifth")
        val output = synchronizedSetOf<String>()
        val count = AtomicInteger()

        QueuedIterator(list.iterator()).iterate {
            count.incrementAndGet()
            output += it
        }

        assertEquals(count.get(), 5)
        assertEquals(output, setOf("first", "second", "third", "fourth", "fifth"))
    }

    @Test
    fun queueThousandElements() {
        val list = AsyncSpliteratorTest.buildList(1000)
        val output = synchronizedSetOf<String>()
        val count = AtomicInteger()

        QueuedIterator(list.iterator()).iterate {
            count.incrementAndGet()
            output += it
        }

        assertEquals(count.get(), 1000)
        assertEquals(output, AsyncSpliteratorTest.buildList(1000).toSet())
    }

    @Test
    fun `queue check really async`() {
        val list = AsyncSpliteratorTest.buildList(500)
        val output = synchronizedSetOf<String>()
        val count = AtomicInteger()
        val start = nanos()


        QueuedIterator(list.iterator()).iterate {
            count.incrementAndGet()
            output += it
        }

        val end = nanos()
        val delta = end - start

        if (delta > 300L * 10L * 1000_000L) {
            throw AssertionError("Operation was not async, took ${delta.formatNanos()}, expected < 3s")
        }

        assertEquals(count.get(), 500)
        assertEquals(output, AsyncSpliteratorTest.buildList(500).toSet())
    }

    @Test
    fun reuse() {
        val list = AsyncSpliteratorTest.buildList(1000)
        val output = synchronizedSetOf<String>()
        val count = AtomicInteger()

        val iterator = QueuedIterator(list.iterator())

        iterator.iterate {
            count.incrementAndGet()
            output += it
        }
        iterator.reuse(list.iterator())
        iterator.iterate {
            count.incrementAndGet()
            output += it
        }

        assertEquals(count.get(), 2000)
        assertEquals(output, AsyncSpliteratorTest.buildList(1000).toSet())
    }

    @Test
    fun `single thread`() {
        val list = AsyncSpliteratorTest.buildList(1000)
        val output = synchronizedSetOf<String>()
        val count = AtomicInteger()

        val pool = ThreadPool(1)
        val iterator = QueuedIterator(list.iterator(), pool = pool, queueSize = 10)

        iterator.iterate {
            count.incrementAndGet()
            output += it
        }

        assertEquals(count.get(), 1000)
        assertEquals(output, AsyncSpliteratorTest.buildList(1000).toSet())
        pool.shutdownNow()
    }

    // @Test
    fun `no threads small list`() {
        val list = AsyncSpliteratorTest.buildList(100)
        val output = synchronizedSetOf<String>()
        val count = AtomicInteger()

        val pool = ThreadPool(1)
        Thread.sleep(10L) // wait for thread pool to be started
        pool += ForcePooledRunnable { Thread.sleep(100000000) }
        val start = nanos()
        val iterator = QueuedIterator(list.iterator(), pool, queueSize = 200)

        iterator.iterate {
            count.incrementAndGet()
            output += it
        }
        val end = nanos()
        Assertions.assertTrue((end - start) < 1000_000_000L, "Took to long!")

        assertEquals(count.get(), 100)
        assertEquals(output, AsyncSpliteratorTest.buildList(100).toSet())
        pool.shutdownNow()
    }

    // @Test
    fun `no threads big list`() {
        val list = AsyncSpliteratorTest.buildList(1000)
        val output = synchronizedSetOf<String>()
        val count = AtomicInteger()

        val pool = ThreadPool(1)
        Thread.sleep(10L) // wait for thread pool to be started
        pool += ForcePooledRunnable { Thread.sleep(100000000) }
        val start = nanos()
        val iterator = QueuedIterator(list.iterator(), pool, queueSize = 100)

        iterator.iterate {
            count.incrementAndGet()
            output += it
        }
        val end = nanos()
        Assertions.assertTrue((end - start) < 1000_000_000L, "Took to long!")

        assertEquals(count.get(), 1000)
        assertEquals(output, AsyncSpliteratorTest.buildList(1000).toSet())
        pool.shutdownNow()
    }
}
