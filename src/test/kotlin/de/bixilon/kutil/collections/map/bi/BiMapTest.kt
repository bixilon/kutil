/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.collections.map.bi

import de.bixilon.kutil.collections.CollectionUtil.biMapOf
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertDoesNotThrow
import kotlin.test.assertEquals

internal class BiMapTest {

    @Test
    fun checkInitDuplicate() {
        assertThrows(IllegalArgumentException::class.java) { biMapOf("first" to "second", "third" to "second") }
    }

    @Test
    fun checkNoInitDuplicate() {
        assertDoesNotThrow { biMapOf("first" to "second", "third" to "fourth") }
    }

    @Test
    fun checkGetByKey() {
        val map = biMapOf("first" to "second", "third" to "fourth")
        assertEquals(map["first"], "second")
        assertEquals(map["third"], "fourth")
    }

    @Test
    fun checkGetByValue() {
        val map = biMapOf("first" to "second", "third" to "fourth")
        assertEquals(map.getKey("second"), "first")
        assertEquals(map.getKey("fourth"), "third")
    }
}
