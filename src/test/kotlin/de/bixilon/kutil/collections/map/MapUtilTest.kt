package de.bixilon.kutil.collections.map

import de.bixilon.kutil.collections.map.MapUtil.replace
import kotlin.test.Test
import kotlin.test.assertEquals

class MapUtilTest {

    @Test
    fun `replace clear map`() {
        val map = mutableMapOf(0 to 0, 1 to 1)
        map.replace(emptyMap())

        assertEquals(map, emptyMap())
    }

    @Test
    fun `replace add to empty map`() {
        val map: MutableMap<Int, Int> = mutableMapOf()
        map.replace(mutableMapOf(0 to 0, 1 to 1))

        assertEquals(map, mutableMapOf(0 to 0, 1 to 1))
    }

    @Test
    fun `replace value`() {
        val map = mutableMapOf(1 to 4)
        map.replace(mutableMapOf(0 to 0, 1 to 1))

        assertEquals(map, mutableMapOf(0 to 0, 1 to 1))
    }

    @Test
    fun `remove old value`() {
        val map = mutableMapOf(3 to 10, 1 to 4)
        map.replace(mutableMapOf(0 to 0, 1 to 1))

        assertEquals(map, mutableMapOf(0 to 0, 1 to 1))
    }
}
