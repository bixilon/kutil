package de.bixilon.kutil.collections.set

import de.bixilon.kutil.collections.set.SetUtil.replace
import org.junit.jupiter.api.Assertions.assertEquals
import kotlin.test.Test

class SetUtilTest {


    @Test
    fun `replace clear set`() {
        val set = mutableSetOf(0, 1)
        set.replace(emptySet())

        assertEquals(set, emptySet<Int>())
    }

    @Test
    fun `replace add to empty set`() {
        val map: MutableSet<Int> = mutableSetOf()
        map.replace(mutableSetOf(0, 1))

        assertEquals(map, mutableSetOf(0, 1))
    }

    @Test
    fun `replace value`() {
        val map = mutableSetOf(1)
        map.replace(mutableSetOf(0, 1))

        assertEquals(map, mutableSetOf(0, 1))
    }

    @Test
    fun `remove old value`() {
        val map = mutableSetOf(3, 1)
        map.replace(mutableSetOf(0, 1))

        assertEquals(map, mutableSetOf(0, 1))
    }
}
