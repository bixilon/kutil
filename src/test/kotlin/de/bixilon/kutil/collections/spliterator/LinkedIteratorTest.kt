package de.bixilon.kutil.collections.spliterator

import de.bixilon.kutil.collections.iterator.EmptyIterator
import de.bixilon.kutil.collections.iterator.LinkedIterator
import de.bixilon.kutil.collections.iterator.SingleIterator
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class LinkedIteratorTest {

    @Test
    fun empty() {
        val iterator = LinkedIterator<Any>()
        assertFalse(iterator.hasNext())
        assertThrows<IllegalStateException> { iterator.next() }
    }

    @Test
    fun empty2() {
        val iterator = LinkedIterator(EmptyIterator)
        assertFalse(iterator.hasNext())
        assertThrows<IllegalStateException> { iterator.next() }
    }

    @Test
    fun single() {
        val iterator = LinkedIterator(SingleIterator("test"))
        assertTrue(iterator.hasNext())
        assertEquals(iterator.next(), "test")
        assertFalse(iterator.hasNext())
        assertThrows<IllegalStateException> { iterator.next() }
    }

    @Test
    fun `two items`() {
        val iterator = LinkedIterator(listOf("test", "test2").iterator())
        assertTrue(iterator.hasNext())
        assertEquals(iterator.next(), "test")
        assertTrue(iterator.hasNext())
        assertEquals(iterator.next(), "test2")
        assertFalse(iterator.hasNext())
        assertThrows<IllegalStateException> { iterator.next() }
    }

    @Test
    fun `two single iterators`() {
        val iterator = LinkedIterator(SingleIterator("test"), SingleIterator("test2"))
        assertTrue(iterator.hasNext())
        assertEquals(iterator.next(), "test")
        assertTrue(iterator.hasNext())
        assertEquals(iterator.next(), "test2")
        assertFalse(iterator.hasNext())
        assertThrows<IllegalStateException> { iterator.next() }
    }

    @Test
    fun `two multi iterators`() {
        val iterator = LinkedIterator(listOf("1", "2").iterator(), listOf("3", "4").iterator())
        assertTrue(iterator.hasNext())
        assertEquals(iterator.next(), "1")
        assertTrue(iterator.hasNext())
        assertEquals(iterator.next(), "2")
        assertTrue(iterator.hasNext())
        assertEquals(iterator.next(), "3")
        assertTrue(iterator.hasNext())
        assertEquals(iterator.next(), "4")
        assertFalse(iterator.hasNext())
        assertThrows<IllegalStateException> { iterator.next() }
    }
}
