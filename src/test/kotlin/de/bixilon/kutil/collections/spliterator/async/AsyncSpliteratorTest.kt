/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.collections.spliterator.async

class AsyncSpliteratorTest {


    /*
    @Test
    fun benchmark1() {
        val list = Stream.of(*buildList(100000).toTypedArray()).spliterator()
        val output = synchronizedSetOf<String>()
        val count = AtomicInteger()

        val a = nanos()
        QueuedIterator(list, executor = {
            count.incrementAndGet()
            output += it
            Thread.sleep(1)
        }).iterate()
        println((nanos() - a).formatNanos())
        assertEquals(100000, count.get())
    }

    @Test
    fun benchmark2() {
        val list = buildList(100000)
        val output = synchronizedSetOf<String>()
        val count = AtomicInteger()

        val a = nanos()
        list.parallel {
            count.incrementAndGet()
            output += it
            Thread.sleep(1)
        }
        println((nanos() - a).formatNanos())
        assertEquals(list.size, count.get())
    }

    @Test
    fun benchmark3() {
        val list = buildList(100000)
        val output = synchronizedSetOf<String>()
        val count = AtomicInteger()

        val a = nanos()
        list.parallelStream().forEach {
            count.incrementAndGet()
            output += it
            Thread.sleep(1)
        }
        println((nanos() - a).formatNanos())
        assertEquals(list.size, count.get())
    }

    @Test
    fun benchmark4() {
        val list = buildList(10000)
        val output = synchronizedSetOf<String>()
        val count = AtomicInteger()

        val a = nanos()
        for (entry in list) {
            count.incrementAndGet()
            output += entry
            Thread.sleep(1)
        }
        println(((nanos() - a) * 10).formatNanos())
        assertEquals(list.size, count.get())
    }
     */

    companion object {

        fun buildList(count: Int): List<String> {
            val list = ArrayList<String>(count)

            for (i in 0 until count) {
                list += i.toString()
            }

            return list
        }
    }
}
