package de.bixilon.kutil.collections.spliterator

import de.bixilon.kutil.collections.iterator.EmptyIterator
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class EmptyIteratorTest {

    @Test
    fun test() {
        val iterator = EmptyIterator
        assertFalse(iterator.hasNext())
        assertThrows<IllegalStateException> { iterator.next() }
    }
}
