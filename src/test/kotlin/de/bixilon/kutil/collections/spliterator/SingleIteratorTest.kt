package de.bixilon.kutil.collections.spliterator

import de.bixilon.kutil.collections.iterator.SingleIterator
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class SingleIteratorTest {

    @Test
    fun test() {
        val iterator = SingleIterator("abc")
        assertTrue(iterator.hasNext())
        assertEquals(iterator.next(), "abc")
        assertFalse(iterator.hasNext())
        assertThrows<IllegalStateException> { iterator.next() }
    }
}
