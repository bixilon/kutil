/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.http

import de.bixilon.kutil.http.QueryUtil.fromQuery
import de.bixilon.kutil.http.QueryUtil.toQuery
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class QueryUtilTest {

    @Test
    fun toQuery() {
        val expected = "test=value&another=this"
        val map = mapOf(
            "test" to "value",
            "another" to "this",
        )
        val result = map.toQuery()
        assertEquals(expected, result)
    }

    @Test
    fun specialCharToQuery() {
        val expected = "te%22st=value&another=th%26%26is"
        val map = mapOf(
            "te\"st" to "value",
            "another" to "th&&is",
        )
        val result = map.toQuery()
        assertEquals(expected, result)
    }

    @Test
    fun fromQuery() {
        val string = "test=value&another=this"
        val expected = mapOf(
            "test" to "value",
            "another" to "this",
        )
        val result = string.fromQuery()
        assertEquals(expected, result)
    }

    @Test
    fun specialCharFromQuery() {
        val string = "te%22st=value&another=th%26%26is"
        val expected = mapOf(
            "te\"st" to "value",
            "another" to "th&&is",
        )
        val result = string.fromQuery()
        assertEquals(expected, result)
    }
}
