/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.latch

import de.bixilon.kutil.concurrent.pool.DefaultThreadPool
import de.bixilon.kutil.concurrent.pool.runnable.ForcePooledRunnable
import de.bixilon.kutil.time.TimeUtil.millis
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import kotlin.test.assertEquals


internal class CallbackLatchTest {

    @Test
    fun testInitialCount() {
        val latch = CallbackLatch(5)
        assertEquals(5, latch.count)
    }

    @Test
    fun countDown() {
        val latch = CallbackLatch(5)
        latch.countDown()
        assertEquals(4, latch.count)
    }

    @Test
    fun countDownMinus() {
        val latch = CallbackLatch(5)
        latch.minus(2)
        assertEquals(3, latch.count)
    }

    @Test
    fun countUp() {
        val latch = CallbackLatch(5)
        latch.countUp()
        assertEquals(6, latch.count)
    }

    @Test
    fun countUpPlus() {
        val latch = CallbackLatch(5)
        latch.plus(2)
        assertEquals(7, latch.count)
    }

    @Test
    fun testBelowMinus() {
        val latch = CallbackLatch(1)
        assertThrows<IllegalStateException> { latch.minus(2) }
    }

    @Test
    fun testParentInitialCountAdd() {
        val parent = CallbackLatch(10)
        val child = CallbackLatch(7, parent)
        assertEquals(17, parent.count)
    }

    @Test
    fun testParentCountDown() {
        val parent = CallbackLatch(10)
        val child = CallbackLatch(7, parent)
        child.countDown()
        assertEquals(16, parent.count)
    }

    @Test
    fun testParentCountUp() {
        val parent = CallbackLatch(10)
        val child = CallbackLatch(7, parent)
        child.countUp()
        assertEquals(18, parent.count)
    }

    @Test
    fun testParentCountUp2() {
        val parent = CallbackLatch(10)
        val child1 = CallbackLatch(7, parent)
        val child2 = CallbackLatch(4, parent)
        child1.countUp()
        assertEquals(22, parent.count)
        child2.countUp()
        assertEquals(23, parent.count)
    }

    @Test
    fun testParentCountDown2() {
        val parent = CallbackLatch(10)
        val child1 = CallbackLatch(7, parent)
        val child2 = CallbackLatch(4, parent)
        child1.countDown()
        assertEquals(20, parent.count)
        child2.countDown()
        assertEquals(19, parent.count)
    }

    @Test
    fun testEmptyAwait() {
        val latch = CallbackLatch(0)
        latch.await(10L)
    }

    @Test
    fun testAwait() {
        val latch = CallbackLatch(2)
        val time = millis()
        DefaultThreadPool += ForcePooledRunnable { Thread.sleep(100L); latch.countDown() }
        DefaultThreadPool += ForcePooledRunnable { Thread.sleep(210L); latch.countDown() }
        latch.await(1000L)
        val delta = millis() - time
        assert(delta > 200L) { "Delta too small: $delta" }
    }

    @Test
    fun testTimeout() {
        val latch = CallbackLatch(2)
        val start = millis()
        assertThrows<InterruptedException> { latch.await(100L) }
        val delta = millis() - start
        assert(delta in 100L..200L) { "Delta out of range: $delta" } // small padding
    }

    @Test
    fun testChange() {
        val latch = CallbackLatch(2)
        val start = millis()
        DefaultThreadPool += ForcePooledRunnable { Thread.sleep(100L); latch.countDown() }
        latch.waitForChange(1000000L)
        assertEquals(1, latch.count)
        val delta = millis() - start
        assert(delta in 100L..200L) { "Delta out of range: $delta" } // small padding
    }

    @Test
    fun testAwaitWithChange() {
        val latch = CallbackLatch(2)
        val start = millis()
        DefaultThreadPool += ForcePooledRunnable { Thread.sleep(100L); latch.countDown() }
        DefaultThreadPool += ForcePooledRunnable { Thread.sleep(200L); latch.countDown() }
        latch.awaitWithChange(1000L)
        assertEquals(0, latch.count)
        val delta = millis() - start
        assert(delta in 200L..500L) { "Delta out of range: $delta" } // small padding
    }

    @Test
    fun awaitParent() {
        val parent = CallbackLatch(0)
        val child = CallbackLatch(2, parent = parent)
        assertEquals(2, parent.count)
        DefaultThreadPool += ForcePooledRunnable { Thread.sleep(100L); child.countDown() }
        DefaultThreadPool += ForcePooledRunnable { Thread.sleep(200L); child.countDown() }
        parent.awaitWithChange(1000L)
        assertEquals(0, parent.count)
    }

    @Test
    fun testCounting() {
        val threads = 1000
        val parent = CallbackLatch(0)
        val child = CallbackLatch(threads, parent = parent)
        for (thread in 0 until threads) {
            DefaultThreadPool += ForcePooledRunnable { child.countDown() }
        }
        parent.awaitWithChange(10000L)
        assertEquals(0, parent.count)
    }


    @Test
    fun testConstructor() {
        val parent = CallbackLatch(0)
        val child = CallbackLatch(1, parent)
        assertEquals(parent.count, 1)
        child.countDown()
        assertEquals(parent.count, 0)
    }

    @Test
    fun testConstructor2() {
        val parent = CallbackLatch(2)
        val child = CallbackLatch(3, parent)
        assertEquals(child.count, 3)
        assertEquals(parent.count, 5)
        child.countDown()
        assertEquals(child.count, 2)
        assertEquals(parent.count, 4)
    }

    @Test
    fun complex() {
        val a = CallbackLatch(1)
        val b = CallbackLatch(1, a)
        val c = ParentLatch(1, b)
        val d = ParentLatch(1, c)

        assertEquals(a.count, 4)
        assertEquals(b.count, 3)
        assertEquals(c.count, 2)
        assertEquals(d.count, 1)

        d.countDown()
        assertEquals(a.count, 3)
        assertEquals(b.count, 2)
        assertEquals(c.count, 1)
        assertEquals(d.count, 0)

        c.countDown()
        assertEquals(a.count, 2)
        assertEquals(b.count, 1)
        assertEquals(c.count, 0)
        assertEquals(d.count, 0)

        b.countDown()
        assertEquals(a.count, 1)
        assertEquals(b.count, 0)
        assertEquals(c.count, 0)
        assertEquals(d.count, 0)

        a.countDown()
        assertEquals(a.count, 0)
        assertEquals(b.count, 0)
        assertEquals(c.count, 0)
        assertEquals(d.count, 0)
    }
}
