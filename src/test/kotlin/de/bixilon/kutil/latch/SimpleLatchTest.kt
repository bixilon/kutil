/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.latch

import de.bixilon.kutil.concurrent.pool.DefaultThreadPool
import de.bixilon.kutil.concurrent.pool.runnable.ForcePooledRunnable
import de.bixilon.kutil.time.TimeUtil.millis
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import kotlin.test.assertEquals


internal class SimpleLatchTest {

    @Test
    fun testInitialCount() {
        val latch = SimpleLatch(5)
        assertEquals(5, latch.count)
    }

    @Test
    fun countDown() {
        val latch = SimpleLatch(5)
        latch.countDown()
        assertEquals(4, latch.count)
    }

    @Test
    fun countDownMinus() {
        val latch = SimpleLatch(5)
        latch.minus(2)
        assertEquals(3, latch.count)
    }

    @Test
    fun countUp() {
        val latch = SimpleLatch(5)
        latch.countUp()
        assertEquals(6, latch.count)
    }

    @Test
    fun countUpPlus() {
        val latch = SimpleLatch(5)
        latch.plus(2)
        assertEquals(7, latch.count)
    }

    @Test
    fun testBelowMinus() {
        val latch = SimpleLatch(1)
        assertThrows<IllegalStateException> { latch.minus(2) }
    }

    @Test
    fun testAwait() {
        val latch = SimpleLatch(2)
        val time = millis()
        DefaultThreadPool += ForcePooledRunnable { Thread.sleep(100L); latch.countDown() }
        DefaultThreadPool += ForcePooledRunnable { Thread.sleep(210L); latch.countDown() }
        latch.await(1000L)
        val delta = millis() - time
        assert(delta > 200L) { "Delta too small: $delta" }
    }

    @Test
    fun testTimeout() {
        val latch = SimpleLatch(2)
        val start = millis()
        assertThrows<InterruptedException> { latch.await(100L) }
        val delta = millis() - start
        assert(delta in 100L..200L) { "Delta out of range: $delta" } // small padding
    }

    @Test
    fun testChange() {
        val latch = SimpleLatch(2)
        val start = millis()
        DefaultThreadPool += ForcePooledRunnable { Thread.sleep(100L); latch.countDown() }
        latch.waitForChange(1000000L)
        assertEquals(1, latch.count)
        val delta = millis() - start
        assert(delta in 100L..200L) { "Delta out of range: $delta" } // small padding
    }

    @Test
    fun testAwaitWithChange() {
        val latch = SimpleLatch(2)
        val start = millis()
        DefaultThreadPool += ForcePooledRunnable { Thread.sleep(100L); latch.countDown() }
        DefaultThreadPool += ForcePooledRunnable { Thread.sleep(200L); latch.countDown() }
        latch.awaitWithChange(1000L)
        assertEquals(0, latch.count)
        val delta = millis() - start
        assert(delta in 200L..500L) { "Delta out of range: $delta" } // small padding
    }

    @Test
    fun testAwaitOrChangeEmpty() {
        val latch = SimpleLatch(2)
        latch.dec(); latch.dec()
        latch.awaitOrChange()
    }
}
