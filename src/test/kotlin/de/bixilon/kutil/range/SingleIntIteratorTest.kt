package de.bixilon.kutil.range

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class SingleIntIteratorTest {

    @Test
    fun `iterate once`() {
        val iterator = SingleIntIterator(123)
        assertTrue(iterator.hasNext())
        assertEquals(iterator.next(), 123)
    }

    @Test
    fun `iterate twice`() {
        val iterator = SingleIntIterator(123)
        assertEquals(iterator.next(), 123)
        assertFalse(iterator.hasNext())
        assertThrows<NoSuchElementException> { iterator.next() }
    }
}
