/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.unit

import de.bixilon.kutil.unit.UnitFormatter.formatBytes
import de.bixilon.kutil.unit.UnitFormatter.formatMillis
import de.bixilon.kutil.unit.UnitFormatter.formatNanos
import de.bixilon.kutil.unit.UnitFormatter.formatSeconds
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals


internal class UnitFormatterTest {

    @Test
    fun `test Bytes 1`() {
        assertEquals("0B", 0L.formatBytes())
    }

    @Test
    fun `test Bytes 2`() {
        assertEquals("10B", 10L.formatBytes())
    }

    @Test
    fun `test Bytes 3`() {
        assertEquals("1000B", 1000L.formatBytes())
    }

    @Test
    fun `test Bytes 4`() {
        assertEquals("1023B", 1023L.formatBytes())
    }

    @Test
    fun `test Bytes 5`() {
        assertEquals("1.0 KiB", 1024L.formatBytes())
    }

    @Test
    fun `test Bytes 6`() {
        assertEquals("1.0 KiB", 1025L.formatBytes())
    }

    @Test
    fun `test Bytes 7`() {
        assertEquals("1.1 KiB", 1200L.formatBytes())
    }

    @Test
    fun `test Bytes 8`() {
        assertEquals("1.6 KiB", 1700L.formatBytes())
    }

    @Test
    fun `test Bytes 9`() {
        assertEquals("1.9 KiB", 2000L.formatBytes())
    }

    @Test
    fun `test Bytes 10`() {
        assertEquals("1.1 MiB", 1200000L.formatBytes())
    }

    @Test
    fun `test Bytes 11`() {
        assertEquals("11 MiB", 12000000L.formatBytes())
    }

    @Test
    fun `test Bytes 12`() {
        assertEquals("888 PiB", 999999999999999999L.formatBytes())
    }


    @Test
    fun `test Time 1`() {
        assertEquals("1.0 ns", 1L.formatNanos())
    }

    @Test
    fun `test Time 2`() {
        assertEquals("10 ns", 10L.formatNanos())
    }

    @Test
    fun `test Time 3`() {
        assertEquals("999 ns", 999L.formatNanos())
    }

    @Test
    fun `test Time 4`() {
        assertEquals("1.0 µs", 1000L.formatNanos())
    }

    @Test
    fun `test Time 5`() {
        assertEquals("1.1 µs", 1100L.formatNanos())
    }

    @Test
    fun `test Time 6`() {
        assertEquals("1.2 µs", 1290L.formatNanos())
    }

    @Test
    fun `test Time 7`() {
        assertEquals("12 µs", 12900L.formatNanos())
    }

    @Test
    fun `test Time 8`() {
        assertEquals("1.0 ms", 1.formatMillis())
    }

    @Test
    fun `test Time 9`() {
        assertEquals("1.0 s", 1.formatSeconds())
    }

    @Test
    fun `test Time 10`() {
        assertEquals("9.0 s", 9.formatSeconds())
    }

    @Test
    fun `test Time 11`() {
        assertEquals("7.4 s", 7400.formatMillis())
    }

    @Test
    fun `test Time 12`() {
        assertEquals("19 s", 19.formatSeconds())
    }

    @Test
    fun `test Time 13`() {
        assertEquals("1 m", 60.formatSeconds())
    }

    @Test
    fun `test Time 14`() {
        assertEquals("1 m", 65.formatSeconds())
    }

    @Test
    fun `test Time 15`() {
        assertEquals("2 m", 120.formatSeconds())
    }

    @Test
    fun `test Time 16`() {
        assertEquals("20 m", (60 * 20).formatSeconds())
    }

    @Test
    fun `test Time 17`() {
        assertEquals("1 h", (60 * 60).formatSeconds())
    }

    @Test
    fun `test Time 18`() {
        assertEquals("1 h", (60 * 65).formatSeconds())
    }

    @Test
    fun `test Time 19`() {
        assertEquals("20 h", (60 * 60 * 20).formatSeconds())
    }

    @Test
    fun `test Time 20`() {
        assertEquals("1 d", (60 * 60 * 24).formatSeconds())
    }

    @Test
    fun `test Time 21`() {
        assertEquals("1 d", (60 * 60 * 28).formatSeconds())
    }

    @Test
    fun `test Time 22`() {
        assertEquals("2 d", (60 * 60 * 24 * 2).formatSeconds())
    }

    @Test
    fun `test Time 24`() {
        assertEquals("1 w", (60 * 60 * 24 * 7).formatSeconds())
    }

    @Test
    fun `test Time 25`() {
        assertEquals("2 w", (60 * 60 * 24 * 7 * 2).formatSeconds())
    }

    @Test
    fun `test Time 26`() {
        assertEquals("1 M", (60 * 60 * 24 * 30).formatSeconds())
    }

    @Test
    fun `test Time 27`() {
        assertEquals("5 M", (60 * 60 * 24 * 30 * 5).formatSeconds())
    }

    @Test
    fun `test Time 28`() {
        assertEquals("1 y", (60 * 60 * 24 * 365).formatSeconds())
    }

    @Test
    fun `test Time 29`() {
        assertEquals("2 y", (60 * 60 * 24 * 365 * 2).formatSeconds())
    }

    @Test
    fun `test Time 30`() {
        assertEquals("100 y", (60 * 60 * 24 * 365 * 100L).formatSeconds())
    }

    @Test
    fun `test Time 31`() {
        assertEquals("100 y", (60 * 60 * 24 * 365 * 100L).formatSeconds())
    }

    @Test
    fun `test Time 32`() {
        assertEquals("-1.0 ms", (-1).formatMillis())
    }
}
