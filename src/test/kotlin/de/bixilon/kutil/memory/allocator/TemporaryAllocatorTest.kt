package de.bixilon.kutil.memory.allocator

import kotlin.test.Test
import kotlin.test.assertNotSame
import kotlin.test.assertSame
import kotlin.test.assertTrue

class TemporaryAllocatorTest {

    private fun create() = ByteAllocator()

    @Test
    fun `allocate correct size`() {
        val allocator = create()
        val a = allocator.allocate(1)
        assertTrue(a.size >= 1)
    }

    @Test
    fun `allocate 2`() {
        val allocator = create()
        val a = allocator.allocate(1)
        val b = allocator.allocate(1)
        assertNotSame(a, b)
    }

    @Test
    fun `allocate twice, get same`() {
        val allocator = create()
        val a = allocator.allocate(1)
        allocator.free(a)
        val b = allocator.allocate(1)
        assertSame(a, b)
    }

    @Test
    fun `allocate twice, get not same, different size`() {
        val allocator = create()
        val a = allocator.allocate(1)
        allocator.free(a)
        val b = allocator.allocate(1)
        assertSame(a, b)
    }
}
