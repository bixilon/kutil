/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.reflection.wrapper.unsafe

object UnsafeStaticFieldTestObject {
    val booleanField = true
    val byteField = 1.toByte()
    val shortField = 1.toShort()
    val charField = 1.toChar()
    val intField = 1.toInt()
    val longField = 1.toLong()
    val floatField = 1.toFloat()
    val doubleField = 1.toDouble()
    val stringField = "abc"
}
