package de.bixilon.kutil.reflection.wrapper.unsafe

import de.bixilon.kutil.reflection.wrapper.ObjectFieldTest
import java.lang.reflect.Field

class UnsafeObjectFieldTest : ObjectFieldTest() {

    override fun create(field: Field) = UnsafeObjectField(field)
}
