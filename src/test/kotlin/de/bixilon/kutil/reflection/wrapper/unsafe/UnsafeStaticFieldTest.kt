package de.bixilon.kutil.reflection.wrapper.unsafe

import de.bixilon.kutil.reflection.wrapper.StaticFieldTest
import java.lang.reflect.Field

class UnsafeStaticFieldTest : StaticFieldTest() {
    override val clazz = UnsafeStaticFieldTestObject::class.java

    override fun create(field: Field) = UnsafeStaticField(field)
}
