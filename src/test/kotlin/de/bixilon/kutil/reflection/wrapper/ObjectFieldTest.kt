/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.reflection.wrapper

import de.bixilon.kutil.reflection.ReflectionUtil.getUnsafeField
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.lang.reflect.Field
import kotlin.test.assertEquals

abstract class ObjectFieldTest {

    protected abstract fun create(field: Field): ObjectField

    private fun instance() = ObjectFieldTestObject()


    @Test
    fun `put boolean`() {
        val i = instance()
        val j = i::class.java.getUnsafeField("booleanField")
        val field = create(j)
        field.set(i, false)
        assertEquals(field.getBoolean(i), false)
        assertEquals(j.getBoolean(i), false)
        field.set(i, true)
        assertEquals(field.getBoolean(i), true)
        assertEquals(j.getBoolean(i), true)
        assertThrows<IllegalArgumentException> { field.set(i, 0) }
        assertThrows<IllegalArgumentException> { field.getInt(i) }
    }

    @Test
    fun `put byte`() {
        val i = instance()
        val j = i::class.java.getUnsafeField("byteField")
        val field = create(j)
        field.set(i, 1.toByte())
        assertEquals(field.getByte(i), 1.toByte())
        assertEquals(j.getByte(i), 1.toByte())
        field.set(i, 2.toByte())
        assertEquals(field.getByte(i), 2.toByte())
        assertEquals(j.getByte(i), 2.toByte())
        assertThrows<IllegalArgumentException> { field.set(i, false) }
        assertThrows<IllegalArgumentException> { field.getBoolean(i) }
    }

    @Test
    fun `put short`() {
        val i = instance()
        val j = i::class.java.getUnsafeField("shortField")
        val field = create(j)
        field.set(i, 1.toShort())
        assertEquals(field.getShort(i), 1.toShort())
        assertEquals(j.getShort(i), 1.toShort())
        field.set(i, 2.toShort())
        assertEquals(field.getShort(i), 2.toShort())
        assertEquals(j.getShort(i), 2.toShort())
        assertThrows<IllegalArgumentException> { field.set(i, false) }
        assertThrows<IllegalArgumentException> { field.getBoolean(i) }
    }

    @Test
    fun `put char`() {
        val i = instance()
        val j = i::class.java.getUnsafeField("charField")
        val field = create(j)
        field.set(i, 1.toChar())
        assertEquals(field.getChar(i), 1.toChar())
        assertEquals(j.getChar(i), 1.toChar())
        field.set(i, 2.toChar())
        assertEquals(field.getChar(i), 2.toChar())
        assertEquals(j.getChar(i), 2.toChar())
        assertThrows<IllegalArgumentException> { field.set(i, false) }
        assertThrows<IllegalArgumentException> { field.getBoolean(i) }
    }

    @Test
    fun `put int`() {
        val i = instance()
        val j = i::class.java.getUnsafeField("intField")
        val field = create(j)
        field.set(i, 1.toInt())
        assertEquals(field.getInt(i), 1.toInt())
        assertEquals(j.getInt(i), 1.toInt())
        field.set(i, 2.toInt())
        assertEquals(field.getInt(i), 2.toInt())
        assertEquals(j.getInt(i), 2.toInt())
        assertThrows<IllegalArgumentException> { field.set(i, false) }
        assertThrows<IllegalArgumentException> { field.getBoolean(i) }
    }

    @Test
    fun `put long`() {
        val i = instance()
        val j = i::class.java.getUnsafeField("longField")
        val field = create(j)
        field.set(i, 1.toLong())
        assertEquals(field.getLong(i), 1.toLong())
        assertEquals(j.getLong(i), 1.toLong())
        field.set(i, 2.toLong())
        assertEquals(field.getLong(i), 2.toLong())
        assertEquals(j.getLong(i), 2.toLong())
        assertThrows<IllegalArgumentException> { field.set(i, false) }
        assertThrows<IllegalArgumentException> { field.getBoolean(i) }
    }

    @Test
    fun `put float`() {
        val i = instance()
        val j = i::class.java.getUnsafeField("floatField")
        val field = create(j)
        field.set(i, 1.toFloat())
        assertEquals(field.getFloat(i), 1.toFloat())
        assertEquals(j.getFloat(i), 1.toFloat())
        field.set(i, 2.toFloat())
        assertEquals(field.getFloat(i), 2.toFloat())
        assertEquals(j.getFloat(i), 2.toFloat())
        assertThrows<IllegalArgumentException> { field.set(i, false) }
        assertThrows<IllegalArgumentException> { field.getBoolean(i) }
    }

    @Test
    fun `put double`() {
        val i = instance()
        val j = i::class.java.getUnsafeField("doubleField")
        val field = create(j)
        field.set(i, 1.toDouble())
        assertEquals(field.getDouble(i), 1.toDouble())
        assertEquals(j.getDouble(i), 1.toDouble())
        field.set(i, 2.toDouble())
        assertEquals(field.getDouble(i), 2.toDouble())
        assertEquals(j.getDouble(i), 2.toDouble())
        assertThrows<IllegalArgumentException> { field.set(i, false) }
        assertThrows<IllegalArgumentException> { field.getBoolean(i) }
    }

    @Test
    fun `put string`() {
        val i = instance()
        val j = i::class.java.getUnsafeField("stringField")
        val field = create(j)
        field.set(i, 1.toString())
        assertEquals(field.get(i), 1.toString())
        assertEquals(j.get(i), 1.toString())
        field.set(i, 2.toString())
        assertEquals(field.get(i), 2.toString())
        assertEquals(j.get(i), 2.toString())
        assertThrows<IllegalArgumentException> { field.set(i, false) }
        assertThrows<IllegalArgumentException> { field.getBoolean(i) }
    }
}
