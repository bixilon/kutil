package de.bixilon.kutil.reflection.wrapper.wrapped

import de.bixilon.kutil.reflection.wrapper.ObjectFieldTest
import java.lang.reflect.Field

class WrappedObjectFieldTest : ObjectFieldTest() {

    override fun create(field: Field) = WrappedObjectField(field)
}
