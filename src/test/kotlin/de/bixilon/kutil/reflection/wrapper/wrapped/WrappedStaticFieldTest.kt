package de.bixilon.kutil.reflection.wrapper.wrapped

import de.bixilon.kutil.reflection.wrapper.StaticFieldTest
import java.lang.reflect.Field

class WrappedStaticFieldTest : StaticFieldTest() {
    override val clazz = WrappedStaticFieldTestObject::class.java

    override fun create(field: Field) = WrappedStaticField(field)
}
