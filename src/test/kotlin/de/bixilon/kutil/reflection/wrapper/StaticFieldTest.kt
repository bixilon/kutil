/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.reflection.wrapper

import de.bixilon.kutil.reflection.ReflectionUtil.getUnsafeField
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.lang.reflect.Field
import kotlin.test.assertEquals

abstract class StaticFieldTest {
    protected abstract val clazz: Class<*>

    protected abstract fun create(field: Field): StaticField

    @Test
    fun `put boolean`() {
        val j = clazz.getUnsafeField("booleanField")
        val field = create(j)
        field.set(false)
        assertEquals(field.getBoolean(), false)
        assertEquals(j.getBoolean(null), false)
        field.set(true)
        assertEquals(field.getBoolean(), true)
        assertEquals(j.getBoolean(null), true)
        assertThrows<IllegalArgumentException> { field.set(0) }
        assertThrows<IllegalArgumentException> { field.getInt() }
    }

    @Test
    fun `put byte`() {
        val j = clazz.getUnsafeField("byteField")
        val field = create(j)
        field.set(1.toByte())
        assertEquals(field.getByte(), 1.toByte())
        assertEquals(j.getByte(null), 1.toByte())
        field.set(2.toByte())
        assertEquals(field.getByte(), 2.toByte())
        assertEquals(j.getByte(null), 2.toByte())
        assertThrows<IllegalArgumentException> { field.set(false) }
        assertThrows<IllegalArgumentException> { field.getBoolean() }
    }

    @Test
    fun `put short`() {
        val j = clazz.getUnsafeField("shortField")
        val field = create(j)
        field.set(1.toShort())
        assertEquals(field.getShort(), 1.toShort())
        assertEquals(j.getShort(null), 1.toShort())
        field.set(2.toShort())
        assertEquals(field.getShort(), 2.toShort())
        assertEquals(j.getShort(null), 2.toShort())
        assertThrows<IllegalArgumentException> { field.set(false) }
        assertThrows<IllegalArgumentException> { field.getBoolean() }
    }

    @Test
    fun `put char`() {
        val j = clazz.getUnsafeField("charField")
        val field = create(j)
        field.set(1.toChar())
        assertEquals(field.getChar(), 1.toChar())
        assertEquals(j.getChar(null), 1.toChar())
        field.set(2.toChar())
        assertEquals(field.getChar(), 2.toChar())
        assertEquals(j.getChar(null), 2.toChar())
        assertThrows<IllegalArgumentException> { field.set(false) }
        assertThrows<IllegalArgumentException> { field.getBoolean() }
    }

    @Test
    fun `put int`() {
        val j = clazz.getUnsafeField("intField")
        val field = create(j)
        field.set(1.toInt())
        assertEquals(field.getInt(), 1.toInt())
        assertEquals(j.getInt(null), 1.toInt())
        field.set(2.toInt())
        assertEquals(field.getInt(), 2.toInt())
        assertEquals(j.getInt(null), 2.toInt())
        assertThrows<IllegalArgumentException> { field.set(false) }
        assertThrows<IllegalArgumentException> { field.getBoolean() }
    }

    @Test
    fun `put long`() {
        val j = clazz.getUnsafeField("longField")
        val field = create(j)
        field.set(1.toLong())
        assertEquals(field.getLong(), 1.toLong())
        assertEquals(j.getLong(null), 1.toLong())
        field.set(2.toLong())
        assertEquals(field.getLong(), 2.toLong())
        assertEquals(j.getLong(null), 2.toLong())
        assertThrows<IllegalArgumentException> { field.set(false) }
        assertThrows<IllegalArgumentException> { field.getBoolean() }
    }

    @Test
    fun `put float`() {
        val j = clazz.getUnsafeField("floatField")
        val field = create(j)
        field.set(1.toFloat())
        assertEquals(field.getFloat(), 1.toFloat())
        assertEquals(j.getFloat(null), 1.toFloat())
        field.set(2.toFloat())
        assertEquals(field.getFloat(), 2.toFloat())
        assertEquals(j.getFloat(null), 2.toFloat())
        assertThrows<IllegalArgumentException> { field.set(false) }
        assertThrows<IllegalArgumentException> { field.getBoolean() }
    }

    @Test
    fun `put double`() {
        val j = clazz.getUnsafeField("doubleField")
        val field = create(j)
        field.set(1.toDouble())
        assertEquals(field.getDouble(), 1.toDouble())
        assertEquals(j.getDouble(null), 1.toDouble())
        field.set(2.toDouble())
        assertEquals(field.getDouble(), 2.toDouble())
        assertEquals(j.getDouble(null), 2.toDouble())
        assertThrows<IllegalArgumentException> { field.set(false) }
        assertThrows<IllegalArgumentException> { field.getBoolean() }
    }

    @Test
    fun `put string`() {
        val j = clazz.getUnsafeField("stringField")
        val field = create(j)
        field.set(1.toString())
        assertEquals(field.get(), 1.toString())
        assertEquals(j.get(null), 1.toString())
        field.set(2.toString())
        assertEquals(field.get(), 2.toString())
        assertEquals(j.get(null), 2.toString())
        assertThrows<IllegalArgumentException> { field.set(false) }
        assertThrows<IllegalArgumentException> { field.getBoolean() }
    }
}
