/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.reflection.wrapper.wrapped

object WrappedStaticFieldTestObject { // var, because java 18 does not support changing final primitive fields
    var booleanField = true
    var byteField = 1.toByte()
    var shortField = 1.toShort()
    var charField = 1.toChar()
    var intField = 1.toInt()
    var longField = 1.toLong()
    var floatField = 1.toFloat()
    var doubleField = 1.toDouble()
    var stringField = "abc"
}
