package de.bixilon.kutil.reflection

import de.bixilon.kutil.reflection.ReflectionUtil.forceSet
import de.bixilon.kutil.reflection.ReflectionUtil.set
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class ReflectionUtilTest {


    @Test
    fun `static final boolean`() {
        ReflectionTestObject::finalB.forceSet(false)
        assertEquals(ReflectionTestObject.finalB, false)
    }

    // @Test
    fun `const boolean`() {
        ReflectionTestObject::constFinal.forceSet(false)
        assertEquals(ReflectionTestObject.constFinal, true) // const variables are actually inlined at compile time, so this check if rather useless
    }

    fun `const boolean reflection`() {
        ReflectionTestObject::constFinal.forceSet(false)
        assertEquals(ReflectionTestObject::constFinal.get(), false)
    }

    @Test
    fun `static final string`() {
        ReflectionTestObject::finalS.forceSet("test")
        assertEquals(ReflectionTestObject::finalS.get(), "test")
    }

    private data class ABC(val a: String = "test")

    @Test
    fun `easy setting`() {
        val abc = ABC()
        ABC::class[abc, "a"] = "abc"
        assertEquals(abc.a, "abc")
    }
}
