/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.array

import de.bixilon.kutil.array.ArrayUtil.isEmptyOrOnlyNull
import de.bixilon.kutil.array.ArrayUtil.trim
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import kotlin.test.assertContentEquals
import kotlin.test.assertEquals
import kotlin.test.assertTrue


internal class ArrayUtilTest {

    @Test
    fun simpleArrayModify() {
        assertEquals(4, ArrayUtil.modifyArrayIndex(4, 5))
    }

    @Test
    fun simpleArrayModify2() {
        assertEquals(0, ArrayUtil.modifyArrayIndex(5, 5))
    }

    @Test
    fun simpleArrayModify3() {
        assertEquals(0, ArrayUtil.modifyArrayIndex(15, 5))
    }

    @Test
    fun negativeArrayModify() {
        assertEquals(4, ArrayUtil.modifyArrayIndex(-1, 5))
    }

    @Test
    fun negativeArrayModify2() {
        assertEquals(4, ArrayUtil.modifyArrayIndex(-6, 5))
    }

    @Test
    fun testInvalidSizeArrayModify() {
        assertThrows<IllegalArgumentException> { ArrayUtil.modifyArrayIndex(0, 0) }
    }

    @Test
    fun `empty is empty`() {
        assertTrue(arrayOf<String?>(null, null, null).isEmptyOrOnlyNull())
    }

    @Test
    fun `not empty is empty`() {
        assertFalse(arrayOf(null, "null", null).isEmptyOrOnlyNull())
    }

    @Test
    fun `trim array`() {
        assertContentEquals(arrayOf(null, "abc", null).trim(), arrayOf("abc"))
    }
}
