package de.bixilon.kutil.array

import de.bixilon.kutil.array.ByteArrayUtil.toHex
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class ByteArrayUtilTest {

    @Test
    fun toHex1() {
        assertEquals("abc0", byteArrayOf(0xAB.toByte(), 0xC0.toByte()).toHex())
    }
}
