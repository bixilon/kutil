/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.uuid

import de.bixilon.kutil.uuid.UUIDUtil.toUUID
import de.bixilon.kutil.uuid.UUIDUtil.trim
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.util.*

internal class UUIDUtilTest {

    @Test
    fun normalUUID() {
        val expected = UUID.fromString("ee243d57-df17-4b3f-bd7f-d9cdd494c8c6")
        val uuid = "ee243d57-df17-4b3f-bd7f-d9cdd494c8c6".toUUID()
        assertEquals(expected, uuid)
    }

    @Test
    fun trimmedUUID() {
        val expected = UUID.fromString("24a7ad57-a6dc-42ba-8595-a3a9b8c3a56a")
        val uuid = "24a7ad57a6dc42ba8595a3a9b8c3a56a".toUUID()
        assertEquals(expected, uuid)
    }

    @Test
    fun trim() {
        val expected = "4c8c308a3c82496baac4c4900ef597d4"
        val uuid = UUID.fromString("4c8c308a-3c82-496b-aac4-c4900ef597d4")
        assertEquals(expected, uuid.trim())
    }

    @Test

    fun `invalid normal uuid`() {
        assertThrows<IllegalArgumentException> { "X4a7ad57a6dc42ba8595a3a9b8c3a56a".toUUID() }
    }

    @Test
    fun `invalid trimmed uuid 1`() {
        assertThrows<IllegalArgumentException> { "Xe243d57-df17-4b3f-bd7f-d9cdd494c8c6".toUUID() }
    }

    @Test
    fun `invalid trimmed uuid 2`() {
        assertThrows<IllegalArgumentException> { "-e243d57-df17-4b3f-bd7f-d9cdd494c8c6".toUUID() }
    }
}
