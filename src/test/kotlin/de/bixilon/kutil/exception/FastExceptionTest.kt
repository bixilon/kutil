package de.bixilon.kutil.exception

import de.bixilon.kutil.exception.ExceptionUtil.catchAll
import de.bixilon.kutil.unit.UnitFormatter.formatNanos
import kotlin.system.measureNanoTime

class FastExceptionTest {

    // @Test
    fun `create a lot of them`() {
        val time = measureNanoTime {
            for (x in 0 until 80000) {
                catchAll {
                    throw FastException()
                }
            }
        }

        println("That took ${time.formatNanos()}")
    }
}
