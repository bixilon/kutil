package de.bixilon.kutil.time

import de.bixilon.kutil.time.DurationUtil.durationString
import de.bixilon.kutil.time.DurationUtil.raw
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import kotlin.time.Duration
import kotlin.time.Duration.Companion.seconds

class DurationUtilTest {

    @Test
    fun `convert 0 to raw`() {
        val raw = Duration.ZERO.raw()
        assertEquals(raw, 0L)
    }

    @Test
    fun `convert one second to raw`() {
        val raw = 1.seconds.raw()
        assertEquals(raw, 2000000000L)
    }

    @Test
    fun `convert one second to String`() {
        val raw = 2000000000L.durationString()
        assertEquals(raw, "1s")
    }
}
