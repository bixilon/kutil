package de.bixilon.kutil.hex

import de.bixilon.kutil.hex.HexUtil.fromHex
import de.bixilon.kutil.hex.HexUtil.fromHexString
import de.bixilon.kutil.hex.HexUtil.isHexString
import de.bixilon.kutil.hex.HexUtil.isHexStringFast
import de.bixilon.kutil.hex.HexUtil.isHexStringSafe
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import kotlin.test.assertContentEquals

class HexUtilTest {

    @Test
    fun `not a hex string`() {
        val string = "acbdefg"
        assertFalse(string.isHexString)
    }

    @Test
    fun `hex string`() {
        val string = "0123456789abcdefABCDEF"
        assertTrue(string.isHexString)
    }

    @Test
    fun `fast not a hex string`() {
        val string = "acbdefg"
        assertFalse(string.isHexStringFast)
    }

    @Test
    fun `fast hex string`() {
        val string = "0123456789abcdefABCDEF"
        assertTrue(string.isHexStringFast)
    }

    @Test
    fun `safe not a hex string`() {
        val string = "acbdefg"
        assertFalse(string.isHexStringSafe)
    }

    @Test
    fun `safe hex string`() {
        val string = "0123456789abcdefABCDEF"
        assertTrue(string.isHexStringSafe)
    }

    @Test
    fun `from hex lowercase`() {
        assertEquals('a'.code.fromHex(), 0x0A)
    }

    @Test
    fun `from hex uppercase`() {
        assertEquals('A'.code.fromHex(), 0x0A)
    }

    @Test
    fun `from hex digit`() {
        assertEquals('1'.code.fromHex(), 0x01)
    }

    @Test
    fun `from hex invalid`() {
        assertEquals('G'.code.fromHex(), -1)
    }


    @Test
    fun hexString1() {
        val data = "0011223344556677"
        assertContentEquals(data.fromHexString(), byteArrayOf(0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77))
    }

    @Test
    fun hexString2() {
        val data = "0123456789ABCDEF"
        assertContentEquals(data.fromHexString(), byteArrayOf(0x01, 0x23, 0x45, 0x67, 0x89.toByte(), 0xAB.toByte(), 0xCD.toByte(), 0xEF.toByte()))
    }
}
