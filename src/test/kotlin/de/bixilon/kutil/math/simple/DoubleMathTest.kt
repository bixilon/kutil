/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.math.simple

import de.bixilon.kutil.math.simple.DoubleMath.rounded10
import kotlin.test.Test
import kotlin.test.assertEquals


internal class DoubleMathTest {

    @Test
    fun testRounded10_1() {
        assertEquals(180.0, 180.04.rounded10)
    }

    @Test
    fun testRounded10_2() {
        assertEquals(180.0, 179.95.rounded10)
    }

    @Test
    fun testRounded10_3() {
        assertEquals(180.0, 180.0.rounded10)
    }

    @Test
    fun testRounded10_4() {
        assertEquals(180.0, 180.0001.rounded10)
    }

    @Test
    fun testRounded10_5() {
        assertEquals(180.1, 180.05.rounded10)
    }

    @Test
    fun testRounded10_6() {
        assertEquals(181.0, 181.04.rounded10)
    }
}
