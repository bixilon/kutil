/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.math

import org.junit.jupiter.api.Assertions.assertEquals
import kotlin.test.Test

class TrigonometryTest {

    @Test
    fun sin1() {
        assertEquals(0.0f, Trigonometry.sin(0.0f))
    }

    @Test
    fun sin2() {
        assertEquals(0.38940498f, Trigonometry.sin(0.4f))
    }

    @Test
    fun sin3() {
        assertEquals(0.8414514f, Trigonometry.sin(1.0f))
    }

    @Test
    fun sin4() {
        assertEquals(0.9974911f, Trigonometry.sin(1.5f))
    }

    @Test
    fun sin5() {
        assertEquals(0.9093276f, Trigonometry.sin(2.0f))
    }

    @Test
    fun sin6() {
        assertEquals(0.7457608f, Trigonometry.sin(2.3f))
    }

    @Test
    fun sin7() {
        assertEquals(0.23925838f, Trigonometry.sin(2.9f))
    }

    @Test
    fun sin8() {
        assertEquals(0.0016298539f, Trigonometry.sin(3.14f))
    }

    @Test
    fun sin9() {
        assertEquals(1.2246469E-16f, Trigonometry.sin(Math.PI.toFloat()))
    }

    @Test
    fun sin10() {
        assertEquals(0.9893586f, Trigonometry.sin(8.0f))
    }

    @Test
    fun cos1() {
        assertEquals(1.0f, Trigonometry.cos(0.0f))
    }

    @Test
    fun cos2() {
        assertEquals(0.92106664f, Trigonometry.cos(0.4f))
    }

    @Test
    fun cos3() {
        assertEquals(0.54033285f, Trigonometry.cos(1.0f))
    }

    @Test
    fun cos4() {
        assertEquals(0.070791475f, Trigonometry.cos(1.5f))
    }

    @Test
    fun cos5() {
        assertEquals(-0.41608086f, Trigonometry.cos(2.0f))
    }

    @Test
    fun cos6() {
        assertEquals(-0.6662138f, Trigonometry.cos(2.3f))
    }

    @Test
    fun cos7() {
        assertEquals(-0.9709559f, Trigonometry.cos(2.9f))
    }

    @Test
    fun cos8() {
        assertEquals(-0.9999987f, Trigonometry.cos(3.14f))
    }

    @Test
    fun cos9() {
        assertEquals(-1.0f, Trigonometry.cos(Math.PI.toFloat()))
    }

    @Test
    fun cos10() {
        assertEquals(-0.14549749f, Trigonometry.cos(8.0f))
    }
}
