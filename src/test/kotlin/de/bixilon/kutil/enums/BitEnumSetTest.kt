package de.bixilon.kutil.enums

import de.bixilon.kutil.enums.ValuesEnum.Companion.names
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNotEquals
import kotlin.test.assertTrue

class BitEnumSetTest {

    @Test
    fun empty() {
        val set = ABC.set()
        assertEquals(set.size, 0)
        assertFalse(ABC.A in set)
        assertFalse(ABC.B in set)
        assertFalse(ABC.C in set)
    }

    @Test
    fun `kotlin copy with one element`() {
        val set = ABC.set(ABC.A)
        set.toSet() // does not call hasNext()
    }

    @Test
    fun `empty iterator`() {
        val set = ABC.set()
        val iterator = set.iterator()
        assertFalse(iterator.hasNext())
        assertThrows<IllegalStateException> { iterator.next() }
        assertThrows<IllegalStateException> { iterator.remove() }
    }

    @Test
    fun `single element iterator`() {
        val set = ABC.set(ABC.A)
        val iterator = set.iterator()
        assertTrue(iterator.hasNext())
        assertTrue(iterator.hasNext())
        assertEquals(iterator.next(), ABC.A)
        assertFalse(iterator.hasNext())
        assertFalse(iterator.hasNext())
        assertThrows<IllegalStateException> { iterator.next() }
    }

    @Test
    fun `multiple element iterator`() {
        val set = ABC.set(ABC.A, ABC.B)
        val iterator = set.iterator()
        assertTrue(iterator.hasNext())
        assertTrue(iterator.hasNext())
        assertEquals(iterator.next(), ABC.A)
        assertTrue(iterator.hasNext())
        assertTrue(iterator.hasNext())
        assertEquals(iterator.next(), ABC.B)
        assertFalse(iterator.hasNext())
        assertThrows<IllegalStateException> { iterator.next() }
    }

    @Test
    fun `multiple element iterator starting in middle`() {
        val set = ABC.set(ABC.B, ABC.C)
        val iterator = set.iterator()
        assertTrue(iterator.hasNext())
        assertTrue(iterator.hasNext())
        assertEquals(iterator.next(), ABC.B)
        assertTrue(iterator.hasNext())
        assertTrue(iterator.hasNext())
        assertEquals(iterator.next(), ABC.C)
        assertFalse(iterator.hasNext())
        assertThrows<IllegalStateException> { iterator.next() }
    }

    @Test
    fun `multiple element iterator starting at almost end`() {
        val set = ABC.set()
        set += ABC.F
        val iterator = set.iterator()
        assertTrue(iterator.hasNext())
        assertEquals(iterator.next(), ABC.F)
        assertFalse(iterator.hasNext())
        assertThrows<IllegalStateException> { iterator.next() }
    }

    @Test
    fun `full element iterator`() {
        val set = ABC.set(ABC.A, ABC.B, ABC.C)
        val iterator = set.iterator()
        assertTrue(iterator.hasNext())
        assertEquals(iterator.next(), ABC.A)
        assertTrue(iterator.hasNext())
        assertEquals(iterator.next(), ABC.B)
        assertTrue(iterator.hasNext())
        assertEquals(iterator.next(), ABC.C)

        assertFalse(iterator.hasNext())
        assertThrows<IllegalStateException> { iterator.next() }
    }

    @Test
    fun `iterator removing`() {
        val set = ABC.set(ABC.A, ABC.B)
        val iterator = set.iterator()
        assertTrue(iterator.hasNext())
        assertEquals(iterator.next(), ABC.A)
        iterator.remove()
        assertEquals(set, setOf(ABC.B))
    }

    @Test
    fun `equality same type`() {
        assertEquals(ABC.set(ABC.A, ABC.B), ABC.set(ABC.A, ABC.B))
        assertNotEquals(ABC.set(ABC.A, ABC.B), ABC.set(ABC.A, ABC.C))
    }

    @Test
    fun `same hash code same type`() {
        assertEquals(ABC.set(ABC.A, ABC.B).hashCode(), ABC.set(ABC.A, ABC.B).hashCode())
        assertNotEquals(ABC.set(ABC.A, ABC.B).hashCode(), ABC.set(ABC.A, ABC.C).hashCode())
    }

    @Test
    fun `equality different type`() {
        assertEquals(ABC.set(ABC.A, ABC.B), setOf(ABC.A, ABC.B))
    }

    @Test
    fun `equality different sizes`() {
        assertNotEquals(ABC.set(ABC.A, ABC.B), setOf(ABC.A, ABC.B, ABC.C))
    }

    @Test
    fun `equality same size 2`() {
        assertEquals(setOf(ABC.E), ABC.set(ABC.E))
    }

    @Test
    fun `add value`() {
        val set = ABC.set(ABC.A, ABC.B)
        set += ABC.C
        assertEquals(set, setOf(ABC.A, ABC.B, ABC.C))
    }

    @Test
    fun `add same value`() {
        val set = ABC.set(ABC.A, ABC.B)
        set += ABC.A
        assertEquals(set, setOf(ABC.A, ABC.B))
    }

    @Test
    fun `remove value`() {
        val set = ABC.set(ABC.A, ABC.B)
        set -= ABC.B
        assertEquals(set, setOf(ABC.A))
    }

    @Test
    fun `remove non existing value`() {
        val set = ABC.set(ABC.A, ABC.B)
        set -= ABC.C
        assertEquals(set, setOf(ABC.A, ABC.B))
    }

    @Test
    fun `equality of copy`() {
        val set = ABC.set(ABC.A, ABC.C)
        assertEquals(set, set.copy())
    }

    @Test
    fun `equality of copy while original was modified`() {
        val set = ABC.set(ABC.A, ABC.C)
        val copy = set.copy()
        set -= ABC.A
        assertNotEquals(set, copy)
    }

    @Test
    fun `size property of copy`() {
        val set = ABC.set(ABC.A, ABC.C)
        val copy = set.copy()
        assertEquals(set.size, 2)
        assertEquals(copy.size, 2)
    }


    private enum class ABC {
        A,
        B,
        C,
        D,
        E,
        F,
        ;

        companion object : ValuesEnum<ABC> {
            override val VALUES = values()
            override val NAME_MAP = names()
        }
    }
}
