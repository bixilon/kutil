/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.uri

import de.bixilon.kutil.uri.URIUtil.checkWeb
import de.bixilon.kutil.uri.URIUtil.toURI
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertDoesNotThrow
import java.net.URI

internal class URIUtilTest {

    @Test
    fun toURI() {
        val expected = URI("https://test.de/abc")
        val uri = "https://test.de/abc".toURI()
        assertEquals(expected, uri)
    }

    @Test
    fun checkValid() {
        val uri = "https://test.de/dummy.test".toURI()
        assertDoesNotThrow { uri.checkWeb() }
    }

    @Test
    fun checkInvalid() {
        val uri = "ftp://test.de/dummy.test".toURI()
        assertThrows(IllegalArgumentException::class.java) { uri.checkWeb() }
    }
}
