package de.bixilon.kutil.concurrent.lock.locks.reentrant

import de.bixilon.kutil.concurrent.lock.LockTest
import org.junit.jupiter.api.Test
import java.util.concurrent.TimeoutException
import kotlin.concurrent.thread
import kotlin.test.assertFalse
import kotlin.time.Duration.Companion.milliseconds

class ReentrantLockTest : LockTest() {

    override fun create() = ReentrantLock()


    @Test
    fun `lock fails on second thread`() {
        val lock = create()
        lock.lock()
        var executed = false
        thread(start = true) { assertFalse(lock.lock(10.milliseconds)); executed = true }

        var i = 0
        while (!executed) {
            Thread.sleep(1)
            if (i++ > 100) throw TimeoutException()
        }
    }
}
