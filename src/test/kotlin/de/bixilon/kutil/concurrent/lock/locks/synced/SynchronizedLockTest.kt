package de.bixilon.kutil.concurrent.lock.locks.synced

import de.bixilon.kutil.concurrent.lock.LockTest
import org.junit.jupiter.api.Test
import kotlin.test.assertFalse
import kotlin.time.Duration.Companion.milliseconds

class SynchronizedLockTest : LockTest() {

    override fun create() = SynchronizedLock()

    @Test
    fun `second lock fails same thread`() {
        val lock = create()
        lock.lock()
        assertFalse(lock.lock(10.milliseconds))
    }
}
