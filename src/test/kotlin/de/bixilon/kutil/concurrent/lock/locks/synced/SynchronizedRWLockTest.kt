package de.bixilon.kutil.concurrent.lock.locks.synced

import de.bixilon.kutil.concurrent.lock.RWLockTest
import org.junit.jupiter.api.Test
import kotlin.test.assertFalse
import kotlin.time.Duration.Companion.milliseconds

class SynchronizedRWLockTest : RWLockTest() {

    override fun create() = SynchronizedRWLock()

    @Test
    fun `second lock fails same thread`() {
        val lock = create()
        lock.lock()
        assertFalse(lock.lock(10.milliseconds))
    }

    @Test
    fun `can not acquire locked same thread`() {
        val lock = create()
        lock.lock()
        assertFalse(lock.acquire(10.milliseconds))
    }
}
