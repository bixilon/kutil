package de.bixilon.kutil.concurrent.lock

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import kotlin.test.assertFalse
import kotlin.time.Duration.Companion.milliseconds

abstract class RWLockTest : LockTest() {

    abstract override fun create(): RWLock

    @Test
    fun `acquire 100 times`() {
        val lock = create()
        for (i in 0 until 100) {
            lock.acquire()
        }
    }

    @Test
    fun `acquire 100 times and release`() {
        val lock = create()
        for (i in 0 until 100) {
            lock.acquire()
        }
        for (i in 0 until 100) {
            lock.release()
        }
    }

    @Test
    fun `acquire and release`() {
        val lock = create()
        lock.acquire()
        lock.release()
    }

    @Test
    fun `can not lock acquired`() {
        val lock = create()
        lock.acquire()
        assertFalse(lock.lock(10.milliseconds))
    }

    @Test
    fun `release more often than acquired`() {
        val lock = create()
        lock.acquire()
        lock.release()
        assertThrows<IllegalMonitorStateException> { lock.release() }
    }

    // @Test
    fun `benchmark rapid acquiring and releasing`() {
        val lock = create()
        for (i in 0 until 99_999_999) {
            lock.acquire()
            lock.release()
        }
    }
}
