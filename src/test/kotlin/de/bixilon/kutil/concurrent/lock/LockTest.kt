package de.bixilon.kutil.concurrent.lock

import de.bixilon.kutil.concurrent.pool.ThreadPool
import de.bixilon.kutil.environment.Environment
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import kotlin.test.assertEquals

abstract class LockTest {

    abstract fun create(): Lock

    @Test
    fun `lock and unlock`() {
        val lock = create()
        lock.lock()
        lock.unlock()
    }

    @Test
    fun `unlock more than locked fails`() {
        val lock = create()
        lock.lock()
        lock.unlock()
        assertThrows<IllegalMonitorStateException> { lock.unlock() }
    }

    @Test
    fun `count to 1000000, practically`() {
        if (Environment.isInCI()) {
            System.err.println("Not checking 1000000 additions!")
            return
        }
        val cpus = Runtime.getRuntime().availableProcessors()
        if (cpus < 2) {
            System.err.println("For this test the jvm must have at least 2 cores")
            return
        }
        val target = 10_000
        val lock = create()
        var number = 0
        val pool = ThreadPool(cpus)
        for (i in 0 until target) {
            pool += {
                lock.lock()
                number++
                lock.unlock()
            }
        }
        while (pool.queueSize > 0) {
            Thread.sleep(1)
        }
        Thread.sleep(5) // pool size of 0 does not mean it is done
        assertEquals(number, target)
        pool.shutdownNow()
    }

    // @Test
    fun `benchmark rapid locking and unlocking`() {
        val lock = create()
        for (i in 0 until 99_999_999) {
            lock.lock()
            lock.unlock()
        }
    }
}
