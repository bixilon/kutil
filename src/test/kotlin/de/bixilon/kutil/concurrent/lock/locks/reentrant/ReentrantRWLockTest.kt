package de.bixilon.kutil.concurrent.lock.locks.reentrant

import de.bixilon.kutil.concurrent.lock.RWLockTest
import org.junit.jupiter.api.Test
import java.util.concurrent.TimeoutException
import kotlin.concurrent.thread
import kotlin.test.assertFalse
import kotlin.time.Duration.Companion.milliseconds

class ReentrantRWLockTest : RWLockTest() {

    override fun create() = ReentrantRWLock()

    @Test
    fun `lock fails on second thread`() {
        val lock = create()
        lock.lock()
        var executed = false
        thread(start = true) { assertFalse(lock.lock(10.milliseconds)); executed = true }

        var i = 0
        while (!executed) {
            Thread.sleep(1)
            if (i++ > 100) throw TimeoutException()
        }
    }

    @Test
    fun `acquire fails on locked thread`() {
        val lock = create()
        lock.lock()
        var executed = false
        thread(start = true) { assertFalse(lock.acquire(10.milliseconds)); executed = true }

        var i = 0
        while (!executed) {
            Thread.sleep(1)
            if (i++ > 100) throw TimeoutException()
        }
    }

    @Test
    fun `lock fails on acquired thread`() {
        val lock = create()
        lock.acquire()
        var executed = false
        thread(start = true) { assertFalse(lock.lock(10.milliseconds)); executed = true }

        var i = 0
        while (!executed) {
            Thread.sleep(1)
            if (i++ > 100) throw TimeoutException()
        }
    }
}
