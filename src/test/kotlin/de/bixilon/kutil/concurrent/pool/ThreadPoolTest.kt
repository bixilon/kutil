/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.concurrent.pool

import de.bixilon.kutil.concurrent.pool.runnable.ForcePooledRunnable
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class ThreadPoolTest {

    @Test
    fun `Check high priority task`() {
        val pool = ThreadPool(1)
        pool += ForcePooledRunnable { Thread.sleep(200) }
        var highPriorityTaskExecuted = false
        var executed = false
        pool += ForcePooledRunnable(priority = ThreadPool.Priorities.LOW) {
            Thread.sleep(200)
            assertTrue { highPriorityTaskExecuted }
            executed = true
        }
        pool += ForcePooledRunnable(priority = ThreadPool.Priorities.HIGH) {
            Thread.sleep(200)
            highPriorityTaskExecuted = true
        }
        Thread.sleep(700)
        assertTrue { executed }
        pool.shutdownNow()
    }

    @Test
    fun `Check low priority task`() {
        val pool = ThreadPool(1)
        pool += ForcePooledRunnable { Thread.sleep(200) }
        var highPriorityTaskExecuted = false
        var executed = false
        pool += ForcePooledRunnable(priority = ThreadPool.Priorities.LOWER) {
            Thread.sleep(200)
            assertTrue { highPriorityTaskExecuted }
            executed = true
        }
        pool += ForcePooledRunnable(priority = ThreadPool.Priorities.LOW) {
            Thread.sleep(200)
            highPriorityTaskExecuted = true
        }
        Thread.sleep(700)
        assertTrue { executed }
        pool.shutdownNow()
    }

    @Test
    fun `Check creation order`() {
        val pool = ThreadPool(1)
        var lastTask = 0


        fun addTask(id: Int) {
            pool += ForcePooledRunnable {
                Thread.sleep(10)
                assertEquals(id, lastTask)
                lastTask++
            }
            Thread.sleep(3)
        }
        for (i in 0 until 15) {
            addTask(i)
        }

        Thread.sleep(300)
        assertEquals(15, lastTask)
        pool.shutdownNow()
    }

    @Test
    fun `Check execution order`() {
        val pool = ThreadPool(1)
        var lastTask = 0


        fun addTask(id: Int) {
            pool += ForcePooledRunnable {
                Thread.sleep(10)
                assertEquals(id, lastTask)
                lastTask++
            }
        }
        for (i in 0 until 15) {
            addTask(i)
        }

        Thread.sleep(300)
        assertEquals(15, lastTask)
        pool.shutdownNow()
    }
}
