/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.concurrent.worker

import de.bixilon.kutil.cast.CastUtil.unsafeCast
import de.bixilon.kutil.concurrent.worker.task.TaskWorker
import de.bixilon.kutil.concurrent.worker.task.WorkerTask
import de.bixilon.kutil.latch.SimpleLatch
import kotlin.test.Test
import kotlin.test.assertTrue


internal class TaskWorkerTest {

    @Test
    fun testComplex() {
        val worker = TaskWorker()
        val done = BooleanArray(1000)
        for (id in done.indices) {
            val dependencies = Array<Any?>(id / 3) { it }
            worker += WorkerTask(id, dependencies = dependencies.unsafeCast()) { done[id] = true }
        }
        val latch = SimpleLatch(1)
        worker.work(latch)
        latch.dec()
        latch.await()

        for ((index, value) in done.withIndex()) {
            assertTrue(value, "Task $index not completed!")
        }
    }
}
