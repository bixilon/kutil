/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.concurrent.worker.unconditional

import de.bixilon.kutil.concurrent.pool.ThreadPool
import de.bixilon.kutil.latch.SimpleLatch
import kotlin.test.Test
import kotlin.test.assertEquals

internal class UnconditionalWorkerTest {

    @Test
    fun test() {
        val count = SimpleLatch(0)

        val worker = UnconditionalWorker()
        for (i in 0 until 1923) {
            worker += { count.inc() }
        }
        worker.work()
        assertEquals(1923, count.count)
    }

    @Test
    fun testBusy() {
        val count = SimpleLatch(0)
        val pool = ThreadPool(1, "busy#")
        pool += { Thread.sleep(100000L) }

        val worker = UnconditionalWorker(pool)
        for (i in 0 until 1923) {
            worker += { count.inc() }
        }
        worker.work()
        assertEquals(1923, count.count)
        pool.shutdownNow()
    }
}
