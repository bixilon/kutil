/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.concurrent.queue

import org.junit.jupiter.api.Test
import kotlin.system.measureTimeMillis
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue
import kotlin.time.Duration.Companion.milliseconds

internal class QueueTest {

    @Test
    fun testSingleTask() {
        val queue = Queue()
        var ran = false
        queue += { ran = true }
        assertFalse { ran }
        queue.work(1)
        assertTrue { ran }
    }

    @Test
    fun testMultipleStepTask() {
        val queue = Queue()
        var ran = 0
        queue += { ran++ }
        queue += { ran++ }
        queue.work(1)
        assertEquals(ran, 1)
        queue.work(1)
        assertEquals(ran, 2)
    }

    @Test
    fun testMultipleTask() {
        val queue = Queue()
        var ran = 0
        queue += { ran++ }
        queue += { ran++ }
        queue += { ran++ }
        queue.work(3)
        assertEquals(ran, 3)
    }

    @Test
    fun testTaskLimit() {
        val queue = Queue()
        var ran = 0
        queue += { ran++ }
        queue += { ran++ }
        queue += { ran++ }
        queue.work(2)
        assertEquals(ran, 2)
        queue.work(1)
        assertEquals(ran, 3)
    }

    @Test
    fun testTaskTimeLimit() {
        val queue = Queue()
        var ran = 0
        queue += { ran++;Thread.sleep(100L) }
        queue += { ran++;Thread.sleep(100L) }
        queue += { ran++;Thread.sleep(100L) }
        queue.workTimeLimited(150.milliseconds)
        assertEquals(ran, 2)
        queue.workTimeLimited(1.milliseconds)
        assertEquals(ran, 3)
    }

    @Test
    fun `task time limited duration`() {
        val queue = Queue()
        var ran = 0
        queue += { ran++;Thread.sleep(100L) }
        queue += { ran++;Thread.sleep(100L) }
        queue += { ran++;Thread.sleep(100L) }
        queue.workTimeLimited(150.milliseconds)
        assertEquals(ran, 2)
        queue.workTimeLimited(1.milliseconds)
        assertEquals(ran, 3)
    }

    @Test
    fun workTimeout1() {
        val queue = Queue()
        val time = measureTimeMillis {
            queue.workBlocking(70L.milliseconds)
        }
        assertTrue(time in 65L..130L, "Timeout too long: $time")
    }

    @Test
    fun workTimeout2() {
        val queue = Queue()
        var ran = 0
        Thread { Thread.sleep(100L); queue += { ran++ } }.start()
        val time = measureTimeMillis {
            queue.workBlocking(200L.milliseconds)
        }
        assertEquals(ran, 1)
        assertTrue(time in 100L..350L, "Timeout too long: $time")
    }
}
