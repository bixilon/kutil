package de.bixilon.kutil.concurrent

import kotlin.system.measureTimeMillis
import kotlin.test.Test
import kotlin.test.assertTrue

class WakerTest {

    @Test
    fun test() {
        var time = -1L
        val waker = Waker()
        Thread { time = measureTimeMillis { waker.wait(); } }.start()
        Thread.sleep(10L) // wait for thread

        Thread.sleep(80L)
        waker.notify()
        Thread.sleep(10L)

        assertTrue(time in 80L..100L, "Not in range: $time")
    }
}
