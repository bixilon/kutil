/*
 * KUtil
 * Copyright (C) 2021-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.kutil.concurrent.schedule

import de.bixilon.kutil.concurrent.pool.DefaultThreadPool
import de.bixilon.kutil.reflection.ReflectionUtil.forceInit
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import kotlin.time.Duration.Companion.milliseconds

class TaskSchedulerTest {

    init {
        // otherwise timings might won't work as expected
        DefaultThreadPool::class.java.forceInit()
        TaskScheduler::class.java.forceInit()
    }

    @Test
    fun runIn() {
        var run = 0
        val task = QueuedTask(100.milliseconds) { run++ }
        try {
            TaskScheduler += task
            Thread.sleep(80)
            assertEquals(run, 0)
            Thread.sleep(40)
            assertEquals(run, 1)
            Thread.sleep(100)
            assertEquals(run, 1)
        } finally {
            TaskScheduler -= task
        }
    }

    @Test
    fun repeated() {
        var run = 0
        val task = RepeatedTask(100.milliseconds) { run++ }
        try {
            TaskScheduler += task
            Thread.sleep(80)
            assertEquals(1, run)
            Thread.sleep(40)
            assertEquals(2, run)
            Thread.sleep(100)
            assertEquals(3, run)
            Thread.sleep(100)
            assertEquals(4, run)
        } finally {
            TaskScheduler -= task
        }
    }
}
