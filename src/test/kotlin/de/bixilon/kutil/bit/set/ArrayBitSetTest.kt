package de.bixilon.kutil.bit.set

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals

class ArrayBitSetTest {

    @Test
    fun `empty creation`() {
        val set = ArrayBitSet(1)
        assertEquals(set.length(), 0)
        assertEquals(set[0], false)
        assertEquals(set[63], false)
    }

    @Test
    fun `index out of bounds creation`() {
        val set = ArrayBitSet(1)
        assertThrows<IndexOutOfBoundsException> { set[-1] }
        assertThrows<IndexOutOfBoundsException> { set[64] }
    }

    @Test
    fun `correct setting index 0`() {
        val set = ArrayBitSet(1)
        assertEquals(set[0], false)
        set[0] = true
        assertEquals(set[0], true)
        set[0] = false
        assertEquals(set[0], false)
    }

    @Test
    fun `correct setting index 63`() {
        val set = ArrayBitSet(1)
        assertEquals(set[63], false)
        set[63] = true
        assertEquals(set[63], true)
        set[63] = false
        assertEquals(set[63], false)
    }

    @Test
    fun `correct setting index 127`() {
        val set = ArrayBitSet(127)
        assertEquals(set[127], false)
        set[127] = true
        assertEquals(set[127], true)
        set[127] = false
        assertEquals(set[127], false)
    }

    @Test
    fun clearing() {
        val set = ArrayBitSet(1)
        set[63] = true
        set.clear()
        assertEquals(set[63], false)
    }

    @Test
    fun cloning() {
        val set = ArrayBitSet(1)
        set[63] = true
        assertEquals(set[30], false)
        assertEquals(set[63], true)
        val clone = set.clone()
        assertEquals(clone[30], false)
        assertEquals(clone[63], true)
    }

    @Test
    fun `empty length`() {
        val set = ArrayBitSet(1)
        assertEquals(set.length(), 0)
    }

    @Test
    fun `first bit set length`() {
        val set = ArrayBitSet(1)
        set[0] = true
        assertEquals(set.length(), 1)
    }

    @Test
    fun `last bit set length`() {
        val set = ArrayBitSet(1)
        set[63] = true
        assertEquals(set.length(), 64)
    }

    @Test
    fun `equality 1`() {
        val set = ArrayBitSet(1)
        val other = ArrayBitSet(1)
        assertEquals(set.hashCode(), other.hashCode())
        assertEquals(set, other)
    }

    @Test
    fun `equality 2`() {
        val set = ArrayBitSet(1).apply { this[25] = true }
        val other = ArrayBitSet(1).apply { this[25] = true }
        assertEquals(set.hashCode(), other.hashCode())
        assertEquals(set, other)
    }

    @Test
    fun `not equal 2`() {
        val set = ArrayBitSet(1).apply { this[25] = true }
        val other = ArrayBitSet(1).apply { this[27] = true }
        assertNotEquals(set.hashCode(), other.hashCode())
        assertNotEquals(set, other)
    }
}
