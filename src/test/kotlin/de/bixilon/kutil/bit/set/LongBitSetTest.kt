package de.bixilon.kutil.bit.set

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals

class LongBitSetTest {

    @Test
    fun `empty creation`() {
        val set = LongBitSet()
        assertEquals(set.length(), 0)
        assertEquals(set[0], false)
        assertEquals(set[63], false)
    }

    @Test
    fun `index out of bounds creation`() {
        val set = LongBitSet()
        assertThrows<IndexOutOfBoundsException> { set[-1] }
        assertThrows<IndexOutOfBoundsException> { set[64] }
    }

    @Test
    fun `correct setting index 0`() {
        val set = LongBitSet()
        assertEquals(set[0], false)
        set[0] = true
        assertEquals(set[0], true)
        set[0] = false
        assertEquals(set[0], false)
    }

    @Test
    fun `correct setting index 63`() {
        val set = LongBitSet()
        assertEquals(set[63], false)
        set[63] = true
        assertEquals(set[63], true)
        set[63] = false
        assertEquals(set[63], false)
    }

    @Test
    fun clearing() {
        val set = LongBitSet()
        set[63] = true
        set.clear()
        assertEquals(set[63], false)
    }

    @Test
    fun cloning() {
        val set = LongBitSet()
        set[63] = true
        assertEquals(set[30], false)
        assertEquals(set[63], true)
        val clone = set.clone()
        assertEquals(clone[30], false)
        assertEquals(clone[63], true)
    }

    @Test
    fun `empty length`() {
        val set = LongBitSet()
        assertEquals(set.length(), 0)
    }

    @Test
    fun `first bit set length`() {
        val set = LongBitSet()
        set[0] = true
        assertEquals(set.length(), 1)
    }

    @Test
    fun `last bit set length`() {
        val set = LongBitSet()
        set[63] = true
        assertEquals(set.length(), 64)
    }

    @Test
    fun `equality 1`() {
        val set = LongBitSet()
        val other = LongBitSet()
        assertEquals(set.hashCode(), other.hashCode())
        assertEquals(set, other)
    }

    @Test
    fun `equality 2`() {
        val set = LongBitSet().apply { this[25] = true }
        val other = LongBitSet().apply { this[25] = true }
        assertEquals(set.hashCode(), other.hashCode())
        assertEquals(set, other)
    }

    @Test
    fun `not equal 2`() {
        val set = LongBitSet().apply { this[25] = true }
        val other = LongBitSet().apply { this[27] = true }
        assertNotEquals(set.hashCode(), other.hashCode())
        assertNotEquals(set, other)
    }
}
