package de.bixilon.kutil.bit.set

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals

class IntBitSetTest {

    @Test
    fun `empty creation`() {
        val set = IntBitSet()
        assertEquals(set.length(), 0)
        assertEquals(set[0], false)
        assertEquals(set[31], false)
    }

    @Test
    fun `index out of bounds creation`() {
        val set = IntBitSet()
        assertThrows<IndexOutOfBoundsException> { set[-1] }
        assertThrows<IndexOutOfBoundsException> { set[32] }
    }

    @Test
    fun `correct setting index 0`() {
        val set = IntBitSet()
        assertEquals(set[0], false)
        set[0] = true
        assertEquals(set[0], true)
        set[0] = false
        assertEquals(set[0], false)
    }

    @Test
    fun `correct setting index 31`() {
        val set = IntBitSet()
        assertEquals(set[31], false)
        set[31] = true
        assertEquals(set[31], true)
        set[31] = false
        assertEquals(set[31], false)
    }

    @Test
    fun clearing() {
        val set = IntBitSet()
        set[31] = true
        set.clear()
        assertEquals(set[31], false)
    }

    @Test
    fun cloning() {
        val set = IntBitSet()
        set[31] = true
        assertEquals(set[30], false)
        assertEquals(set[31], true)
        val clone = set.clone()
        assertEquals(clone[30], false)
        assertEquals(clone[31], true)
    }

    @Test
    fun `empty length`() {
        val set = IntBitSet()
        assertEquals(set.length(), 0)
    }

    @Test
    fun `first bit set length`() {
        val set = IntBitSet()
        set[0] = true
        assertEquals(set.length(), 1)
    }

    @Test
    fun `last bit set length`() {
        val set = IntBitSet()
        set[31] = true
        assertEquals(set.length(), 32)
    }

    @Test
    fun `equality 1`() {
        val set = IntBitSet()
        val other = IntBitSet()
        assertEquals(set.hashCode(), other.hashCode())
        assertEquals(set, other)
    }

    @Test
    fun `equality 2`() {
        val set = IntBitSet().apply { this[25] = true }
        val other = IntBitSet().apply { this[25] = true }
        assertEquals(set.hashCode(), other.hashCode())
        assertEquals(set, other)
    }

    @Test
    fun `not equal 2`() {
        val set = IntBitSet().apply { this[25] = true }
        val other = IntBitSet().apply { this[27] = true }
        assertNotEquals(set.hashCode(), other.hashCode())
        assertNotEquals(set, other)
    }
}
